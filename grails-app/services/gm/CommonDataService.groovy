package gm

import grails.transaction.Transactional

@Transactional
class CommonDataService {

    def dynamicQueryService


    def flagList() {
        [[value: 'yyyy-mm-dd', name: 'daily'],
         [value: 'week', name: 'weekly'],
         [value: 'yyyy-mm', name: 'monthly'],
         [value: 'yyyy-mm-dd hh24', name: 'hourly']
        ]
    }

    def partList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'partList', orgid: map.orgid])
    }

    def stageList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'stageList', orgid: map.orgid])
    }

    def processList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'processList', orgid: map.orgid])
    }

    def partversion(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'partversion', orgid: map.orgid])
    }

    def charidList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'charidList', orgid: map.orgid])
    }

    def lotsList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'lotsList', orgid: map.orgid])
    }

    def locationList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'locationList', orgid: map.orgid])
    }

    def departmentList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'departmentList', orgid: map.orgid])
    }

    def edcSetNameList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'edcSetNameList', orgid: map.orgid])
    }

    def materiaType(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'materiaType', orgid: map.orgid])
    }

    def itemNameList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'itemNameList', orgid: map.orgid])
    }

    def procedure_IdList(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'procedure_IdList', orgid: map.orgid])
    }

    def procedureTeam(Map map) {
        dynamicQueryService.queryForList([SYS_QUERY_NAME: 'procedureTeam', orgid: map.orgid])
    }

}