package org

/**
 * 数据处理的工具类
 */
class CommonUtils {

    static Map getDynamicQueryMap(Map params, List<String> list) {
        def map = [:]
        for (String str in list) {
            if (params.containsKey(str)) {
                if ((params.get(str)!='') && params.get(str) != null) {
                    map.put(str, params.get(str))
                }
            }
        }
        map
    }
}
