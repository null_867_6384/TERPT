package org

/**
 * 测试数据提供工具
 */
class DataUtils {

    static ArrayList equipmentList() {
        def eqpList = []

        def eqpMap1 = ['equipment_id': 'RC01']
        def eqpMap2 = ['equipment_id': 'SO02']
        def eqpMap3 = ['equipment_id': 'eqp003']
        def eqpMap4 = ['equipment_id': 'eqp004']
        def eqpMap5 = ['equipment_id': 'eqp005']

        eqpList.add(eqpMap1)
        eqpList.add(eqpMap2)
        eqpList.add(eqpMap3)
        eqpList.add(eqpMap4)
        eqpList.add(eqpMap5)

        eqpList
    }


    static ArrayList getCellList() {
        def now = new Date()
        def cellList = []

        def cellMap1 = ['id': 1, 'org': '区域一', 'lot': 'lot000001', 'plate': 'glass000001', 'isActive': true]
        def cellMap2 = ['id': 2, 'org': '区域二', 'lot': 'lot000002', 'plate': 'glass000002', 'isActive': false]
        def cellMap3 = ['id': 3, 'org': '区域三', 'lot': 'lot000003', 'plate': 'glass000003', 'isActive': true]
        def cellMap4 = ['id': 4, 'org': '区域四', 'lot': 'lot000004', 'plate': 'glass000004', 'isActive': true]
        def cellMap5 = ['id': 5, 'org': '区域五', 'lot': 'lot000005', 'plate': 'glass000005', 'isActive': true]

        cellList.add(cellMap1)
        cellList.add(cellMap2)
        cellList.add(cellMap3)
        cellList.add(cellMap4)
        cellList.add(cellMap5)

        cellList
    }

    static ArrayList getStepList() {
        def now = new Date()
        def stepList = []

        def stepMap1 = ['id': 1, 'stepname': '工序一', 'version': '版本一', 'created': now]
        def stepMap2 = ['id': 2, 'stepname': '工序二', 'version': '版本二', 'created': now]
        def stepMap3 = ['id': 3, 'stepname': '工序三', 'version': '版本三', 'created': now]
        def stepMap4 = ['id': 4, 'stepname': '工序四', 'version': '版本三', 'created': now]
        def stepMap5 = ['id': 5, 'stepname': '工序五', 'version': '版本三', 'created': now]
        def stepMap6 = ['id': 6, 'stepname': '工序六', 'version': '版本二', 'created': now]
        def stepMap7 = ['id': 7, 'stepname': '工序七', 'version': '版本三', 'created': now]
        def stepMap8 = ['id': 8, 'stepname': '工序八', 'version': '版本二', 'created': now]
        def stepMap9 = ['id': 9, 'stepname': '工序九', 'version': '版本一', 'created': now]
        def stepMap10 = ['id': 10, 'stepname': '工序十', 'version': '版本一', 'created': now]

        stepList.add(stepMap1)
        stepList.add(stepMap2)
        stepList.add(stepMap3)
        stepList.add(stepMap4)
        stepList.add(stepMap5)
        stepList.add(stepMap6)
        stepList.add(stepMap7)
        stepList.add(stepMap8)
        stepList.add(stepMap9)
        stepList.add(stepMap10)

        stepList
    }

    static ArrayList materialList() {
        def materialList = []

        def materialMap1 = ['eqpid': 'RC01', 'appData': 'RECIPE_NO', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 121,
                           '8AKA555555A4': 121,'8AKA555555A5': 121, '8AKA555555A6':121, '8AKA555555A7': 121]
        def materialMap2 = ['eqpid': 'RC01', 'appData': 'OVEN_NO', '8AKA555555A1': 2, '8AKA555555A2': 2, '8AKA555555A3': 2,
                            '8AKA555555A4': 2,'8AKA555555A5': 2, '8AKA555555A6':2, '8AKA555555A7': 2]
        def materialMap3 = ['eqpid': 'RC01', 'appData': 'OVEN_SLOT', '8AKA555555A1': 4, '8AKA555555A2': 4, '8AKA555555A3': 4,
                            '8AKA555555A4': 4,'8AKA555555A5': 4, '8AKA555555A6':4, '8AKA555555A7': 4]
        def materialMap4 = ['eqpid': 'RC01', 'appData': 'SET_TEMP', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 120,
                            '8AKA555555A4': 120,'8AKA555555A5': 120, '8AKA555555A6':120, '8AKA555555A7': 120]
        def materialMap5 = ['eqpid': 'RC01', 'appData': 'UPPER_L_TEMP', '8AKA555555A1': 120, '8AKA555555A2': 120, '8AKA555555A3': 120,
                            '8AKA555555A4': 120,'8AKA555555A5': 120, '8AKA555555A6':120, '8AKA555555A7': 120]
        def materialMap6 = ['eqpid': 'RC01', 'appData': 'UPPER_R_TEMP', '8AKA555555A1': 119, '8AKA555555A2': 119, '8AKA555555A3': 119,
                            '8AKA555555A4': 119,'8AKA555555A5': 119, '8AKA555555A6':119, '8AKA555555A7': 119]
        def materialMap7 = ['eqpid': 'RC01', 'appData': 'LOWER_L_TEMP', '8AKA555555A1': 119, '8AKA555555A2': 119, '8AKA555555A3': 119,
                            '8AKA555555A4': 119,'8AKA555555A5': 119, '8AKA555555A6':119, '8AKA555555A7': 119]
        def materialMap8 = ['eqpid': 'RC01', 'appData': 'LOWER_R_TEMP', '8AKA555555A1': 80, '8AKA555555A2': 80, '8AKA555555A3': 80,
                            '8AKA555555A4': 80,'8AKA555555A5': 80, '8AKA555555A6':80, '8AKA555555A7': 80]
        def materialMap9 = ['eqpid': 'RC01', 'appData': 'SET_OVEN_TIME', '8AKA555555A1': 4817, '8AKA555555A2': 4817, '8AKA555555A3': 4817,
                            '8AKA555555A4': 4817,'8AKA555555A5': 4817, '8AKA555555A6':4817, '8AKA555555A7': 4817]
        def materialMap10 = ['eqpid': 'RC01', 'appData': 'HEATING_TIME', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 121,
                             '8AKA555555A4': 121,'8AKA555555A5': 121, '8AKA555555A6':121, '8AKA555555A7': 121]

        materialList.add(materialMap1)
        materialList.add(materialMap2)
        materialList.add(materialMap3)
        materialList.add(materialMap4)
        materialList.add(materialMap5)
        materialList.add(materialMap6)
        materialList.add(materialMap7)
        materialList.add(materialMap8)
        materialList.add(materialMap9)
        materialList.add(materialMap10)

        materialList
    }



    static ArrayList phaseList(){
        def now = new Date()

        def phaseList = []

        def phaseMap1 = ['phase': 'PH1', 'productSpec': 'XA', 'startTime': now, 'endTime': now, 'assyIn': 1557]
        def phaseMap2 = ['phase': 'PH1', 'productSpec': 'XB', 'startTime': now, 'endTime': now, 'assyIn': 1557]
        def phaseMap3 = ['phase': 'PH1', 'productSpec': 'XC', 'startTime': now, 'endTime': now, 'assyIn': 1557]
        def phaseMap4 = ['phase': 'PH1', 'productSpec': 'XD', 'startTime': now, 'endTime': now, 'assyIn': 1557]

        phaseList.add(phaseMap1)
        phaseList.add(phaseMap2)
        phaseList.add(phaseMap3)
        phaseList.add(phaseMap4)

        phaseList
    }



    static ArrayList specList(){
        def specList = []

        def phaseMap1 = ['specName': 'XA']
        def phaseMap2 = ['specName': 'XB']
        def phaseMap3 = ['specName': 'XC']

        specList.add(phaseMap1)
        specList.add(phaseMap2)
        specList.add(phaseMap3)

        specList
    }


    static ArrayList specDataList(){
        def specDataList = []

        def specDataMap1 = ['eqpid': 'SD01','tactTIme':148.8 ,'qty':406]
        def specDataMap2 = ['eqpid': 'SD02','tactTIme': 145.9 ,'qty':400]
        def specDataMap3 = ['eqpid': 'SD03','tactTIme':152.6 ,'qty':356]
        def specDataMap4 = ['eqpid': 'SD04','tactTIme':'' ,'qty':'']
        def specDataMap5 = ['eqpid': 'LD01','tactTIme':156.8 ,'qty':554]
        def specDataMap6 = ['eqpid': 'LD02','tactTIme':143.0 ,'qty':602]
        def specDataMap7 = ['eqpid': 'ARC','tactTIme':55.5 ,'qty':1557]
        def specDataMap8 = ['eqpid': 'ARC OVEN','tactTIme':37.3 ,'qty':2313]
        def specDataMap9 = ['eqpid': 'SI','tactTIme':144.9 ,'qty':596]
        def specDataMap10 = ['eqpid': 'PS','tactTIme':141.6 ,'qty':609]

        specDataList.add(specDataMap1)
        specDataList.add(specDataMap2)
        specDataList.add(specDataMap3)
        specDataList.add(specDataMap4)
        specDataList.add(specDataMap5)
        specDataList.add(specDataMap6)
        specDataList.add(specDataMap7)
        specDataList.add(specDataMap8)
        specDataList.add(specDataMap9)
        specDataList.add(specDataMap10)

        specDataList
    }



    static ArrayList cycleTimeList() {

        def cycleTimeList = []

        def cycleTimeMap1 = [ 'appData': 'site1', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 121,
                            '8AKA555555A4': 121,'8AKA555555A5': 121, '8AKA555555A6':121, '8AKA555555A7': 121]
        def cycleTimeMap2 = [ 'appData': 'site2', '8AKA555555A1': 2, '8AKA555555A2': 2, '8AKA555555A3': 2,
                            '8AKA555555A4': 2,'8AKA555555A5': 2, '8AKA555555A6':2, '8AKA555555A7': 2]
        def cycleTimeMap3 = ['appData': 'site3', '8AKA555555A1': 4, '8AKA555555A2': 4, '8AKA555555A3': 4,
                            '8AKA555555A4': 4,'8AKA555555A5': 4, '8AKA555555A6':4, '8AKA555555A7': 4]
        def cycleTimeMap4 = ['appData': 'site4', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 120,
                            '8AKA555555A4': 120,'8AKA555555A5': 120, '8AKA555555A6':120, '8AKA555555A7': 120]
        def cycleTimeMap5 = ['appData': 'site5', '8AKA555555A1': 120, '8AKA555555A2': 120, '8AKA555555A3': 120,
                            '8AKA555555A4': 120,'8AKA555555A5': 120, '8AKA555555A6':120, '8AKA555555A7': 120]
        def cycleTimeMap6 = ['appData': 'site6', '8AKA555555A1': 119, '8AKA555555A2': 119, '8AKA555555A3': 119,
                            '8AKA555555A4': 119,'8AKA555555A5': 119, '8AKA555555A6':119, '8AKA555555A7': 119]
        def cycleTimeMap7 = ['appData': 'site7', '8AKA555555A1': 119, '8AKA555555A2': 119, '8AKA555555A3': 119,
                            '8AKA555555A4': 119,'8AKA555555A5': 119, '8AKA555555A6':119, '8AKA555555A7': 119]
        def cycleTimeMap8 = ['appData': 'site8', '8AKA555555A1': 80, '8AKA555555A2': 80, '8AKA555555A3': 80,
                            '8AKA555555A4': 80,'8AKA555555A5': 80, '8AKA555555A6':80, '8AKA555555A7': 80]
        def cycleTimeMap9 = ['appData': 'site9', '8AKA555555A1': 4817, '8AKA555555A2': 4817, '8AKA555555A3': 4817,
                            '8AKA555555A4': 4817,'8AKA555555A5': 4817, '8AKA555555A6':4817, '8AKA555555A7': 4817]
        def cycleTimeMap10 = ['appData': 'site10', '8AKA555555A1': 121, '8AKA555555A2': 121, '8AKA555555A3': 121,
                             '8AKA555555A4': 121,'8AKA555555A5': 121, '8AKA555555A6':121, '8AKA555555A7': 121]
        def cycleTimeMap11 = ['appData': 'Avg', '8AKA555555A1': 562.4, '8AKA555555A2': 562.4, '8AKA555555A3': 562.4,
                              '8AKA555555A4': 562.4,'8AKA555555A5': 562.4, '8AKA555555A6':562.4, '8AKA555555A7': 562.4]
        def cycleTimeMap12 = ['appData': 'Min', '8AKA555555A1': 2, '8AKA555555A2': 2, '8AKA555555A3': 2,
                              '8AKA555555A4': 2,'8AKA555555A5': 2, '8AKA555555A6':2, '8AKA555555A7': 2]
        def cycleTimeMap13 = ['appData': 'Max', '8AKA555555A1': 4817, '8AKA555555A2': 4817, '8AKA555555A3': 4817,
                              '8AKA555555A4': 4817,'8AKA555555A5': 4817, '8AKA555555A6':4817, '8AKA555555A7': 4817]

        cycleTimeList.add(cycleTimeMap1)
        cycleTimeList.add(cycleTimeMap2)
        cycleTimeList.add(cycleTimeMap3)
        cycleTimeList.add(cycleTimeMap4)
        cycleTimeList.add(cycleTimeMap5)
        cycleTimeList.add(cycleTimeMap6)
        cycleTimeList.add(cycleTimeMap7)
        cycleTimeList.add(cycleTimeMap8)
        cycleTimeList.add(cycleTimeMap9)
        cycleTimeList.add(cycleTimeMap10)
        cycleTimeList.add(cycleTimeMap11)
        cycleTimeList.add(cycleTimeMap12)
        cycleTimeList.add(cycleTimeMap13)

        cycleTimeList
    }

    static int getTotalCount() {
        100
    }


    static ArrayList getHoldGlassList() {
        def now = new Date()
        def holdGlassList = []

        def holdGlassMap1 = ['id'   : 1, 'plateNo': 'FRS-11N', 'stripNo': 501, 'prcslowId': 'CIA', 'prcsNo': 'FLP00001-L8FA', 'holdType': 'BRK',
                             'holdCode': '00021', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap2 = ['id'   : 2, 'plateNo': 'FRS-12N', 'stripNo': 502, 'prcslowId': 'CIA', 'prcsNo': 'FLP00002-L8FA', 'holdType': 'BRK',
                             'holdCode': '00022', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap3 = ['id'   : 3, 'plateNo': 'FRS-13N', 'stripNo': 503, 'prcslowId': 'CIA', 'prcsNo': 'FLP00003-L8FA', 'holdType': 'BRK',
                             'holdCode': '00023', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap4 = ['id'   : 4, 'plateNo': 'FRS-14N', 'stripNo': 504, 'prcslowId': 'CIA', 'prcsNo': 'FLP00004-L8FA', 'holdType': 'BRK',
                             'holdCode': '00024', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap5 = ['id'   : 5, 'plateNo': 'FRS-15N', 'stripNo': 505, 'prcslowId': 'CIA', 'prcsNo': 'FLP00005-L8FA', 'holdType': 'BRK',
                             'holdCode': '00025', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap6 = ['id'   : 6, 'plateNo': 'FRS-16N', 'stripNo': 506, 'prcslowId': 'CIA', 'prcsNo': 'FLP00006-L8FA', 'holdType': 'BRK',
                             'holdCode': '00026', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap7 = ['id'   : 7, 'plateNo': 'FRS-17N', 'stripNo': 507, 'prcslowId': 'CIA', 'prcsNo': 'FLP00007-L8FA', 'holdType': 'BRK',
                             'holdCode': '00027', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap8 = ['id'   : 8, 'plateNo': 'FRS-18N', 'stripNo': 508, 'prcslowId': 'CIA', 'prcsNo': 'FLP00008-L8FA', 'holdType': 'BRK',
                             'holdCode': '00028', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']

        holdGlassList.add(holdGlassMap1)
        holdGlassList.add(holdGlassMap2)
        holdGlassList.add(holdGlassMap3)
        holdGlassList.add(holdGlassMap4)
        holdGlassList.add(holdGlassMap5)
        holdGlassList.add(holdGlassMap6)
        holdGlassList.add(holdGlassMap7)
        holdGlassList.add(holdGlassMap8)

        holdGlassList
    }


    static ArrayList getMoveInfoList() {
        def moveInfoList = []

        def moveInfoMap1 = ['id': 1, 'eqpId': 'FRS-11N', 'productId': 'CIA', 'productName': 'FLP00001-L8FA', 'weekNo': 'FLP00001-L8FA', 'qty': 10]
        def moveInfoMap2 = ['id': 2, 'eqpId': 'FRS-12N', 'productId': 'CIA', 'productName': 'FLP00002-L8FA', 'weekNo': 'FLP00002-L8FA', 'qty': 11]
        def moveInfoMap3 = ['id': 3, 'eqpId': 'FRS-13N', 'productId': 'CIA', 'productName': 'FLP00003-L8FA', 'weekNo': 'FLP00003-L8FA', 'qty': 12]
        def moveInfoMap4 = ['id': 4, 'eqpId': 'FRS-14N', 'productId': 'CIA', 'productName': 'FLP00004-L8FA', 'weekNo': 'FLP00004-L8FA', 'qty': 15]
        def moveInfoMap5 = ['id': 5, 'eqpId': 'FRS-15N', 'productId': 'CIA', 'productName': 'FLP00005-L8FA', 'weekNo': 'FLP00005-L8FA', 'qty': 17]

        moveInfoList.add(moveInfoMap1)
        moveInfoList.add(moveInfoMap2)
        moveInfoList.add(moveInfoMap3)
        moveInfoList.add(moveInfoMap4)
        moveInfoList.add(moveInfoMap5)

        moveInfoList
    }

    static ArrayList getProductList() {
        def productList = []

        def productMap1 = ['id': 1, 'productId': 'CIA', 'productName': 'FLP00001-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap2 = ['id': 2, 'productId': 'CIA', 'productName': 'FLP00002-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap3 = ['id': 3, 'productId': 'CIA', 'productName': 'FLP00003-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap4 = ['id': 4, 'productId': 'CIA', 'productName': 'FLP00004-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap5 = ['id': 5, 'productId': 'CIA', 'productName': 'FLP00005-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]

        productList.add(productMap1)
        productList.add(productMap2)
        productList.add(productMap3)
        productList.add(productMap4)
        productList.add(productMap5)

        productList
    }

    static HashMap getWipProdMap() {
        def prodTypeList = ['TFT', 'CF']
        def countMap = ['beforeCell': 580, 'lms': 482, 'pi': 512, 'pDirect': 520, 'odf': 485, 'pmb': 396, 'pme': 412, 'pmb': 560, 'afterCell': 620]

        def wipProdList = []
        def data1List = []
        def data2List = []

        for (type in prodTypeList) {
            if (type.equals('TFT')) {
                data1List.add(['type': type, 'dataMap': ['beforeCell': 120, 'lms': 70, 'pi': 90, 'pDirect': 80, 'afterCell': 110]])
                data2List.add(['type': type, 'dataMap': ['beforeCell': 120, 'lms': 70, 'pi': 90, 'pDirect': 80, 'afterCell': 110]])
            } else {
                data1List.add(['type': type, 'dataMap': ['beforeCell': 90, 'lms': 80, 'pi': 92, 'pDirect': 88, 'afterCell': 121]])
                data2List.add(['type': type, 'dataMap': ['beforeCell': 90, 'lms': 80, 'pi': 92, 'pDirect': 88, 'afterCell': 121]])
            }
        }

        def productMap1 = ['id': 1, 'cellName': '24A', 'dataList': data1List]
        def productMap2 = ['id': 2, 'cellName': '41A', 'dataList': data2List]

        wipProdList.add(productMap1)
        wipProdList.add(productMap2)

        def wipProdMap = ['wipProdList': wipProdList, 'prodTypeList': prodTypeList, countMap: countMap]

        wipProdMap
    }


    static ArrayList getCassetteInfoList() {
        def now = new Date()
        def cassetteInfoList = []

        def cassetteInfoMap1 = ['id'   : 1, 'cassetteId': 'FRS-11N', 'cellId': 'FLP00001-L8FA', 'storkId': 'FLP00001-L8FA', 'pieceNumber': 10,
                                 'prodNo': 30, 'portId': 'Port00001', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap2 = ['id'   : 2, 'cassetteId': 'FRS-12N', 'cellId': 'FLP00002-L8FA', 'storkId': 'FLP00002-L8FA', 'pieceNumber': 11,
                                'type': '类型二', 'prodNo': 40, 'portId': 'Port00002', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap3 = ['id'   : 3, 'cassetteId': 'FRS-13N', 'cellId': 'FLP00003-L8FA', 'storkId': 'FLP00003-L8FA', 'pieceNumber': 12,
                                'type': '类型三', 'prodNo': 50, 'portId': 'Port00003', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap4 = ['id'   : 4, 'cassetteId': 'FRS-14N', 'cellId': 'FLP00004-L8FA', 'storkId': 'FLP00004-L8FA', 'pieceNumber': 15,
                                'isActive': false, 'type': '类型四', 'prodNo': 60, 'portId': 'Port00004', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap5 = ['id'   : 5, 'cassetteId': 'FRS-15N', 'cellId': 'FLP00005-L8FA', 'storkId': 'FLP00005-L8FA', 'pieceNumber': 17,
                                'type': '类型五', 'prodNo': 70, 'portId': 'Port00005', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]

        cassetteInfoList.add(cassetteInfoMap1)
        cassetteInfoList.add(cassetteInfoMap2)
        cassetteInfoList.add(cassetteInfoMap3)
        cassetteInfoList.add(cassetteInfoMap4)
        cassetteInfoList.add(cassetteInfoMap5)

        cassetteInfoList
    }


    static ArrayList getCassetteTransHistoryList() {
        def now = new Date()
        def cassetteTransHistoryList = []

        def cassetteTransHistoryMap1 = ['id'   : 1, 'cassetteId': 'FRS-11N', 'cellId': 'FLP00001-L8FA', 'storkId': 'FLP00001-L8FA', 'pieceNumber': 10,
                                         'prodNo': 30, 'portId': 'Port00001', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap2 = ['id'   : 2, 'cassetteId': 'FRS-12N', 'cellId': 'FLP00002-L8FA', 'storkId': 'FLP00002-L8FA', 'pieceNumber': 11,
                                        'type': '类型二', 'prodNo': 40, 'portId': 'Port00002', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap3 = ['id'   : 3, 'cassetteId': 'FRS-13N', 'cellId': 'FLP00003-L8FA', 'storkId': 'FLP00003-L8FA', 'pieceNumber': 12,
                                        'type': '类型三', 'prodNo': 50, 'portId': 'Port00003', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap4 = ['id'   : 4, 'cassetteId': 'FRS-14N', 'cellId': 'FLP00004-L8FA', 'storkId': 'FLP00004-L8FA', 'pieceNumber': 15,
                                        'isActive': false, 'type': '类型四', 'prodNo': 60, 'portId': 'Port00004', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap5 = ['id'   : 5, 'cassetteId': 'FRS-15N', 'cellId': 'FLP00005-L8FA', 'storkId': 'FLP00005-L8FA', 'pieceNumber': 17,
                                        'type': '类型五', 'prodNo': 70, 'portId': 'Port00005', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        cassetteTransHistoryList.add(cassetteTransHistoryMap1)
        cassetteTransHistoryList.add(cassetteTransHistoryMap2)
        cassetteTransHistoryList.add(cassetteTransHistoryMap3)
        cassetteTransHistoryList.add(cassetteTransHistoryMap4)
        cassetteTransHistoryList.add(cassetteTransHistoryMap5)

        cassetteTransHistoryList
    }

    static ArrayList getMoveTrendList() {
        def now = new Date()
        def moveTrendList = []

        def moveTrendMap1 = ['id'     : 1, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10001', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00001-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00001', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0001']

        def moveTrendMap2 = ['id'     : 2, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10002', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00002-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00002', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0002']

        def moveTrendMap3 = ['id'     : 3, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10003', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00003-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00003', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0003']

        def moveTrendMap4 = ['id'     : 4, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10004', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00004-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00004', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0004']

        def moveTrendMap5 = ['id'     : 5, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10005', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00005-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00005', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0005']

        moveTrendList.add(moveTrendMap1)
        moveTrendList.add(moveTrendMap2)
        moveTrendList.add(moveTrendMap3)
        moveTrendList.add(moveTrendMap4)
        moveTrendList.add(moveTrendMap5)

        moveTrendList
    }


    static ArrayList getGlassList() {
        def glassList = []
        def glassMap1 = ['id': 1, 'org': '区域一', 'lot': 'lot000001', 'cellNumber': 48, 'location': '江苏省张家港市', 'isActive': true]
        glassList
    }

    static ArrayList<Integer> getPageSizeList() {
        [10,20, 50,100,1000]
    }

    static ArrayList<Integer> getMonPageSizeList() {
        [144,432,1000]
    }

    static ArrayList getCassetteStatusList() {

        def cassetteStatusList = []

        def assetteStatusMap1 = [ "item": "ACT",  "num1": 'SD01',   "num2": 'XA',"num3": 148.8, "num4": 411, "num5": 21,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap2 = [ "item": "CF",   "num1": 'SD02',   "num2": 'XA',"num3": 146.9, "num4": 416, "num5": 22,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap3 = [ "item": "DMT",  "num1": 'SD03',   "num2": 'XA',"num3": 152.4, "num4": 356, "num5": 23,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap4 = [ "item": "EMP",  "num1": 'LD01',   "num2": 'XA',"num3": 156.2, "num4": 554, "num5": 24,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap5 = [ "item": "MG",   "num1": 'LD02',   "num2": 'XA',"num3": 143.0, "num4": 602, "num5": 25,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap6 = [ "item": "MUL",  "num1": 'ARC',   "num2": 'XA',"num3": 55.3, "num4": 1416, "num5": 26,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap7 = [ "item": "REP",  "num1": 'ARC OVEN',   "num2": 'XA',   "num3": 37.5, "num4": 417, "num5": 27,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap8 = [ "item": "TFT",  "num1": 'SI001',   "num2": 'XA',   "num3": 144.0, "num4": 418, "num5": 28,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap9 = [ "item": "Total","num1": 'PS',   "num2": 'XA',   "num3": 141.9, "num4": 419, "num5": 29,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap10 = [ "item": "REP",  "num1": 'SI002',   "num2": 'XA',   "num3": 145.2, "num4": 417, "num5": 27,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]

        cassetteStatusList.add(assetteStatusMap1)
        cassetteStatusList.add(assetteStatusMap2)
        cassetteStatusList.add(assetteStatusMap3)
        cassetteStatusList.add(assetteStatusMap4)
        cassetteStatusList.add(assetteStatusMap5)
        cassetteStatusList.add(assetteStatusMap6)
        cassetteStatusList.add(assetteStatusMap7)
        cassetteStatusList.add(assetteStatusMap8)
        cassetteStatusList.add(assetteStatusMap9)
        cassetteStatusList.add(assetteStatusMap10)

        cassetteStatusList
    }


    static ArrayList getTactTimeByLotList() {

        def now = new Date()
        def lotTactTimeList = []

        def lotTactTimeMap1 = ['id': 1, 'lotId': 'FRS-11N', 'eqpId': 'FLP00001-L8FA', 'stepId': 'FLP00001-L8FA', 'shift': 10,
                                         'glassQty': 30, 'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap2 = ['id': 2, 'lotId': 'FRS-12N', 'eqpId': 'FLP00002-L8FA', 'stepId': 'FLP00002-L8FA', 'shift': 11,
                                         'glassQty': 40, 'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap3 = ['id': 3, 'lotId': 'FRS-13N', 'eqpId': 'FLP00003-L8FA', 'stepId': 'FLP00003-L8FA', 'shift': 12,
                                        'glassQty': 50,  'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap4 = ['id': 4, 'lotId': 'FRS-14N', 'eqpId': 'FLP00004-L8FA', 'stepId': 'FLP00004-L8FA', 'shift': 15,
                                         'glassQty': 60,  'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap5 = ['id': 5, 'lotId': 'FRS-15N', 'eqpId': 'FLP00005-L8FA', 'stepId': 'FLP00005-L8FA', 'shift': 17,
                                         'glassQty': 70,  'trackInTime': now, 'trackOutTime': now]
        lotTactTimeList.add(lotTactTimeMap1)
        lotTactTimeList.add(lotTactTimeMap2)
        lotTactTimeList.add(lotTactTimeMap3)
        lotTactTimeList.add(lotTactTimeMap4)
        lotTactTimeList.add(lotTactTimeMap5)

        lotTactTimeList
    }


    static ArrayList getTactTimeByGlassList() {

        def now = new Date()
        def glassTactTimeList = []

        def glassTactTimeMap1 = ['id': 1, 'glassId': 'FRS-11N', 'eqpId': 'FLP00001-L8FA', 'stepId': 'FLP00001-L8FA', 'shift': 10,
                               'glassQty': 30, 'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap2 = ['id': 2, 'glassId': 'FRS-12N', 'eqpId': 'FLP00002-L8FA', 'stepId': 'FLP00002-L8FA', 'shift': 11,
                               'glassQty': 40, 'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap3 = ['id': 3, 'glassId': 'FRS-13N', 'eqpId': 'FLP00003-L8FA', 'stepId': 'FLP00003-L8FA', 'shift': 12,
                               'glassQty': 50,  'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap4 = ['id': 4, 'glassId': 'FRS-14N', 'eqpId': 'FLP00004-L8FA', 'stepId': 'FLP00004-L8FA', 'shift': 15,
                               'glassQty': 60,  'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap5 = ['id': 5, 'glassId': 'FRS-15N', 'eqpId': 'FLP00005-L8FA', 'stepId': 'FLP00005-L8FA', 'shift': 17,
                               'glassQty': 70,  'trackInTime': now, 'trackOutTime': now]
        glassTactTimeList.add(glassTactTimeMap1)
        glassTactTimeList.add(glassTactTimeMap2)
        glassTactTimeList.add(glassTactTimeMap3)
        glassTactTimeList.add(glassTactTimeMap4)
        glassTactTimeList.add(glassTactTimeMap5)

        glassTactTimeList
    }


    static  ArrayList getTrendTestList(){
        def now = new Date()
        def trendTestList = []

        def trendTestMap1 = ['id': 1, 'type': 'PH1', 'operation': '89.31%', 'eqpId': '1.21%', 'operInTime': '9.21%',
                             'operInLotId': 60, 'input': 197,'model':'KA','jcNumber':1,'s': '06:00 KA', 'dcs': '15:00 KA', 'xHighLight': '']
        def trendTestMap2 = ['id': 2, 'type': 'PH2', 'operation': '100.0%', 'eqpId': '', 'operInTime':  '0.46%',
                             'operInLotId': 70, 'input': 161,'model':'HL','jcNumber':0, 's': '06:00 HL', 'dcs': '15:00 HL', 'xHighLight': '']
        def trendTestMap3 = ['id': 3, 'type': 'PH3', 'operation': '100.0%', 'eqpId': '', 'operInTime': '',
                             'operInLotId': 80, 'input': 144,'model':'XA','jcNumber':1, 's': '06:00 XA', 'dcs': '15:00 XA', 'xHighLight': '']
        def trendTestMap4 = ['id': 4, 'type': 'PH4', 'operation': '92.36%', 'eqpId': '', 'operInTime': '7.91%',
                             'operInLotId': 110, 'input': 255,'model':'KB','jcNumber':2,  's': '06:00 KB', 'dcs': '15:00 KB', 'xHighLight': '']
        def trendTestMap5 = ['id': 5, 'type': 'PH5', 'operation': '92.09%', 'eqpId': '', 'operInTime': '7.76%',
                             'operInLotId': 130, 'input': 172,'model':'LL','jcNumber':0 ,'s': '06:00 LL', 'dcs': '15:00 LL', 'xHighLight': '']
        def trendTestMap6 = ['id': 6, 'type': 'PH6', 'operation': '100.0%', 'eqpId': '', 'operInTime': '2.61%',
                             'operInLotId': 90, 'input': 182,'model':'LM','jcNumber':1, 's': '06:00 LM', 'dcs': '15:00 LM', 'xHighLight': '']
        def trendTestMap7 = ['id': 7, 'type': 'PH7', 'operation': '97.92%', 'eqpId': '', 'operInTime': '',
                             'operInLotId': 65, 'input': 245,'model':'XA','jcNumber':1, 's': '06:00 XA', 'dcs': '15:00 XA', 'xHighLight': '']
        def trendTestMap8 = ['id': 8, 'type': 'PH8', 'operation': '54.62%', 'eqpId': '', 'operInTime': '45.11%',
                             'operInLotId':60, 'input': 178,'model':'MB','jcNumber':1, 's': '06:00 MB', 'dcs': '15:00 MB', 'xHighLight': '']
        def trendTestMap9 = ['id': 9, 'type': 'PH9', 'operation': '96.23%', 'eqpId': '0.68%', 'operInTime': '3.68%',
                             'operInLotId': 112, 'input': 107,'model':'SF','jcNumber':2, 's': '06:00 SF', 'dcs': '15:00 SF', 'xHighLight': '']
        def trendTestMap10 = ['id': 10, 'type': 'PH10', 'operation': '67.81%', 'eqpId': '', 'operInTime': '32.89%',
                              'operInLotId':70, 'input': 207,'model':'FE','jcNumber':0, 's': '06:00 FE', 'dcs': '15:00 FE', 'xHighLight': '']

        trendTestList.add(trendTestMap1)
        trendTestList.add(trendTestMap2)
        trendTestList.add(trendTestMap3)
        trendTestList.add(trendTestMap4)
        trendTestList.add(trendTestMap5)
        trendTestList.add(trendTestMap6)
        trendTestList.add(trendTestMap7)
        trendTestList.add(trendTestMap8)
        trendTestList.add(trendTestMap9)
        trendTestList.add(trendTestMap10)

        trendTestList
    }

    static  ArrayList getUnitTestList(){
        def now = new Date()
        def trendTestList = []

        def trendTestMap1 = ['id': 1, 'operation': '11100', 'eqpId': '3CIP101', 'operInTime': now, 'unitId': '3CCS360001', 'lotId': 'LOT-S120001', 'input': 197, 's': '0%', 'dcs': '1.5%', 'xHighLight': '0.0%']
        def trendTestMap2 = ['id': 2, 'operation': '11100', 'eqpId': '3CIP102', 'operInTime': now, 'unitId': '3CCS360002', 'lotId': 'LOT-S120002',  'input': 81, 's': '0%', 'dcs': '1.2%', 'xHighLight': '0.0%']
        def trendTestMap3 = ['id': 3, 'operation': '11100', 'eqpId': '3CIP103', 'operInTime': now, 'unitId': '3CCS360003', 'lotId': 'LOT-S120003',  'input': 54, 's': '0%', 'dcs': '0.1%', 'xHighLight': '0.0%']
        def trendTestMap4 = ['id': 4, 'operation': '11100', 'eqpId': '3CIP104', 'operInTime': now, 'unitId': '3CCS360004', 'lotId': 'LOT-S120004', 'input': 55, 's': '0%', 'dcs': '3.6%', 'xHighLight': '0.0%']
        def trendTestMap5 = ['id': 5, 'operation': '11100', 'eqpId': '3CIP105', 'operInTime': now, 'unitId': '3CCS360005', 'lotId': 'LOT-S120005',  'input': 72, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%']
        def trendTestMap6 = ['id': 6, 'operation': '11100', 'eqpId': '3CIP106', 'operInTime': now, 'unitId': '3CCS360006', 'lotId': 'LOT-S120006',  'input': 82, 's': '0%', 'dcs': '2.4%', 'xHighLight': '0.0%']
        def trendTestMap7 = ['id': 7, 'operation': '11100', 'eqpId': '3CIP107', 'operInTime': now, 'unitId': '3CCS360007', 'lotId': 'LOT-S120007',  'input': 45, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%']
        def trendTestMap8 = ['id': 8, 'operation': '11100', 'eqpId': '3CIP108', 'operInTime': now, 'unitId': '3CCS360008', 'lotId': 'LOT-S120008',  'input': 78, 's': '0%', 'dcs': '3.8%', 'xHighLight': '0.0%']
        def trendTestMap9 = ['id': 9, 'operation': '11100', 'eqpId': '3CIP109', 'operInTime': now, 'unitId': '3CCS360009', 'lotId': 'LOT-S120009',  'input': 107, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%']

        trendTestList.add(trendTestMap1)
        trendTestList.add(trendTestMap2)
        trendTestList.add(trendTestMap3)
        trendTestList.add(trendTestMap4)
        trendTestList.add(trendTestMap5)
        trendTestList.add(trendTestMap6)
        trendTestList.add(trendTestMap7)
        trendTestList.add(trendTestMap8)
        trendTestList.add(trendTestMap9)
        trendTestList
    }


    static  ArrayList getOperInfoList(){
        def now = new Date()
        def operInfoList = []

        def operInfoMap1 = ['id': 1, 'employee': '026110', 'name': '张三', 'department': '技术部', 'companyNo':'SH001', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': "O", 'lcdAll': '', 'testAll': '', 'tftAll': '']
        def operInfoMap2 = ['id': 1, 'employee': '026111', 'name': '李四', 'department': '技术部', 'companyNo':'SH002', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll': 'O', 'testAll': '', 'tftAll':'']
        def operInfoMap3 = ['id': 1, 'employee': '026112', 'name': '王五', 'department': '技术部', 'companyNo':'SH003', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': '', 'testAll': 'O', 'tftAll':'O']
        def operInfoMap4 = ['id': 1, 'employee': '026113', 'name': '赵六', 'department': '生产部', 'companyNo':'SH004', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll': '', 'testAll': '', 'tftAll':'']
        def operInfoMap5 = ['id': 1, 'employee': '026114', 'name': '知名', 'department': '技术部', 'companyNo':'SH005', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': 'O', 'testAll': '', 'tftAll': 'O']
        def operInfoMap6 = ['id': 1, 'employee': '026115', 'name': '小花', 'department': '技术部', 'companyNo':'SH006', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll':'', 'testAll':'O', 'tftAll':'']
        def operInfoMap7 = ['id': 1, 'employee': '026116', 'name': '小黄', 'department': '技术部', 'companyNo':'SH007', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll':'', 'testAll':'', 'tftAll': 'O']
        def operInfoMap8 = ['id': 1, 'employee': '026117', 'name': '小黑', 'department': '人事部', 'companyNo':'SH008', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': 'O', 'testAll': '', 'tftAll': 'O']
        def operInfoMap9 = ['id': 1, 'employee': '026118', 'name': '佳琪', 'department': '财务部', 'companyNo':'SH009', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll':'', 'testAll':'', 'tftAll': '']

        operInfoList.add(operInfoMap1)
        operInfoList.add(operInfoMap2)
        operInfoList.add(operInfoMap3)
        operInfoList.add(operInfoMap4)
        operInfoList.add(operInfoMap5)
        operInfoList.add(operInfoMap6)
        operInfoList.add(operInfoMap7)
        operInfoList.add(operInfoMap8)
        operInfoList.add(operInfoMap9)
        operInfoList
    }


    static  ArrayList getDailyYieldList(){
        def now = new Date()
        def dailyYieldList = []

        def dailyYieldMap1 = ['id': 1, 'itemDate': now, 'inputNo': 4719, 'pQty': 3983,'sQty': 93,'qQty': 35,
                              'ngQty': 33,'emQty': 544,'pxl': 25,'sdo': 0,'sgo': 0,'sOpen': 0,'dds': 33,'dcs': 83]
        def dailyYieldMap2 = ['id': 1, 'itemDate': now, 'inputNo': 4518, 'pQty': 1428,'sQty': 91,'qQty': 36,
                              'ngQty': 35,'emQty': 206,'pxl': 18,'sdo': 1,'sgo': 0,'sOpen': 0,'dds': 98,'dcs': 3]
        def dailyYieldMap3 =  ['id': 1, 'itemDate': now, 'inputNo': 4214, 'pQty': 3203,'sQty': 83,'qQty': 37,
                               'ngQty': 53,'emQty': 183,'pxl': 19,'sdo': 0,'sgo': 0,'sOpen': 0,'dds': 39,'dcs': 9]
        def dailyYieldMap4 =  ['id': 1, 'itemDate': now, 'inputNo': 4111, 'pQty': 3119,'sQty': 60,'qQty': 39,
                               'ngQty': 33,'emQty': 193,'pxl': 13,'sdo': 2,'sgo': 0,'sOpen': 0,'dds': 83,'dcs': 8]

        dailyYieldList.add(dailyYieldMap1)
        dailyYieldList.add(dailyYieldMap2)
        dailyYieldList.add(dailyYieldMap3)
        dailyYieldList.add(dailyYieldMap4)

        dailyYieldList
    }

    static  ArrayList getJudgeRawList(){
        def now = new Date()
        def judgeRawList = []

        def judgeRawMap1 = ['id': 1, 'testTime': now, 'panelId': 'Panel00001', 'scrapCode': 'S00001','eqpId': 'EQP00001','grade': 'G00001',
                              'operId': 'O00001','shift':'SHIFT00001','stepId': 'Step0001','proId': 'Prod00001','glassId': 'Glass00001']
        def judgeRawMap2 = ['id': 2, 'testTime': now, 'panelId': 'Panel00002', 'scrapCode': 'S00002','eqpId': 'EQP00002','grade': 'G00002',
                            'operId': 'O00002','shift':'SHIFT00002','stepId': 'Step0002','proId': 'Prod00002','glassId': 'Glass00002']
        def judgeRawMap3 =  ['id': 3, 'testTime': now, 'panelId': 'Panel00003', 'scrapCode': 'S00003','eqpId': 'EQP00003','grade': 'G00003',
                             'operId': 'O00003','shift':'SHIFT00003','stepId': 'Step0003','proId': 'Prod00003','glassId': 'Glass00003']
        def judgeRawMap4 =  ['id': 4, 'testTime': now, 'panelId': 'Panel00004', 'scrapCode': 'S00004','eqpId': 'EQP00004','grade': 'G00004',
                             'operId': 'O00004','shift':'SHIFT00004','stepId': 'Step0004','proId': 'Prod00004','glassId': 'Glass00004']

        judgeRawList.add(judgeRawMap1)
        judgeRawList.add(judgeRawMap2)
        judgeRawList.add(judgeRawMap3)
        judgeRawList.add(judgeRawMap4)

        judgeRawList
    }

    static  ArrayList getCumYieldList(){
        def now = new Date()
        def judgeRawList = []

        def judgeRawMap1 = ['id': 1, 'testDate': now, 'inputQty': 51, 'outputQty': 31, 'scrapQty': 'S00001',
                            'stepId': 'Step0001','proId': 'Prod00001','yield': 'yield00001']
        def judgeRawMap2 = ['id': 2, 'testDate': now, 'inputQty': 61, 'outputQty': 42, 'scrapQty': 'S00002',
                            'stepId': 'Step0002','proId': 'Prod00002','yield': 'yield00002']
        def judgeRawMap3 =  ['id': 3, 'testDate': now, 'inputQty':71, 'outputQty':53, 'scrapQty': 'S00003',
                             'stepId': 'Step0003','proId': 'Prod00003','yield': 'yield00003']
        def judgeRawMap4 =  ['id': 4, 'testDate': now, 'inputQty': 81, 'outputQty':64, 'scrapQty': 'S00004',
                             'stepId': 'Step0004','proId': 'Prod00004','yield': 'yield00004']

        judgeRawList.add(judgeRawMap1)
        judgeRawList.add(judgeRawMap2)
        judgeRawList.add(judgeRawMap3)
        judgeRawList.add(judgeRawMap4)

        judgeRawList
    }



    static  ArrayList lineList(){
        def lineList = []

        def lineMap1 = ['line_id': 'line001']
        def lineMap2 = ['line_id': 'line002']
        def lineMap3 = ['line_id': 'line003']
        def lineMap4 = ['line_id': 'line004']

        lineList.add(lineMap1)
        lineList.add(lineMap2)
        lineList.add(lineMap3)
        lineList.add(lineMap4)

        lineList
    }


    static  ArrayList glassList(){
        def glassList = []

        def glassMap1 = ['glass_id': '8AKA555555A1']
        def glassMap2 = ['glass_id': '8AKA555555A2']
        def glassMap3 = ['glass_id': '8AKA555555A3']
        def glassMap4 = ['glass_id': '8AKA555555A4']
        def glassMap5 = ['glass_id': '8AKA555555A5']
        def glassMap6 = ['glass_id': '8AKA555555A6']
        def glassMap7 = ['glass_id': '8AKA555555A7']

        glassList.add(glassMap1)
        glassList.add(glassMap2)
        glassList.add(glassMap3)
        glassList.add(glassMap4)
        glassList.add(glassMap5)
        glassList.add(glassMap6)
        glassList.add(glassMap7)

        glassList
    }

    static  ArrayList dispArcParamsList(){
        def dispArcParamsList = []

        def dispParamsMap1 = ['param_name': 'AK_LOW_FLOW']
        def dispParamsMap2 = ['param_name': 'RINSE_2_BJ_UP_SHOWER_1']
        def dispParamsMap3 = ['param_name': 'RINSE_1_CJ_SHOWER_FLOW']
        def dispParamsMap4 = ['param_name': 'RINSE_1_LOW_NOZZLE_SHOWER_FLOW']
        def dispParamsMap5 = ['param_name': 'RINSE_1_UP_NOZZLE_SHOWER_FLOW']
        def dispParamsMap6 = ['param_name': 'DET_UP_NOZZLE_SHOWER_FLOW']
        def dispParamsMap7 = ['param_name': 'DET_AQUA_SHOWER_FLOW']

        dispArcParamsList.add(dispParamsMap1)
        dispArcParamsList.add(dispParamsMap2)
        dispArcParamsList.add(dispParamsMap3)
        dispArcParamsList.add(dispParamsMap4)
        dispArcParamsList.add(dispParamsMap5)
        dispArcParamsList.add(dispParamsMap6)
        dispArcParamsList.add(dispParamsMap7)

        dispArcParamsList
    }


    static  ArrayList dispArcOvenParamsList(){
        def dispArcOvenParamsList = []

        def dispParamsMap1 = ['param_name': 'HEATING_TEMP_AVE']
        def dispParamsMap2 = ['param_name': 'HEATING_TEMP_MAX']
        def dispParamsMap3 = ['param_name': 'HEATING_TEMP_MIN']

        dispArcOvenParamsList.add(dispParamsMap1)
        dispArcOvenParamsList.add(dispParamsMap2)
        dispArcOvenParamsList.add(dispParamsMap3)

        dispArcOvenParamsList
    }

    static  ArrayList productSpecList(){
        def productSpecList = []

        def productSpecMap1 = ['spec_name': 'XA']
        def productSpecMap2 = ['spec_name': 'XB']
        def productSpecMap3 = ['spec_name': 'XC']

        productSpecList.add(productSpecMap1)
        productSpecList.add(productSpecMap2)
        productSpecList.add(productSpecMap3)

        productSpecList
    }


    static  ArrayList alarmCodeList(){
        def alarmCodeList = []

        def alarmCodeMap1 = ['alarm_code': 'alarmCode01']
        def alarmCodeMap2 = ['alarm_code': 'alarmCode02']
        def alarmCodeMap3 = ['alarm_code': 'alarmCode03']

        alarmCodeList.add(alarmCodeMap1)
        alarmCodeList.add(alarmCodeMap2)
        alarmCodeList.add(alarmCodeMap3)

        alarmCodeList
    }

    static  ArrayList alarmTypeList(){
        def alarmTypeList = []

        def alarmTypeMap1 = ['alarm_type': 'alarmType01']
        def alarmTypeMap2 = ['alarm_type': 'alarmType02']
        def alarmTypeMap3 = ['alarm_type': 'alarmType03']

        alarmTypeList.add(alarmTypeMap1)
        alarmTypeList.add(alarmTypeMap2)
        alarmTypeList.add(alarmTypeMap3)

        alarmTypeList
    }

    static  ArrayList moveTrendList(){
        def now = new Date()
        def moveTrendList = []

        def moveTrendMap1 = ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620352D1', 'heightAvgGls': 3.055,
                            'heightMaxGls': 3.169,'heightMinGls': 2.882,'heightUniGls': 0.0429,'heightRngGls': 0.2562,'heightStdGls': 0.0751]
        def moveTrendMap2 = ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620325C8', 'heightAvgGls': 3.063,
                             'heightMaxGls': 3.148,'heightMinGls': 2.7917,'heightUniGls': 0.063,'heightRngGls': 0.377,'heightStdGls': 0.091]
        def moveTrendMap3 =  ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620308AX', 'heightAvgGls': 3.051,
                              'heightMaxGls': 3.173,'heightMinGls': 2.8959,'heightUniGls': 0.042,'heightRngGls': 0.2522,'heightStdGls': 0.0792]
        def moveTrendMap4 =  ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620370D7', 'heightAvgGls': 3.064,
                              'heightMaxGls': 3.205,'heightMinGls': 2.8629,'heightUniGls': 0.041,'heightRngGls': 0.2489,'heightStdGls': 0.0761]
        def moveTrendMap5 = ['eventTime': now, 'lotId': '8FXA620585Z', 'glassId': '8FXA620318A5', 'heightAvgGls': 3.067,
                             'heightMaxGls': 3.153,'heightMinGls': 2.886,'heightUniGls': 0.0379,'heightRngGls': 0.227,'heightStdGls': 0.0751]
        def moveTrendMap6 =  ['eventTime': now, 'lotId': '8FXA620585Z', 'glassId': '8FXA620313C8', 'heightAvgGls': 3.048,
                              'heightMaxGls': 3.16,'heightMinGls': 2.883,'heightUniGls': 0.047,'heightRngGls': 0.319,'heightStdGls': 0.0753]
        def moveTrendMap7 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620369D3', 'heightAvgGls': 3.06,
                              'heightMaxGls': 3.123,'heightMinGls': 2.875,'heightUniGls': 0.0459,'heightRngGls': 0.276,'heightStdGls': 0.0746]
        def moveTrendMap8 = ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620369C4', 'heightAvgGls': 3.056,
                             'heightMaxGls': 3.1549,'heightMinGls': 2.866,'heightUniGls': 0.04,'heightRngGls': 0.251,'heightStdGls': 0.0756]
        def moveTrendMap9 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620374D4', 'heightAvgGls': 3.055,
                              'heightMaxGls': 3.133,'heightMinGls': 2.904,'heightUniGls': 0.0421,'heightRngGls': 0.228,'heightStdGls': 0.0758]
        def moveTrendMap10 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620375A6', 'heightAvgGls': 3.059,
                              'heightMaxGls': 3.129,'heightMinGls': 2.869,'heightUniGls': 0.0379,'heightRngGls': 0.269,'heightStdGls': 0.0731]

        moveTrendList.add(moveTrendMap1)
        moveTrendList.add(moveTrendMap2)
        moveTrendList.add(moveTrendMap3)
        moveTrendList.add(moveTrendMap4)
        moveTrendList.add(moveTrendMap5)
        moveTrendList.add(moveTrendMap6)
        moveTrendList.add(moveTrendMap7)
        moveTrendList.add(moveTrendMap8)
        moveTrendList.add(moveTrendMap9)
        moveTrendList.add(moveTrendMap10)

        moveTrendList
    }


    static  ArrayList lotTactTimeList(){
        def now = new Date()
        def lotTactTimeList = []

        def lotTactTimeMap1 = ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620352D1', 'height1': 3.055, 'height2': 3.055,
                             'height3': 3.169,'height4': 2.882,'height5': 3.133,'height6': 3.133,'height7': 3.129 , 'height8': 3.129,
                             'height9': 3.169,'height10': 2.882,'height11': 3.123,'height12': 3.129,'height13': 3.123 , 'height14': 3.0751]

        def lotTactTimeMap2 = ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620325C8', 'height1': 3.063, 'height2': 3.055,
                             'height3': 3.148,'height4': 2.7917,'height5': 3.1549,'height6': 3.129,'height7': 3.055 , 'height8': 3.129,
                             'height9': 3.169,'height10': 2.882,'height11': 3.1549,'height12': 3.2562,'height13': 3.064 , 'height14': 3.0751]

        def lotTactTimeMap3 =  ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620308AX', 'height1': 3.051, 'height2': 3.055,
                              'height3': 3.173,'height4': 2.8959,'height5': 3.1549,'height6': 3.133,'height7': 3.173 , 'height8': 3.129,
                              'height9': 3.169,'height10': 2.882,'height11': 3.0429,'height12': 3.2562,'height13': 3.064 , 'height14': 3.064]

        def lotTactTimeMap4 =  ['eventTime': now, 'lotId': '8FXA620407Z-2', 'glassId': '8FXA620370D7', 'height1': 3.064, 'height2': 3.055,
                              'height3': 3.205,'height4': 2.8629,'height5': 3.1549,'height6': 3.169,'height7': 3.1549 , 'height8': 3.129,
                              'height9': 3.169,'height10': 2.882,'height11': 3.123,'height12': 3.169,'height13': 3.123 , 'height14': 3.064]

        def lotTactTimeMap5 = ['eventTime': now, 'lotId': '8FXA620585Z', 'glassId': '8FXA620318A5', 'height1': 3.067, 'height2': 3.055,
                             'height3': 3.153,'height4': 2.886,'height5': 3.133,'height6': 3.173,'height7': 3.192 , 'height8': 3.192,
                             'height9': 3.169,'height10': 2.882,'height11': 3.192,'height12': 3.205,'height13': 3.064 , 'height14': 3.064]

        def lotTactTimeMap6 =  ['eventTime': now, 'lotId': '8FXA620585Z', 'glassId': '8FXA620313C8', 'height1': 3.048, 'height2': 3.055,
                              'height3': 3.16,'height4': 2.883,'height5': 3.192,'height6': 3.192,'height7': 3.1549 , 'height8': 3.192,
                              'height9': 3.169,'height10': 2.882,'height11':3.192,'height12': 3.205,'height13': 3.169 , 'height14': 3.0751]

        def lotTactTimeMap7 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620369D3', 'height1': 3.06, 'height2': 3.055,
                              'height3': 3.123,'height4': 2.875,'height5': 3.133,'height6': 3.276,'height7': 3.192 , 'height8': 3.192,
                              'height9': 3.169,'height10': 2.882,'height11':3.192,'height12': 0.2562,'height13': 3.169 , 'height14': 3.0751]

        def lotTactTimeMap8 = ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620369C4', 'height1': 3.056, 'height2': 3.055,
                             'height3': 3.1549,'height4': 2.866,'height5': 3.133,'height6':3.276,'height7': 3.0756 , 'height8': 3.0751,
                             'height9': 3.169,'height10': 2.882,'height11': 3.133,'height12': 3.205,'height13': 3.123 , 'height14': 3.0751]

        def lotTactTimeMap9 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620374D4', 'height1': 3.055, 'height2': 3.055,
                              'height3': 3.133,'height4': 2.904,'height5': 3.205,'height6': 3.228,'height7': 3.0758 , 'height8': 3.0751,
                              'height9': 3.169,'height10': 2.882,'height11': 3.123,'height12': 0.2562,'height13': 3.0751 , 'height14': 2.9751]

        def lotTactTimeMap10 =  ['eventTime': now, 'lotId': '8FXA620593Z-1', 'glassId': '8FXA620375A6', 'height1': 3.059, 'height2': 3.055,
                               'height3': 3.129,'height4': 2.869,'height5': 3.1549,'height6': 3.269,'height7': 3.0731 , 'height8': 3.0751,
                               'height9': 3.169,'height10': 2.882,'height11': 3.0429,'height12': 3.205,'height13': 3.0751 , 'height14': 0.0751]

        lotTactTimeList.add(lotTactTimeMap1)
        lotTactTimeList.add(lotTactTimeMap2)
        lotTactTimeList.add(lotTactTimeMap3)
        lotTactTimeList.add(lotTactTimeMap4)
        lotTactTimeList.add(lotTactTimeMap5)
        lotTactTimeList.add(lotTactTimeMap6)
        lotTactTimeList.add(lotTactTimeMap7)
        lotTactTimeList.add(lotTactTimeMap8)
        lotTactTimeList.add(lotTactTimeMap9)
        lotTactTimeList.add(lotTactTimeMap10)

        lotTactTimeList
    }


    static  ArrayList odfApdDataList(){
        def now = new Date()
        def odfApdDataList = []

        def odfApdDataMap1 = ['eventTime': '2016-05-04 10:05:32', 'lotid': 'lot001', 'glassid': 'glass001', 'lineid': 'line001', 'eqpid': 'eqp001',
                            'unitNo': 'unit001','recipeNo': 'recipe001','vacTime': '2016-05-04 10:05:32' ,'dpTime':'2016-05-04 10:05:32']
        def odfApdDataMap2 = ['eventTime': '2016-05-04 10:05:32', 'lotid': 'lot002', 'glassid': 'glass002', 'lineid': 'line002', 'eqpid': 'eqp002',
                              'unitNo': 'unit002','recipeNo': 'recipe002','vacTime': '2016-05-04 10:05:32' ,'dpTime':'2016-05-04 10:05:32']
        def odfApdDataMap3 =  ['eventTime': '2016-05-04 10:05:32', 'lotid': 'lot003', 'glassid': 'glass003', 'lineid': 'line003', 'eqpid': 'eqp003',
                               'unitNo': 'unit003','recipeNo': 'recipe003','vacTime': '2016-05-04 10:05:32' ,'dpTime':'2016-05-04 10:05:32']
        def odfApdDataMap4 =  ['eventTime': '2016-05-04 10:05:32', 'lotid': 'lot004', 'glassid': 'glass004', 'lineid': 'line004', 'eqpid': 'eqp004',
                               'unitNo': 'unit004','recipeNo': 'recipe004','vacTime': '2016-05-04 10:05:32' ,'dpTime':'2016-05-04 10:05:32']

        odfApdDataList.add(odfApdDataMap1)
        odfApdDataList.add(odfApdDataMap2)
        odfApdDataList.add(odfApdDataMap3)
        odfApdDataList.add(odfApdDataMap4)

        odfApdDataList
    }


    static  ArrayList odfAlarmList(){
        def odfAlarmList = []

        def odfAlarmMap1 = [ 'Line No': 'line001', 'Eqp Id': 'eqp001','Alarm Code': 'code01','Alarm Text': '报警内容','Alarm Type': '报警类型' ,
                             'eventTime': '2016-05-04 10:05:32', 'Glass Id In Unit': 'glass001']
        def odfAlarmMap2 = [ 'Line No': 'line002', 'Eqp Id': 'eqp002','Alarm Code': 'code02','Alarm Text': '报警内容','Alarm Type': '报警类型' ,
                             'eventTime': '2016-05-04 10:05:32', 'Glass Id In Unit': 'glass002']
        def odfAlarmMap3 = [ 'Line No': 'line003', 'Eqp Id': 'eqp003','Alarm Code': 'code03','Alarm Text': '报警内容','Alarm Type': '报警类型' ,
                             'eventTime': '2016-05-04 10:05:32', 'Glass Id In Unit': 'glass003']
        def odfAlarmMap4 = [ 'Line No': 'line004', 'Eqp Id': 'eqp004','Alarm Code': 'code04','Alarm Text': '报警内容','Alarm Type': '报警类型' ,
                             'eventTime': '2016-05-04 10:05:32', 'Glass Id In Unit': 'glass004']
        def odfAlarmMap5 = [ 'Line No': 'line005', 'Eqp Id': 'eqp005','Alarm Code': 'code05','Alarm Text': '报警内容','Alarm Type': '报警类型' ,
                             'eventTime': '2016-05-04 10:05:32', 'Glass Id In Unit': 'glass005']

        odfAlarmList.add(odfAlarmMap1)
        odfAlarmList.add(odfAlarmMap2)
        odfAlarmList.add(odfAlarmMap3)
        odfAlarmList.add(odfAlarmMap4)
        odfAlarmList.add(odfAlarmMap5)

        odfAlarmList
    }

    static  ArrayList odfVasParamRealList(){
        def odfVasParamRealList = []

        def odfVasParamRealMap1 = [ ' ': '运转Recipe', 'VAS01': 1.1,'VAS02': 1.2,'VAS03':1.3,'VAS04': 1.4 ,
                             'VAS05': 1.5, 'VAS06': 1.6,'VAS07': 1.7, 'VAS08':1.8]
        def odfVasParamRealMap2 = [ ' ': '真空度', 'VAS01': 2.1,'VAS02': 2.2,'VAS03':2.3,'VAS04': 2.4 ,
                                    'VAS05': 2.5, 'VAS06': 2.6,'VAS07': 2.7, 'VAS08':2.8]
        def odfVasParamRealMap3 = [ ' ': '速度切替位置移动压力', 'VAS01': 3.1,'VAS02': 3.2,'VAS03':3.3,'VAS04': 3.4 ,
                                    'VAS05': 3.5, 'VAS06': 3.6,'VAS07': 3.7, 'VAS08':3.8]
        def odfVasParamRealMap4 = [ ' ': 'DF 中央->外周Delay', 'VAS01': 4.1,'VAS02': 4.2,'VAS03':4.3,'VAS04': 4.4 ,
                                    'VAS05':4.5, 'VAS06': 4.6,'VAS07': 4.7, 'VAS08':4.8]
        def odfVasParamRealMap5 = [ ' ': 'DF ON安定', 'VAS01': 5.1,'VAS02':5.2,'VAS03':5.3,'VAS04': 5.4 ,
                                    'VAS05':5.5, 'VAS06':5.6,'VAS07':5.7, 'VAS08':5.8]
        def odfVasParamRealMap6 = [ ' ': '大气开放前安定时间', 'VAS01': 6.1,'VAS02': 6.2,'VAS03':6.3,'VAS04':6.4 ,
                                    'VAS05': 6.5, 'VAS06': 6.6,'VAS07': 6.7, 'VAS08':6.8]
        def odfVasParamRealMap7 = [ ' ': 'TIME B', 'VAS01': 7.1,'VAS02':7.2,'VAS03':7.3,'VAS04': 7.4 ,
                                    'VAS05': 7.5, 'VAS06':7.6,'VAS07':7.7, 'VAS08':7.8]
        def odfVasParamRealMap8 = [ ' ': 'TIME C', 'VAS01': 8.1,'VAS02': 8.2,'VAS03':8.3,'VAS04': 8.4 ,
                                    'VAS05':8.5, 'VAS06': 8.6,'VAS07': 8.7, 'VAS08':8.8]
        def odfVasParamRealMap9 = [ ' ': 'TIME D', 'VAS01':9.1,'VAS02':9.2,'VAS03':9.3,'VAS04':9.4 ,
                                    'VAS05': 9.5, 'VAS06': 9.6,'VAS07':9.7, 'VAS08':9.8]

        odfVasParamRealList.add(odfVasParamRealMap1)
        odfVasParamRealList.add(odfVasParamRealMap2)
        odfVasParamRealList.add(odfVasParamRealMap3)
        odfVasParamRealList.add(odfVasParamRealMap4)
        odfVasParamRealList.add(odfVasParamRealMap5)
        odfVasParamRealList.add(odfVasParamRealMap6)
        odfVasParamRealList.add(odfVasParamRealMap7)
        odfVasParamRealList.add(odfVasParamRealMap8)
        odfVasParamRealList.add(odfVasParamRealMap9)

        odfVasParamRealList
    }



    static  ArrayList odfRealtimeMonitorList(){
        def now = new Date()
        def odfRealtimeMonitorList = []

        def odfRealtimeMonitorMap1 = ['Line No': 'line001', 'Eqp Id': 'eqp001','Mask Id': 'mask01','Update Time': '2016-05-05 10:51:03']
        def odfRealtimeMonitorMap2 = ['Line No': 'line002', 'Eqp Id': 'eqp002','Mask Id': 'mask02','Update Time': '2016-05-05 10:52:03']

        odfRealtimeMonitorList.add(odfRealtimeMonitorMap1)
        odfRealtimeMonitorList.add(odfRealtimeMonitorMap2)

        odfRealtimeMonitorList
    }


    static  ArrayList deviceCycleTimeList() {
        def deviceCycleTimeList = []

        def deviceCycleTimeMap1 = ['Line No': 'line001', 'Eqp Id': 'eqp001','VAG': 21.2, 'MAX': 32.2,'MIN': 11.2];
        def deviceCycleTimeMap2 = ['Line No': 'line001', 'Eqp Id': 'eqp001','VAG': 21.2, 'MAX': 32.2,'MIN': 11.2];
        def deviceCycleTimeMap3 = ['Line No': 'line001', 'Eqp Id': 'eqp001','VAG': 21.2, 'MAX': 32.2,'MIN': 11.2];
        def deviceCycleTimeMap4 = ['Line No': 'line001', 'Eqp Id': 'eqp001','VAG': 21.2, 'MAX': 32.2,'MIN': 11.2];

        deviceCycleTimeList.add(deviceCycleTimeMap1)
        deviceCycleTimeList.add(deviceCycleTimeMap2)
        deviceCycleTimeList.add(deviceCycleTimeMap3)
        deviceCycleTimeList.add(deviceCycleTimeMap4)

        deviceCycleTimeList
    }

    static ArrayList odfAiLegendLsit(){
        def odfAiLegendLsit = []

        odfAiLegendLsit.add('GLASS_1X')
        odfAiLegendLsit.add('GLASS_1Y')
        odfAiLegendLsit.add('GLASS_2X')
        odfAiLegendLsit.add('GLASS_2Y')
        odfAiLegendLsit.add('GLASS_3X')
        odfAiLegendLsit.add('GLASS_3Y')
        odfAiLegendLsit.add('GLASS_4X')
        odfAiLegendLsit.add('GLASS_4Y')

        odfAiLegendLsit
    }


    static  ArrayList rubbingEqpList(){
        def rubbingEqpList = []

        def rubbingEqpMap1 = ['equipment_id': 'RB01']
        def rubbingEqpMap2 = ['equipment_id': 'RB02']

        rubbingEqpList.add(rubbingEqpMap1)
        rubbingEqpList.add(rubbingEqpMap2)

        rubbingEqpList
    }

    static  ArrayList metadataList() {
        def metadataList = []

        def metadataMap1 = [glassid: '8ALL620729B5', 'SEAL_SECTION_CHECK_A1': 3.9422,'SEAL_SECTION_CHECK_A2': 4.8401,'SEAL_SECTION_CHECK_A3': 3.8371,
                            'SEAL_SECTION_CHECK_A4': 3.8381,'SEAL_SECTION_CHECK_A5': 3.8381,'SEAL_SECTION_CHECK_A6': 3.8391,
                            'SEAL_SECTION_CHECK_A7': 3.8361,'SEAL_SECTION_CHECK_A8': 3.8391,'SEAL_SECTION_CHECK_B1': 3.8421,
                            'SEAL_SECTION_CHECK_B2': 3.8371,'SEAL_SECTION_CHECK_B3': 3.8371,'SEAL_SECTION_CHECK_B4': 3.8421,
                            'SEAL_SECTION_CHECK_B5': 3.8371,'SEAL_SECTION_CHECK_B6': 3.8371,'SEAL_SECTION_CHECK_B7': 3.8421,'SEAL_SECTION_CHECK_B8': 3.8421];
        def metadataMap2 = [glassid: '8ALL620729B5', 'SEAL_SECTION_CHECK_A1': 3.8421,'SEAL_SECTION_CHECK_A2': 3.8402,'SEAL_SECTION_CHECK_A3': 3.8372,
                            'SEAL_SECTION_CHECK_A4': 3.8382,'SEAL_SECTION_CHECK_A5': 3.8382,'SEAL_SECTION_CHECK_A6': 3.8392,
                            'SEAL_SECTION_CHECK_A7': 3.8362,'SEAL_SECTION_CHECK_A8': 3.8392,'SEAL_SECTION_CHECK_B1': 3.8422,
                            'SEAL_SECTION_CHECK_B2': 3.8372,'SEAL_SECTION_CHECK_B3': 3.8372,'SEAL_SECTION_CHECK_B4': 3.8422,
                            'SEAL_SECTION_CHECK_B5': 3.8371,'SEAL_SECTION_CHECK_B6': 3.8371,'SEAL_SECTION_CHECK_B7': 3.8421,'SEAL_SECTION_CHECK_B8': 3.8421];
        def metadataMap3 = [glassid: '8ALL620729B5', 'SEAL_SECTION_CHECK_A1': 3.8422,'SEAL_SECTION_CHECK_A2': 3.8403,'SEAL_SECTION_CHECK_A3': 3.8373,
                            'SEAL_SECTION_CHECK_A4': 3.8383,'SEAL_SECTION_CHECK_A5': 3.8383,'SEAL_SECTION_CHECK_A6': 3.8393,
                            'SEAL_SECTION_CHECK_A7': 3.8363,'SEAL_SECTION_CHECK_A8': 3.8393,'SEAL_SECTION_CHECK_B1': 3.8423,
                            'SEAL_SECTION_CHECK_B2': 3.8373,'SEAL_SECTION_CHECK_B3': 3.8373,'SEAL_SECTION_CHECK_B4': 3.8423,
                            'SEAL_SECTION_CHECK_B5': 3.8371,'SEAL_SECTION_CHECK_B6': 3.8371,'SEAL_SECTION_CHECK_B7': 3.8421,'SEAL_SECTION_CHECK_B8': 3.8421];
        def metadataMap4 = [glassid: '8ALL620729B5', 'SEAL_SECTION_CHECK_A1': 3.8423,'SEAL_SECTION_CHECK_A2': 3.8404,'SEAL_SECTION_CHECK_A3': 3.8374,
                            'SEAL_SECTION_CHECK_A4': 3.8384,'SEAL_SECTION_CHECK_A5': 3.8384,'SEAL_SECTION_CHECK_A6': 3.8394,
                            'SEAL_SECTION_CHECK_A7': 3.8364,'SEAL_SECTION_CHECK_A8': 3.8394,'SEAL_SECTION_CHECK_B1': 3.8424,
                            'SEAL_SECTION_CHECK_B2': 3.8374,'SEAL_SECTION_CHECK_B3': 3.8374,'SEAL_SECTION_CHECK_B4': 3.8424,
                            'SEAL_SECTION_CHECK_B5': 3.8371,'SEAL_SECTION_CHECK_B6': 3.8371,'SEAL_SECTION_CHECK_B7': 3.8421,'SEAL_SECTION_CHECK_B8': 3.8421];


//        def metadataMap1 = [glassid: '8ALL620729B5', '19  LC_H_LMW': 3.9422,'20  LC_H_LMW': 4.8401];
//        def metadataMap2 = [glassid: '8ALL620729B6', '19  LC_H_LMW': 3.9812,'20  LC_H_LMW': 3.8491];
//        def metadataMap3 = [glassid: '8ALL620729B7', '19  LC_H_LMW': 3.2420,'20  LC_H_LMW': 2.8701];
//        def metadataMap4 = [glassid: '8ALL620729B8', '19  LC_H_LMW': 3.1422,'20  LC_H_LMW': 1.8301];

        metadataList.add(metadataMap1)
        metadataList.add(metadataMap2)
        metadataList.add(metadataMap3)
        metadataList.add(metadataMap4)

        metadataList
    }

}
