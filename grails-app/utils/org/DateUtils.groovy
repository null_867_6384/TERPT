package org

import java.text.SimpleDateFormat

class DateUtils {
    static getDateOfWeek(Date date) {
        def c = Calendar.instance
        if (date) {
            c.time = date
        }
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return '星期天'
            case 2:
                return '星期一'
            case 3:
                return '星期二'
            case 4:
                return '星期三'
            case 5:
                return '星期四'
            case 6:
                return '星期五'
            case 7:
                return '星期六'
        }
    }


    static Date getFormDate(Date clearDate, String remark) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(clearDate);

        if (remark == 'start') {
            cal.add(Calendar.DATE, -1);
            cal.add(Calendar.HOUR_OF_DAY, 6);
        } else {
            cal.add(Calendar.HOUR_OF_DAY, 6);
        }
        return cal.getTime();
    }

    static List<String> getHourList(Date startTime, Date endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd HH')

        def strHourList = []
        strHourList.add(sdf.format(startTime))

        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startTime)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endTime)

        while (endTime.after(calBegin.getTime())) {
            calBegin.add(Calendar.HOUR_OF_DAY, 1)
            strHourList.add(sdf.format(calBegin.getTime()))
        }

        return strHourList;

    }


    static List<String> getDayList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strDateList = []
        strDateList.add(sdf.format(startDate))
        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startDate)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.DAY_OF_MONTH, 1)
            strDateList.add(sdf.format(calBegin.getTime()))
        }

        return strDateList;

    }

    static List<String> getWeekList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strWeekList = []

        Calendar calBegin = Calendar.getInstance()
        calBegin.setFirstDayOfWeek(Calendar.MONDAY);
        calBegin.setTime(startDate)

        strWeekList.add(calBegin.get(Calendar.WEEK_OF_YEAR).toString())

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.WEEK_OF_YEAR, 1)
            strWeekList.add(calBegin.get(Calendar.WEEK_OF_YEAR).toString())
        }


        return strWeekList;

    }

    static List<String> getYearWeekList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strWeekList = []

        Calendar calBegin = Calendar.getInstance()
        calBegin.setFirstDayOfWeek(Calendar.MONDAY);
        calBegin.setTime(startDate)

        strWeekList.add(calBegin.get(Calendar.YEAR).toString() + ":" + calBegin.get(Calendar.WEEK_OF_YEAR).toString())

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.WEEK_OF_YEAR, 1)
            strWeekList.add(calBegin.get(Calendar.YEAR).toString() + ":" + calBegin.get(Calendar.WEEK_OF_YEAR).toString())
        }


        return strWeekList;

    }

    static List<String> getMonthList(Date startMonth, Date endMonth) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')

        def strMonthList = []
        strMonthList.add(sdf.format(startMonth))

        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startMonth)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endMonth)

        while (endMonth.after(calBegin.getTime())) {
            calBegin.add(Calendar.MONTH, 1)
            strMonthList.add(sdf.format(calBegin.getTime()))
        }

        return strMonthList;

    }
}
