package org

import java.text.DecimalFormat

/**
* 数字处理的工具类
*/
class NumberUtils {

	/**
	* 0、1组成的Byte 16位字符串-》整型,
	*/
    static ByteToInt(String byteStr){
        int result=0
		if(!byteStr || byteStr=='' || byteStr=='0' || byteStr=='null' || byteStr.size()!=16){
             result
        }else {				
            def size=16
            for(int i=0;i<size;i++){
                if(byteStr[i].toInteger()!=0 && byteStr[i].toInteger()!=1){
                    result=0
                    break
                }else{
                    def mul=1
                    if(i!=0){
                        if(byteStr[size-i-1].toInteger()!=0){
                            (1..i).each{						   
                                mul*=2
                            }
                        }
                    }
                     result+=byteStr[size-i-1].toInteger()*mul
                 }
            }
            return result
        }
    }
    
    
	/**
	* 整型-》Byte字符串
	*/
	static  IntToByte(int value){

		if (value==0)
			 ''
		else{
			def size=16
			def charBits =new StringBuffer()
			for   (int   i   =  0; i< size;   i++)   {
				 charBits.append((value   &   1)   ==   0   ?   '0'   :   '1' )
				 value   >>= 1
			}  
			return charBits.reverse().toString()
		}
			
	}
	
	/**
	* Byte字符串-》整型, '0;1;2'
	*/
	static ByteStringToInt(String value){
        
		def sum=0
        if(value){
            value.split(';').each{eachValue ->
               sum+=2**(eachValue as int)
            }
        }
         sum
	}
	
	/**
	* String-》Byte字符串
	*/
	static String StringToByteString(String value){
		StringToByteArray(value).join('') 		
	}
	
	/**
	* String-》List,例如'0;1;3;6'-》
	*/
	static ArrayList StringToByteArray(String value){		
		if(!value || value==''){
			 []
		}else{
			def mask=[]
			(0..15).each{
				mask<<0
			}
			
			value.split(';').eachWithIndex{eachValue,index ->
				mask[15-(eachValue as int)]=1
			}
			return mask
		}
	}
	
	/**
	* String-》ByteString
	*/
	static String StringToByteString(String value, String name){
		StringToByteArray(value,name).join(' ') 		
	}
	
	/**
	* String-》List
	*/
	static ArrayList StringToByteArray(String value, String name){
		if(!value || value==''){
			 []
		}else{
			def mask=[]
			(0..15).each{
				mask<<0
			}
			
			value.split(';').eachWithIndex{eachValue,index ->
				mask[15-(eachValue as int)]=name.split(';')[index]
			}
			return mask
		}
	}
	
    static  exchange(float slope,float shift,double value,int round){
		float result=value
		if(slope!=null && shift!=null){
			result=value*slope+shift
		}
         Math.round(result*(10**round))/(10**round)
	}
    
    static  exchange(float slope,float shift,float value,int round){
		float result=value
		if(slope!=null && shift!=null){
			result=value*slope+shift
		}
         Math.round(result*(10**round))/(10**round)
	}
	/**
	* 数字Long转换成时间间隔
	* @param
	*/
	static String longToDiff(Long value){
		long difference = value/1000
		def seconds    =  difference % 60
		difference = (difference - seconds) / 60
		def minutes    =  difference % 60
		difference = (difference - minutes) / 60
		def hours      =  difference % 24
		difference = (difference - hours)   / 24
		def days       =  difference % 7
		def weeks      = (difference - days)    /  7
		 "${weeks==0?'':weeks+'周 '}${days==0?'':days+'天 '}${hours==0?'':hours+'小时 '}${minutes==0?'':minutes+'分 '}${seconds==0?'':seconds+'秒'}"
	}
	/**
	* 数字Long转换成时间间隔
	* @param
	*/
	static String secondToDiff(Long value){
		long difference = value
		def seconds    =  difference % 60
		difference = (difference - seconds) / 60
		def minutes    =  difference % 60
		difference = (difference - minutes) / 60
		def hours      =  difference % 24
		difference = (difference - hours)   / 24
		def days       =  difference % 7
		def weeks      = (difference - days)    /  7
		 "${weeks==0?'':weeks+'周 '}${days==0?'':days+'天 '}${hours==0?'':hours+'小时 '}${minutes==0?'':minutes+'分 '}${seconds==0?'':seconds+'秒'}"
	}
    
    static String genRandomPwd(int pwd_len){
        int maxNum = 65;
        int i;  //生成的随机数
        int count = 0; //生成的密码的长度
        def str = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
        'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
        'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' 
        ,'$','%','#','&'
        ,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y'];
      
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while(count < pwd_len){
            i = Math.abs(r.nextInt(maxNum)); 
            if (i >= 0 && i < str.size()) {
                pwd.append(str[i]);
                count ++;
            }
        }
        pwd.toString();
    }
    /**
     * 判断字符串是否不为空
     * @param str
     * @return
     */
    static boolean isNotEmpty(String str){
        if(!str || str==''){
            return false
        }
        return true
    }

    static double getStandardDiviation(ArrayList array,double avg) {
        DecimalFormat df   = new DecimalFormat("##0.0000");
        return Double.parseDouble(df.format(Math.sqrt(Math.abs(avg))));
    }

    static double dealWithPoint(double avg) {
        DecimalFormat df   = new DecimalFormat("####0.0000");
        return Double.parseDouble(df.format(avg));
    }

}
