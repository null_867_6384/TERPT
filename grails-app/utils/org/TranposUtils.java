package org;

import java.util.*;

/*import org.robert.db.bean.UserBean;
import org.robert.ibatis.util.RequestToHashMap;*/

public class TranposUtils {

    /**
     * +------------------------------------------------------------------------
     * ---------------------------------------------------+ | Date | department
     * | Author | Version | Description |
     * |===========+===========+==============
     * +================+=================
     * =====+============================================| | 2013-06-19 |
     * GloryIT | Robert Wang | 1.0 | initialization |
     * +--------------------------
     * ------------------------------------------------
     * --------------------------------------------------+
     */

    /**
     * 列传行
     *
     * @param title
     * @param value
     * @param al
     * @return
     */
    public static ArrayList<HashMap> colToRow(String title, String value,
                                              ArrayList<Map> al) {
        //定义一个MAP
        HashMap<String, Integer> sortRecode = new HashMap<String, Integer>();
        int rows = 0;
        ArrayList<String> headList = new ArrayList<String>();
        ArrayList<HashMap> resultList = new ArrayList<HashMap>();
        boolean readHead = true;
        for (Map mp : al) {
            //readHead为True执行下步
            if (readHead) {
                for (Iterator s = mp.keySet().iterator(); s.hasNext(); ) {
                    String temp = s.next().toString();
                    if (!temp.equalsIgnoreCase(title)) {
                        if (!temp.equalsIgnoreCase(value)) {
                            headList.add(temp);
                        }
                    }
                }
                readHead = false;
            }

            String key = "";
            HashMap tempMap = new HashMap();
            for (String p : headList) {
                key = key + mp.get(p);
                tempMap.put(p, mp.get(p));
            }
            if (sortRecode.containsKey(key)) {
                resultList.get(sortRecode.get(key)).put(mp.get(title),
                        mp.get(value));
            } else {
                tempMap.put(mp.get(title), mp.get(value));
                resultList.add(tempMap);
                sortRecode.put(key, new Integer(rows));
                rows++;
            }
        }

        return resultList;
    }

    @SuppressWarnings("rawtypes")
    /*public static HashMap generMap(HttpServletRequest request)
    {
		HashMap paramMAP = RequestToHashMap.toHashMap(request);
		paramMAP.put("SITE_ID",
				((UserBean) request.getSession().getAttribute("USER_INFO"))
						.getSiteId());

		String showStyle = (String) paramMAP.get("showStyle");
		String tableUrl = (String) paramMAP.get("TableRUL");

		if (!paramMAP.containsKey("datasource"))
		{
			paramMAP.put("datasource", "sqlMapClient");
		}

		if (tableUrl == null || tableUrl.length() == 0)
		{
			paramMAP.put("TableRUL", "CommonSingleTable");
		}

		if (showStyle == null || showStyle.length() == 0)
		{
			paramMAP.put("showStyle", "showData");
		}if(paramMAP.containsKey("ScriptFile"))
        {
            request.setAttribute("javascriptAction",paramMAP.get("ScriptFile").toString());
        }
		request.setAttribute("TableRUL", tableUrl +".ftl");//assign display view
        request.setAttribute("export_URL",request.getRequestURL());
		return paramMAP;

	}*/

    public static void main(String[] args) {
        HashMap hm = new HashMap();
        ArrayList<Map> al = new ArrayList<Map>();
        hm.put("姓名", "张三");
        hm.put("学校", "中国大学");
        hm.put("成绩", "80");
        hm.put("性别", "女");
        al.add(hm);
        hm = new HashMap();
        hm.put("姓名", "李四");
        hm.put("学校", "中国大学");
        hm.put("成绩", "89");
        hm.put("性别", "男");
        al.add(hm);
        hm = new HashMap();
        hm.put("姓名", "王二麻子");
        hm.put("学校", "中国大学");
        hm.put("成绩", "60");
        hm.put("性别", "女");
        al.add(hm);
        System.out.println(al);
        System.out.println(TranposUtils.colToRow("姓名", "成绩", al));
        for (HashMap mp : TranposUtils.colToRow("姓名", "成绩", al)) {
            System.out.println(mp);
        }

    }

    /**
     * 多列转行
     *
     * @param title
     * @param al`
     * @param groupId
     * @return
     */
    public static ArrayList<HashMap> groupTable(List<String> title, List<Map> al, List<String> groupId) {

        LinkedHashMap<String, Integer> sortRecode = new LinkedHashMap<String, Integer>();
        int rows = 0;
        HashSet titleSet = new HashSet();
        ArrayList<String> headList = new ArrayList<String>();
        ArrayList<HashMap> resultList = new ArrayList<HashMap>();
        boolean readHead = true;

        for (String t : title) {
            titleSet.add(t);
        }

        for (Map mp : al) {
            if (readHead) {
                for (Iterator s = mp.keySet().iterator(); s.hasNext(); ) {
                    String temp = s.next().toString();
                    if (!titleSet.contains(temp)) {
                        headList.add(temp);
                    }
                }
                readHead = false;
            }

            String key = "";
            LinkedHashMap tempMap = new LinkedHashMap();
            for (String p : headList) {
                key = key + mp.get(p);
                tempMap.put(p, mp.get(p));
            }
            String tempGroup = "";
            if (sortRecode.containsKey(key)) {
                if (groupId != null) {
                    for (String g : groupId) {
                        tempGroup = tempGroup + mp.get(g);
                    }
                }

                for (int i = 0; i < title.size(); i++) {
                    if (mp.get(title.get(i)) != null)
                        resultList.get(sortRecode.get(key)).put(tempGroup + "|" + title.get(i),
                                mp.get(title.get(i)));
                    mp.remove("title.get(i)");
                }


            } else {
                if (groupId != null) {
                    for (String g : groupId) {
                        tempGroup = tempGroup + mp.get(g);
                    }
                }

                for (int i = 0; i < title.size(); i++) {
                    if (mp.get(title.get(i)) != null)
                        tempMap.put(tempGroup + "|" + title.get(i), mp.get(title.get(i)));
                    mp.remove("title.get(i)");
                }
                resultList.add(tempMap);
                sortRecode.put(key, new Integer(rows));
                rows++;
            }
        }

        return resultList;
    }
}
