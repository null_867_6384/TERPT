package kdx

import java.text.SimpleDateFormat

class DateUtils {

    static List<String> getHourList(Date startTime, Date endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd HH')

        def strHourList =  []
        strHourList.add(sdf.format(startTime))

        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startTime)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endTime)

        while (endTime.after(calBegin.getTime())) {
            calBegin.add(Calendar.HOUR_OF_DAY, 1)
            strHourList.add(sdf.format(calBegin.getTime()))
        }

        return strHourList;

    }


    static List<String> getDayList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strDateList =  []
        strDateList.add(sdf.format(startDate))
        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startDate)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.DAY_OF_MONTH, 1)
            strDateList.add(sdf.format(calBegin.getTime()))
        }

        return strDateList;

    }

    static List<String> getWeekList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strWeekList =  []

        Calendar calBegin = Calendar.getInstance()
        calBegin.setFirstDayOfWeek(Calendar.MONDAY);
        calBegin.setTime(startDate)

        strWeekList.add(calBegin.get(Calendar.WEEK_OF_YEAR).toString())

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.WEEK_OF_YEAR, 1)
            strWeekList.add(calBegin.get(Calendar.WEEK_OF_YEAR).toString())
        }


        return strWeekList;

    }

    static List<String> getYearWeekList(Date startDate, Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')

        def strWeekList =  []

        Calendar calBegin = Calendar.getInstance()
        calBegin.setFirstDayOfWeek(Calendar.MONDAY);
        calBegin.setTime(startDate)

        strWeekList.add(calBegin.get(Calendar.YEAR).toString()+":"+calBegin.get(Calendar.WEEK_OF_YEAR).toString())

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endDate)

        while (endDate.after(calBegin.getTime())) {
            calBegin.add(Calendar.WEEK_OF_YEAR, 1)
            strWeekList.add(calBegin.get(Calendar.YEAR).toString()+":"+calBegin.get(Calendar.WEEK_OF_YEAR).toString())
        }


        return strWeekList;

    }

    static List<String> getMonthList(Date startMonth, Date endMonth) {
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')

        def strMonthList =  []
        strMonthList.add(sdf.format(startMonth))

        Calendar calBegin = Calendar.getInstance()
        calBegin.setTime(startMonth)

        Calendar calEnd = Calendar.getInstance()
        calEnd.setTime(endMonth)

        while (endMonth.after(calBegin.getTime())) {
            calBegin.add(Calendar.MONTH, 1)
            strMonthList.add(sdf.format(calBegin.getTime()))
        }

        return strMonthList;

    }

}
