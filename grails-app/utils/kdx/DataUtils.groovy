package kdx

/**
 * 测试数据提供工具
 */
class DataUtils {

    static ArrayList getEquipmentList() {
        def now = new Date()
        def equipmentList = []

        def equipmentMap1 = ['id': 1, 'equipmentGroup': '设备组一', 'vender': '供应商一', 'department': '部门一', 'lockVersion': 1.0, 'created': now,  'location': '江苏省张家港市']
        def equipmentMap2 = ['id': 2, 'equipmentGroup': '设备组二', 'vender': '供应商二', 'department': '部门二', 'lockVersion': 2.0, 'created': now, 'type': '类型二', 'location': '江苏省张家港市']
        def equipmentMap3 = ['id': 3, 'equipmentGroup': '设备组三', 'vender': '供应商三', 'department': '部门三', 'lockVersion': 3.0, 'created': now, 'type': '类型三', 'location': '江苏省张家港市']
        def equipmentMap4 = ['id': 4, 'equipmentGroup': '设备组四', 'vender': '供应商四', 'department': '部门四', 'lockVersion': 4.0, 'created': now, 'type': '类型四', 'location': '江苏省张家港市']
        def equipmentMap5 = ['id': 5, 'equipmentGroup': '设备组五', 'vender': '供应商五', 'department': '部门五', 'lockVersion': 5.0, 'created': now, 'type': '类型五', 'location': '江苏省张家港市']


        equipmentList
    }


    static ArrayList getCellList() {
        def now = new Date()
        def cellList = []

        def cellMap1 = ['id': 1, 'org': '区域一', 'lot': 'lot000001', 'plate': 'glass000001', 'isActive': true]
        def cellMap2 = ['id': 2, 'org': '区域二', 'lot': 'lot000002', 'plate': 'glass000002', 'isActive': false]
        def cellMap3 = ['id': 3, 'org': '区域三', 'lot': 'lot000003', 'plate': 'glass000003', 'isActive': true]
        def cellMap4 = ['id': 4, 'org': '区域四', 'lot': 'lot000004', 'plate': 'glass000004', 'isActive': true]
        def cellMap5 = ['id': 5, 'org': '区域五', 'lot': 'lot000005', 'plate': 'glass000005', 'isActive': true]

        cellList.add(cellMap1)
        cellList.add(cellMap2)
        cellList.add(cellMap3)
        cellList.add(cellMap4)
        cellList.add(cellMap5)

        cellList
    }

    static ArrayList getStepList() {
        def now = new Date()
        def stepList = []

        def stepMap1 = ['id': 1, 'stepname': '工序一', 'version': '版本一', 'created': now]
        def stepMap2 = ['id': 2, 'stepname': '工序二', 'version': '版本二', 'created': now]
        def stepMap3 = ['id': 3, 'stepname': '工序三', 'version': '版本三', 'created': now]
        def stepMap4 = ['id': 4, 'stepname': '工序四', 'version': '版本三', 'created': now]
        def stepMap5 = ['id': 5, 'stepname': '工序五', 'version': '版本三', 'created': now]
        def stepMap6 = ['id': 6, 'stepname': '工序六', 'version': '版本二', 'created': now]
        def stepMap7 = ['id': 7, 'stepname': '工序七', 'version': '版本三', 'created': now]
        def stepMap8 = ['id': 8, 'stepname': '工序八', 'version': '版本二', 'created': now]
        def stepMap9 = ['id': 9, 'stepname': '工序九', 'version': '版本一', 'created': now]
        def stepMap10 = ['id': 10, 'stepname': '工序十', 'version': '版本一', 'created': now]

        stepList.add(stepMap1)
        stepList.add(stepMap2)
        stepList.add(stepMap3)
        stepList.add(stepMap4)
        stepList.add(stepMap5)
        stepList.add(stepMap6)
        stepList.add(stepMap7)
        stepList.add(stepMap8)
        stepList.add(stepMap9)
        stepList.add(stepMap10)

        stepList
    }

    static ArrayList getMaterialList() {
        def now = new Date()
        def materialList = []

        def materialMap1 = ['id'   : 1, 'eqpId': 'FRS-11N', 'recipeId': 501, 'productId': 'CIA', 'productName': 'FLP00001-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00001', 'seqNo': 31, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap2 = ['id'   : 2, 'eqpId': 'FRS-12N', 'recipeId': 502, 'productId': 'CIA', 'productName': 'FLP00002-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00002', 'seqNo': 32, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap3 = ['id'   : 3, 'eqpId': 'FRS-13N', 'recipeId': 503, 'productId': 'CIA', 'productName': 'FLP00003-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00003', 'seqNo': 33, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap4 = ['id'   : 4, 'eqpId': 'FRS-14N', 'recipeId': 504, 'productId': 'CIA', 'productName': 'FLP00004-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00004', 'seqNo': 34, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap5 = ['id'   : 5, 'eqpId': 'FRS-15N', 'recipeId': 505, 'productId': 'CIA', 'productName': 'FLP00005-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00005', 'seqNo': 35, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap6 = ['id'   : 6, 'eqpId': 'FRS-16N', 'recipeId': 506, 'productId': 'CIA', 'productName': 'FLP00006-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00006', 'seqNo': 36, 'isActive': false, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap7 = ['id'   : 7, 'eqpId': 'FRS-17N', 'recipeId': 507, 'productId': 'CIA', 'productName': 'FLP00007-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00007', 'seqNo': 37, 'isActive': false, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap8 = ['id'   : 8, 'eqpId': 'FRS-18N', 'recipeId': 508, 'productId': 'CIA', 'productName': 'FLP00008-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00008', 'seqNo': 38, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap9 = ['id'   : 6, 'eqpId': 'FRS-16N', 'recipeId': 506, 'productId': 'CIA', 'productName': 'FLP00006-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00006', 'seqNo': 36, 'isActive': false, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap10 = ['id'   : 7, 'eqpId': 'FRS-17N', 'recipeId': 507, 'productId': 'CIA', 'productName': 'FLP00007-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00007', 'seqNo': 37, 'isActive': false, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']
        def materialMap11 = ['id'   : 8, 'eqpId': 'FRS-18N', 'recipeId': 508, 'productId': 'CIA', 'productName': 'FLP00008-L8FA', 'cdProduct': '',
                            'cutId': 'CUT00008', 'seqNo': 38, 'commonText': '', 'created': now, 'updated': now, 'updater': 'MES']

        materialList.add(materialMap1)
        materialList.add(materialMap2)
        materialList.add(materialMap3)
        materialList.add(materialMap4)
        materialList.add(materialMap5)
        materialList.add(materialMap6)
        materialList.add(materialMap7)
        materialList.add(materialMap8)
        materialList.add(materialMap9)
        materialList.add(materialMap10)
        materialList.add(materialMap11)
        materialList
    }


    static int getTotalCount() {
        100
    }


    static ArrayList getHoldGlassList() {
        def now = new Date()
        def holdGlassList = []

        def holdGlassMap1 = ['id'      : 1, 'plateNo': 'FRS-11N', 'stripNo': 501, 'prcslowId': 'CIA', 'prcsNo': 'FLP00001-L8FA', 'holdType': 'BRK',
                             'holdCode': '00021', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap2 = ['id'      : 2, 'plateNo': 'FRS-12N', 'stripNo': 502, 'prcslowId': 'CIA', 'prcsNo': 'FLP00002-L8FA', 'holdType': 'BRK',
                             'holdCode': '00022', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap3 = ['id'      : 3, 'plateNo': 'FRS-13N', 'stripNo': 503, 'prcslowId': 'CIA', 'prcsNo': 'FLP00003-L8FA', 'holdType': 'BRK',
                             'holdCode': '00023', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap4 = ['id'      : 4, 'plateNo': 'FRS-14N', 'stripNo': 504, 'prcslowId': 'CIA', 'prcsNo': 'FLP00004-L8FA', 'holdType': 'BRK',
                             'holdCode': '00024', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap5 = ['id'      : 5, 'plateNo': 'FRS-15N', 'stripNo': 505, 'prcslowId': 'CIA', 'prcsNo': 'FLP00005-L8FA', 'holdType': 'BRK',
                             'holdCode': '00025', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap6 = ['id'      : 6, 'plateNo': 'FRS-16N', 'stripNo': 506, 'prcslowId': 'CIA', 'prcsNo': 'FLP00006-L8FA', 'holdType': 'BRK',
                             'holdCode': '00026', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap7 = ['id'      : 7, 'plateNo': 'FRS-17N', 'stripNo': 507, 'prcslowId': 'CIA', 'prcsNo': 'FLP00007-L8FA', 'holdType': 'BRK',
                             'holdCode': '00027', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']
        def holdGlassMap8 = ['id'      : 8, 'plateNo': 'FRS-18N', 'stripNo': 508, 'prcslowId': 'CIA', 'prcsNo': 'FLP00008-L8FA', 'holdType': 'BRK',
                             'holdCode': '00028', 'holdComment': '生产目的定义存在五小时', 'holdStamp': now, 'updater': 'MES']

        holdGlassList.add(holdGlassMap1)
        holdGlassList.add(holdGlassMap2)
        holdGlassList.add(holdGlassMap3)
        holdGlassList.add(holdGlassMap4)
        holdGlassList.add(holdGlassMap5)
        holdGlassList.add(holdGlassMap6)
        holdGlassList.add(holdGlassMap7)
        holdGlassList.add(holdGlassMap8)

        holdGlassList
    }


    static ArrayList getMoveInfoList() {
        def moveInfoList = []

        def moveInfoMap1 = ['id': 1, 'eqpId': 'FRS-11N', 'productId': 'CIA', 'productName': 'FLP00001-L8FA', 'weekNo': 'FLP00001-L8FA', 'qty': 10]
        def moveInfoMap2 = ['id': 2, 'eqpId': 'FRS-12N', 'productId': 'CIA', 'productName': 'FLP00002-L8FA', 'weekNo': 'FLP00002-L8FA', 'qty': 11]
        def moveInfoMap3 = ['id': 3, 'eqpId': 'FRS-13N', 'productId': 'CIA', 'productName': 'FLP00003-L8FA', 'weekNo': 'FLP00003-L8FA', 'qty': 12]
        def moveInfoMap4 = ['id': 4, 'eqpId': 'FRS-14N', 'productId': 'CIA', 'productName': 'FLP00004-L8FA', 'weekNo': 'FLP00004-L8FA', 'qty': 15]
        def moveInfoMap5 = ['id': 5, 'eqpId': 'FRS-15N', 'productId': 'CIA', 'productName': 'FLP00005-L8FA', 'weekNo': 'FLP00005-L8FA', 'qty': 17]

        moveInfoList.add(moveInfoMap1)
        moveInfoList.add(moveInfoMap2)
        moveInfoList.add(moveInfoMap3)
        moveInfoList.add(moveInfoMap4)
        moveInfoList.add(moveInfoMap5)

        moveInfoList
    }

    static ArrayList getProductList() {
        def productList = []

        def productMap1 = ['id': 1, 'productId': 'CIA', 'productName': 'FLP00001-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap2 = ['id': 2, 'productId': 'CIA', 'productName': 'FLP00002-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap3 = ['id': 3, 'productId': 'CIA', 'productName': 'FLP00003-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap4 = ['id': 4, 'productId': 'CIA', 'productName': 'FLP00004-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]
        def productMap5 = ['id': 5, 'productId': 'CIA', 'productName': 'FLP00005-L8FA', 'qty1': 10, 'qty2': 20, 'qty3': 30, 'qty4': 40]

        productList.add(productMap1)
        productList.add(productMap2)
        productList.add(productMap3)
        productList.add(productMap4)
        productList.add(productMap5)

        productList
    }

    static HashMap getWipProdMap() {
        def prodTypeList = ['TFT', 'CF']
        def countMap = ['beforeCell': 580, 'lms': 482, 'pi': 512, 'pDirect': 520, 'odf': 485, 'pmb': 396, 'pme': 412, 'pmb': 560, 'afterCell': 620]

        def wipProdList = []
        def data1List = []
        def data2List = []

        for (type in prodTypeList) {
            if (type.equals('TFT')) {
                data1List.add(['type': type, 'dataMap': ['beforeCell': 120, 'lms': 70, 'pi': 90, 'pDirect': 80, 'afterCell': 110]])
                data2List.add(['type': type, 'dataMap': ['beforeCell': 120, 'lms': 70, 'pi': 90, 'pDirect': 80, 'afterCell': 110]])
            } else {
                data1List.add(['type': type, 'dataMap': ['beforeCell': 90, 'lms': 80, 'pi': 92, 'pDirect': 88, 'afterCell': 121]])
                data2List.add(['type': type, 'dataMap': ['beforeCell': 90, 'lms': 80, 'pi': 92, 'pDirect': 88, 'afterCell': 121]])
            }
        }

        def productMap1 = ['id': 1, 'cellName': '24A', 'dataList': data1List]
        def productMap2 = ['id': 2, 'cellName': '41A', 'dataList': data2List]

        wipProdList.add(productMap1)
        wipProdList.add(productMap2)

        def wipProdMap = ['wipProdList': wipProdList, 'prodTypeList': prodTypeList, countMap: countMap]

        wipProdMap
    }


    static ArrayList getCassetteInfoList() {
        def now = new Date()
        def cassetteInfoList = []

        def cassetteInfoMap1 = ['id'      : 1, 'cassetteId': 'FRS-11N', 'cellId': 'FLP00001-L8FA', 'storkId': 'FLP00001-L8FA', 'pieceNumber': 10,
                                 'prodNo': 30, 'portId': 'Port00001', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap2 = ['id'      : 2, 'cassetteId': 'FRS-12N', 'cellId': 'FLP00002-L8FA', 'storkId': 'FLP00002-L8FA', 'pieceNumber': 11,
                                'type': '类型二', 'prodNo': 40, 'portId': 'Port00002', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap3 = ['id'      : 3, 'cassetteId': 'FRS-13N', 'cellId': 'FLP00003-L8FA', 'storkId': 'FLP00003-L8FA', 'pieceNumber': 12,
                                'type': '类型三', 'prodNo': 50, 'portId': 'Port00003', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap4 = ['id'      : 4, 'cassetteId': 'FRS-14N', 'cellId': 'FLP00004-L8FA', 'storkId': 'FLP00004-L8FA', 'pieceNumber': 15,
                                'isActive': false, 'type': '类型四', 'prodNo': 60, 'portId': 'Port00004', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteInfoMap5 = ['id'      : 5, 'cassetteId': 'FRS-15N', 'cellId': 'FLP00005-L8FA', 'storkId': 'FLP00005-L8FA', 'pieceNumber': 17,
                                'type': '类型五', 'prodNo': 70, 'portId': 'Port00005', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]

        cassetteInfoList.add(cassetteInfoMap1)
        cassetteInfoList.add(cassetteInfoMap2)
        cassetteInfoList.add(cassetteInfoMap3)
        cassetteInfoList.add(cassetteInfoMap4)
        cassetteInfoList.add(cassetteInfoMap5)

        cassetteInfoList
    }


    static ArrayList getCassetteTransHistoryList() {
        def now = new Date()
        def cassetteTransHistoryList = []

        def cassetteTransHistoryMap1 = ['id'      : 1, 'cassetteId': 'FRS-11N', 'cellId': 'FLP00001-L8FA', 'storkId': 'FLP00001-L8FA', 'pieceNumber': 10,
                                         'prodNo': 30, 'portId': 'Port00001', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap2 = ['id'      : 2, 'cassetteId': 'FRS-12N', 'cellId': 'FLP00002-L8FA', 'storkId': 'FLP00002-L8FA', 'pieceNumber': 11,
                                        'type': '类型二', 'prodNo': 40, 'portId': 'Port00002', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap3 = ['id'      : 3, 'cassetteId': 'FRS-13N', 'cellId': 'FLP00003-L8FA', 'storkId': 'FLP00003-L8FA', 'pieceNumber': 12,
                                        'type': '类型三', 'prodNo': 50, 'portId': 'Port00003', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap4 = ['id'      : 4, 'cassetteId': 'FRS-14N', 'cellId': 'FLP00004-L8FA', 'storkId': 'FLP00004-L8FA', 'pieceNumber': 15,
                                        'isActive': false, 'type': '类型四', 'prodNo': 60, 'portId': 'Port00004', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        def cassetteTransHistoryMap5 = ['id'      : 5, 'cassetteId': 'FRS-15N', 'cellId': 'FLP00005-L8FA', 'storkId': 'FLP00005-L8FA', 'pieceNumber': 17,
                                        'type': '类型五', 'prodNo': 70, 'portId': 'Port00005', 'holdComment': '', 'residenceTime': now, 'arrivalTime': now]
        cassetteTransHistoryList.add(cassetteTransHistoryMap1)
        cassetteTransHistoryList.add(cassetteTransHistoryMap2)
        cassetteTransHistoryList.add(cassetteTransHistoryMap3)
        cassetteTransHistoryList.add(cassetteTransHistoryMap4)
        cassetteTransHistoryList.add(cassetteTransHistoryMap5)

        cassetteTransHistoryList
    }

    static ArrayList getMoveTrendList() {
        def now = new Date()
        def moveTrendList = []

        def moveTrendMap1 = ['id'        : 1, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10001', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00001-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00001', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0001']

        def moveTrendMap2 = ['id'        : 2, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10002', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00002-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00002', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0002']

        def moveTrendMap3 = ['id'        : 3, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10003', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00003-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00003', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0003']

        def moveTrendMap4 = ['id'        : 4, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10004', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00004-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00004', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0004']

        def moveTrendMap5 = ['id'        : 5, 'shiftName': 'B', 'lotNo': 'NS90008', 'plateNo': 'CIA00001', 'stripNo': 'SN-10005', 'alignmentPlateNo': '',
                             'plateJudge': 'CIA', 'cassetteId': 'FLP00005-L8FA', 'prcsNo': '47140', 'prcsFlowId': 'TFT_FLOW', 'prcsBatchId': '15868172562', 'PrcsId': 'PE-PSP',
                             'prodName'  : '产品名称', 'portId': 'Port00005', 'aliProId': 'PLP-00001', 'eqpId': 'EQP-0005']

        moveTrendList.add(moveTrendMap1)
        moveTrendList.add(moveTrendMap2)
        moveTrendList.add(moveTrendMap3)
        moveTrendList.add(moveTrendMap4)
        moveTrendList.add(moveTrendMap5)

        moveTrendList
    }


    static ArrayList getGlassList() {
        def glassList = []
        def glassMap1 = ['id': 1, 'org': '区域一', 'lot': 'lot000001', 'cellNumber': 48, 'location': '江苏省张家港市', 'isActive': true]
        glassList
    }

    static ArrayList<Integer> getPageSizeList() {
        [10, 20, 50, 100]
    }




    static ArrayList getCassetteStatusList() {

        def cassetteStatusList = []

        def assetteStatusMap1 = [ "item": "ACT",  "num1": 'SD01',   "num2": 'XA',"num3": 148.8, "num4": 411, "num5": 21,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap2 = [ "item": "CF",   "num1": 'SD02',   "num2": 'XA',"num3": 146.9, "num4": 416, "num5": 22,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap3 = [ "item": "DMT",  "num1": 'SD03',   "num2": 'XA',"num3": 152.4, "num4": 356, "num5": 23,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap4 = [ "item": "EMP",  "num1": 'LD01',   "num2": 'XA',"num3": 156.2, "num4": 554, "num5": 24,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap5 = [ "item": "MG",   "num1": 'LD02',   "num2": 'XA',"num3": 143.0, "num4": 602, "num5": 25,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap6 = [ "item": "MUL",  "num1": 'ARC',   "num2": 'XA',"num3": 55.3, "num4": 1416, "num5": 26,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap7 = [ "item": "REP",  "num1": 'ARC OVEN',   "num2": 'XA',   "num3": 37.5, "num4": 417, "num5": 27,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap8 = [ "item": "TFT",  "num1": 'SI001',   "num2": 'XA',   "num3": 144.0, "num4": 418, "num5": 28,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap9 = [ "item": "Total","num1": 'PS',   "num2": 'XA',   "num3": 141.9, "num4": 419, "num5": 29,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]
        def assetteStatusMap10 = [ "item": "REP",  "num1": 'SI002',   "num2": 'XA',   "num3": 145.2, "num4": 417, "num5": 27,"num6": 148.8, "num7": 411, "num8": 21,"num9": 148.8]

        cassetteStatusList.add(assetteStatusMap1)
        cassetteStatusList.add(assetteStatusMap2)
        cassetteStatusList.add(assetteStatusMap3)
        cassetteStatusList.add(assetteStatusMap4)
        cassetteStatusList.add(assetteStatusMap5)
        cassetteStatusList.add(assetteStatusMap6)
        cassetteStatusList.add(assetteStatusMap7)
        cassetteStatusList.add(assetteStatusMap8)
        cassetteStatusList.add(assetteStatusMap9)
        cassetteStatusList.add(assetteStatusMap10)

        cassetteStatusList
    }


    static ArrayList getTactTimeByLotList() {

        def now = new Date()
        def lotTactTimeList = []

        def lotTactTimeMap1 = ['id': 1, 'lotId': 'FRS-11N', 'eqpId': 'FLP00001-L8FA', 'stepId': 'FLP00001-L8FA', 'shift': 10,
                                         'glassQty': 30, 'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap2 = ['id': 2, 'lotId': 'FRS-12N', 'eqpId': 'FLP00002-L8FA', 'stepId': 'FLP00002-L8FA', 'shift': 11,
                                         'glassQty': 40, 'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap3 = ['id': 3, 'lotId': 'FRS-13N', 'eqpId': 'FLP00003-L8FA', 'stepId': 'FLP00003-L8FA', 'shift': 12,
                                        'glassQty': 50,  'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap4 = ['id': 4, 'lotId': 'FRS-14N', 'eqpId': 'FLP00004-L8FA', 'stepId': 'FLP00004-L8FA', 'shift': 15,
                                         'glassQty': 60,  'trackInTime': now, 'trackOutTime': now]
        def lotTactTimeMap5 = ['id': 5, 'lotId': 'FRS-15N', 'eqpId': 'FLP00005-L8FA', 'stepId': 'FLP00005-L8FA', 'shift': 17,
                                         'glassQty': 70,  'trackInTime': now, 'trackOutTime': now]
        lotTactTimeList.add(lotTactTimeMap1)
        lotTactTimeList.add(lotTactTimeMap2)
        lotTactTimeList.add(lotTactTimeMap3)
        lotTactTimeList.add(lotTactTimeMap4)
        lotTactTimeList.add(lotTactTimeMap5)

        lotTactTimeList
    }


    static ArrayList getTactTimeByGlassList() {

        def now = new Date()
        def glassTactTimeList = []

        def glassTactTimeMap1 = ['id': 1, 'glassId': 'FRS-11N', 'eqpId': 'FLP00001-L8FA', 'stepId': 'FLP00001-L8FA', 'shift': 10,
                               'glassQty': 30, 'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap2 = ['id': 2, 'glassId': 'FRS-12N', 'eqpId': 'FLP00002-L8FA', 'stepId': 'FLP00002-L8FA', 'shift': 11,
                               'glassQty': 40, 'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap3 = ['id': 3, 'glassId': 'FRS-13N', 'eqpId': 'FLP00003-L8FA', 'stepId': 'FLP00003-L8FA', 'shift': 12,
                               'glassQty': 50,  'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap4 = ['id': 4, 'glassId': 'FRS-14N', 'eqpId': 'FLP00004-L8FA', 'stepId': 'FLP00004-L8FA', 'shift': 15,
                               'glassQty': 60,  'trackInTime': now, 'trackOutTime': now]
        def glassTactTimeMap5 = ['id': 5, 'glassId': 'FRS-15N', 'eqpId': 'FLP00005-L8FA', 'stepId': 'FLP00005-L8FA', 'shift': 17,
                               'glassQty': 70,  'trackInTime': now, 'trackOutTime': now]
        glassTactTimeList.add(glassTactTimeMap1)
        glassTactTimeList.add(glassTactTimeMap2)
        glassTactTimeList.add(glassTactTimeMap3)
        glassTactTimeList.add(glassTactTimeMap4)
        glassTactTimeList.add(glassTactTimeMap5)

        glassTactTimeList
    }


    static  ArrayList getTrendTestList(){
        def now = new Date()
        def trendTestList = []

        def trendTestMap1 = ['id': 1, 'type': 's', 'operation': '11100', 'eqpId': '3CIP101', 'operInTime': now, 'operInLotId': '3CCS360001', 'input': 197, 's': '0%', 'dcs': '1.5%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap2 = ['id': 2, 'type': 'DCS', 'operation': '11100', 'eqpId': '3CIP102', 'operInTime': now, 'operInLotId': '3CCS360002', 'input': 81, 's': '0%', 'dcs': '1.2%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap3 = ['id': 3, 'type': 'X 亮线', 'operation': '11100', 'eqpId': '3CIP103', 'operInTime': now, 'operInLotId': '3CCS360003', 'input': 54, 's': '0%', 'dcs': '0.1%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap4 = ['id': 4, 'type': 'Particle其他', 'operation': '11100', 'eqpId': '3CIP104', 'operInTime': now, 'operInLotId': '3CCS360004', 'input': 55, 's': '0%', 'dcs': '3.6%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap5 = ['id': 5, 'type': 'Sell Scratch', 'operation': '11100', 'eqpId': '3CIP105', 'operInTime': now, 'operInLotId': '3CCS360005', 'input': 72, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap6 = ['id': 6, 'type': 'Y薄白线', 'operation': '11100', 'eqpId': '3CIP106', 'operInTime': now, 'operInLotId': '3CCS360006', 'input': 82, 's': '0%', 'dcs': '2.4%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap7 = ['id': 7, 'type': '点灯异常', 'operation': '11100', 'eqpId': '3CIP107', 'operInTime': now, 'operInLotId': '3CCS360007', 'input': 45, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap8 = ['id': 8, 'type': 'X薄暗线', 'operation': '11100', 'eqpId': '3CIP108', 'operInTime': now, 'operInLotId': '3CCS360008', 'input': 78, 's': '0%', 'dcs': '3.8%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap9 = ['id': 9, 'type': 'Y暗线', 'operation': '11100', 'eqpId': '3CIP109', 'operInTime': now, 'operInLotId': '3CCS360009', 'input': 107, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']

        trendTestList.add(trendTestMap1)
        trendTestList.add(trendTestMap2)
        trendTestList.add(trendTestMap3)
        trendTestList.add(trendTestMap4)
        trendTestList.add(trendTestMap5)
        trendTestList.add(trendTestMap6)
        trendTestList.add(trendTestMap7)
        trendTestList.add(trendTestMap8)
        trendTestList.add(trendTestMap9)
        trendTestList
    }

    static  ArrayList getUnitTestList(){
        def now = new Date()
        def trendTestList = []

        def trendTestMap1 = ['id': 1, 'operation': '11100', 'eqpId': '3CIP101', 'operInTime': now, 'unitId': '3CCS360001', 'lotId': 'LOT-S120001', 'input': 197, 's': '0%', 'dcs': '1.5%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap2 = ['id': 2, 'operation': '11100', 'eqpId': '3CIP102', 'operInTime': now, 'unitId': '3CCS360002', 'lotId': 'LOT-S120002',  'input': 81, 's': '0%', 'dcs': '1.2%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap3 = ['id': 3, 'operation': '11100', 'eqpId': '3CIP103', 'operInTime': now, 'unitId': '3CCS360003', 'lotId': 'LOT-S120003',  'input': 54, 's': '0%', 'dcs': '0.1%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap4 = ['id': 4, 'operation': '11100', 'eqpId': '3CIP104', 'operInTime': now, 'unitId': '3CCS360004', 'lotId': 'LOT-S120004', 'input': 55, 's': '0%', 'dcs': '3.6%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap5 = ['id': 5, 'operation': '11100', 'eqpId': '3CIP105', 'operInTime': now, 'unitId': '3CCS360005', 'lotId': 'LOT-S120005',  'input': 72, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap6 = ['id': 6, 'operation': '11100', 'eqpId': '3CIP106', 'operInTime': now, 'unitId': '3CCS360006', 'lotId': 'LOT-S120006',  'input': 82, 's': '0%', 'dcs': '2.4%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap7 = ['id': 7, 'operation': '11100', 'eqpId': '3CIP107', 'operInTime': now, 'unitId': '3CCS360007', 'lotId': 'LOT-S120007',  'input': 45, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap8 = ['id': 8, 'operation': '11100', 'eqpId': '3CIP108', 'operInTime': now, 'unitId': '3CCS360008', 'lotId': 'LOT-S120008',  'input': 78, 's': '0%', 'dcs': '3.8%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap9 = ['id': 9, 'operation': '11100', 'eqpId': '3CIP109', 'operInTime': now, 'unitId': '3CCS360009', 'lotId': 'LOT-S120009',  'input': 107, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']

        trendTestList.add(trendTestMap1)
        trendTestList.add(trendTestMap2)
        trendTestList.add(trendTestMap3)
        trendTestList.add(trendTestMap4)
        trendTestList.add(trendTestMap5)
        trendTestList.add(trendTestMap6)
        trendTestList.add(trendTestMap7)
        trendTestList.add(trendTestMap8)
        trendTestList.add(trendTestMap9)
        trendTestList
    }


    static  ArrayList getOperInfoList(){
        def now = new Date()
        def operInfoList = []

        def operInfoMap1 = ['id': 1, 'employee': '026110', 'name': '张三', 'department': '技术部', 'companyNo':'SH001', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': "O", 'lcdAll': '', 'testAll': '', 'tftAll': '']
        def operInfoMap2 = ['id': 1, 'employee': '026111', 'name': '李四', 'department': '技术部', 'companyNo':'SH002', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll': 'O', 'testAll': '', 'tftAll':'']
        def operInfoMap3 = ['id': 1, 'employee': '026112', 'name': '王五', 'department': '技术部', 'companyNo':'SH003', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': '', 'testAll': 'O', 'tftAll':'O']
        def operInfoMap4 = ['id': 1, 'employee': '026113', 'name': '赵六', 'department': '生产部', 'companyNo':'SH004', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll': '', 'testAll': '', 'tftAll':'']
        def operInfoMap5 = ['id': 1, 'employee': '026114', 'name': '知名', 'department': '技术部', 'companyNo':'SH005', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': 'O', 'testAll': '', 'tftAll': 'O']
        def operInfoMap6 = ['id': 1, 'employee': '026115', 'name': '小花', 'department': '技术部', 'companyNo':'SH006', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll':'', 'testAll':'O', 'tftAll':'']
        def operInfoMap7 = ['id': 1, 'employee': '026116', 'name': '小黄', 'department': '技术部', 'companyNo':'SH007', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll':'', 'testAll':'', 'tftAll': 'O']
        def operInfoMap8 = ['id': 1, 'employee': '026117', 'name': '小黑', 'department': '人事部', 'companyNo':'SH008', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': 'O', 'lcdAll': 'O', 'testAll': '', 'tftAll': 'O']
        def operInfoMap9 = ['id': 1, 'employee': '026118', 'name': '佳琪', 'department': '财务部', 'companyNo':'SH009', 'deleteFlag':'N', 'groupId': 'LCD_ADVISOR', 'fabOrc': '', 'lcdAll':'', 'testAll':'', 'tftAll': '']

        operInfoList.add(operInfoMap1)
        operInfoList.add(operInfoMap2)
        operInfoList.add(operInfoMap3)
        operInfoList.add(operInfoMap4)
        operInfoList.add(operInfoMap5)
        operInfoList.add(operInfoMap6)
        operInfoList.add(operInfoMap7)
        operInfoList.add(operInfoMap8)
        operInfoList.add(operInfoMap9)
        operInfoList
    }


    static  ArrayList getDailyYieldList(){
        def now = new Date()
        def dailyYieldList = []

        def dailyYieldMap1 = ['id': 1, 'itemDate': now, 'inputNo': 4719, 'pQty': 3983,'sQty': 93,'qQty': 35,
                              'ngQty': 33,'emQty': 544,'pxl': 25,'sdo': 0,'sgo': 0,'sOpen': 0,'dds': 33,'dcs': 83]
        def dailyYieldMap2 = ['id': 1, 'itemDate': now, 'inputNo': 4518, 'pQty': 1428,'sQty': 91,'qQty': 36,
                              'ngQty': 35,'emQty': 206,'pxl': 18,'sdo': 1,'sgo': 0,'sOpen': 0,'dds': 98,'dcs': 3]
        def dailyYieldMap3 =  ['id': 1, 'itemDate': now, 'inputNo': 4214, 'pQty': 3203,'sQty': 83,'qQty': 37,
                               'ngQty': 53,'emQty': 183,'pxl': 19,'sdo': 0,'sgo': 0,'sOpen': 0,'dds': 39,'dcs': 9]
        def dailyYieldMap4 =  ['id': 1, 'itemDate': now, 'inputNo': 4111, 'pQty': 3119,'sQty': 60,'qQty': 39,
                               'ngQty': 33,'emQty': 193,'pxl': 13,'sdo': 2,'sgo': 0,'sOpen': 0,'dds': 83,'dcs': 8]

        dailyYieldList.add(dailyYieldMap1)
        dailyYieldList.add(dailyYieldMap2)
        dailyYieldList.add(dailyYieldMap3)
        dailyYieldList.add(dailyYieldMap4)

        dailyYieldList
    }

    static  ArrayList getJudgeRawList(){
        def now = new Date()
        def judgeRawList = []

        def judgeRawMap1 = ['id': 1, 'testTime': now, 'panelId': 'Panel00001', 'scrapCode': 'S00001','eqpId': 'EQP00001','grade': 'G00001',
                              'operId': 'O00001','shift':'SHIFT00001','stepId': 'Step0001','proId': 'Prod00001','glassId': 'Glass00001']
        def judgeRawMap2 = ['id': 2, 'testTime': now, 'panelId': 'Panel00002', 'scrapCode': 'S00002','eqpId': 'EQP00002','grade': 'G00002',
                            'operId': 'O00002','shift':'SHIFT00002','stepId': 'Step0002','proId': 'Prod00002','glassId': 'Glass00002']
        def judgeRawMap3 =  ['id': 3, 'testTime': now, 'panelId': 'Panel00003', 'scrapCode': 'S00003','eqpId': 'EQP00003','grade': 'G00003',
                             'operId': 'O00003','shift':'SHIFT00003','stepId': 'Step0003','proId': 'Prod00003','glassId': 'Glass00003']
        def judgeRawMap4 =  ['id': 4, 'testTime': now, 'panelId': 'Panel00004', 'scrapCode': 'S00004','eqpId': 'EQP00004','grade': 'G00004',
                             'operId': 'O00004','shift':'SHIFT00004','stepId': 'Step0004','proId': 'Prod00004','glassId': 'Glass00004']

        judgeRawList.add(judgeRawMap1)
        judgeRawList.add(judgeRawMap2)
        judgeRawList.add(judgeRawMap3)
        judgeRawList.add(judgeRawMap4)

        judgeRawList
    }

    static  ArrayList getCumYieldList(){
        def now = new Date()
        def judgeRawList = []

        def judgeRawMap1 = ['id': 1, 'testDate': now, 'inputQty': 51, 'outputQty': 31, 'scrapQty': 'S00001',
                            'stepId': 'Step0001','proId': 'Prod00001','yield': 'yield00001']
        def judgeRawMap2 = ['id': 2, 'testDate': now, 'inputQty': 61, 'outputQty': 42, 'scrapQty': 'S00002',
                            'stepId': 'Step0002','proId': 'Prod00002','yield': 'yield00002']
        def judgeRawMap3 =  ['id': 3, 'testDate': now, 'inputQty':71, 'outputQty':53, 'scrapQty': 'S00003',
                             'stepId': 'Step0003','proId': 'Prod00003','yield': 'yield00003']
        def judgeRawMap4 =  ['id': 4, 'testDate': now, 'inputQty': 81, 'outputQty':64, 'scrapQty': 'S00004',
                             'stepId': 'Step0004','proId': 'Prod00004','yield': 'yield00004']

        judgeRawList.add(judgeRawMap1)
        judgeRawList.add(judgeRawMap2)
        judgeRawList.add(judgeRawMap3)
        judgeRawList.add(judgeRawMap4)

        judgeRawList
    }

    static ArrayList trendTestLegendList(){
        def trendTestLegendList = []

        def trendTestLegendMap1 = ['scode':'Input']
        def trendTestLegendMap2 = ['scode':'X 亮线']

        trendTestLegendList.add(trendTestLegendMap1)
        trendTestLegendList.add(trendTestLegendMap2)

        trendTestLegendList
    }

    static ArrayList trendTestXaisList(){
        ['2016-03-12', '2016-03-13', '2016-03-14', '2016-03-15', '2016-03-16', '2016-03-17', '2016-03-18', '2016-03-19', '2016-03-20', '2016-03-21']
    }

    static ArrayList trendseriesDataList(){
        [[251, 113, 451, 237, 159, 222, 568, 582, 110, 318],[7.21, 6.46, 8.02, 4.91, 4.76, 6.12, 8.61, 9.11, 4.68, 5.89]];
    }


    static ArrayList glassReportDataList(){
        def glassReportDataList = []

        def flagItemList = flagItemDataList()

        def glassReportMap1 = [productid: 'K1060FHVFD0001',flagItemList:flagItemList]

        glassReportDataList.add(glassReportMap1)

        glassReportDataList
    }


    static ArrayList flagItemDataList(){
        def flagItemDataList = []

        def itemDataList1 = itemDataList()
        def itemDataList2 = itemDataList()

        def flagItemMap1 = [flag: '投入',itemDataList:itemDataList1]
        def flagItemMap2 = [flag: '产出',itemDataList:itemDataList2]

        flagItemDataList.add(flagItemMap1)
        flagItemDataList.add(flagItemMap2)

        flagItemDataList
    }


    static ArrayList itemDataList(){
        def itemDataList = []

        def itemMap1 = [flag: '计划',itemList:[100 ,110 ,120 ,130 ,140 ,150 ,160 ,170 ,180]]
        def itemMap2 = [flag: '实际',itemList:[100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100]]
        def itemMap3 = [flag: '差异',itemList:[0 ,10 ,20 ,30 ,40 ,50 ,60 ,70 ,80]]
        def itemMap4 = [flag: '计划累计',itemList:[100 ,210 ,330 ,460 ,600 ,750 ,910 ,1080 ,1260]]
        def itemMap5 = [flag: '实际累计',itemList:[100 ,200 ,300 ,400 ,500 ,600 ,700 ,800 ,900]]
        def itemMap6 = [flag: '差异累计',itemList:[0 ,10 ,30 ,60 ,100 ,150 ,210 ,280 ,360]]

        itemDataList.add(itemMap1)
        itemDataList.add(itemMap2)
        itemDataList.add(itemMap3)
        itemDataList.add(itemMap4)
        itemDataList.add(itemMap5)
        itemDataList.add(itemMap6)

        itemDataList
    }

    static ArrayList testItemList(){
        def testItemList = []

        def itemMap1 = [itemList:[100 ,110 ,120 ,130 ,140 ,150 ,160 ,170 ,180]]
        def itemMap2 = [itemList:[100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100]]
        def itemMap3 = [itemList:[0 ,10 ,20 ,30 ,40 ,50 ,60 ,70 ,80]]
        def itemMap4 = [itemList:[100 ,210 ,330 ,460 ,600 ,750 ,910 ,1080 ,1260]]
        def itemMap5 = [itemList:[100 ,200 ,300 ,400 ,500 ,600 ,700 ,800 ,900]]
        def itemMap6 = [itemList:[0 ,10 ,30 ,60 ,100 ,150 ,210 ,280 ,360]]
        def itemMap7 = [itemList:[100 ,110 ,120 ,130 ,140 ,150 ,160 ,170 ,180]]
        def itemMap8 = [itemList:[100 ,100 ,100 ,100 ,100 ,100 ,100 ,100 ,100]]
        def itemMap9 = [itemList:[0 ,10 ,20 ,30 ,40 ,50 ,60 ,70 ,80]]
        def itemMap10 = [itemList:[100 ,210 ,330 ,460 ,600 ,750 ,910 ,1080 ,1260]]
        def itemMap11 = [itemList:[100 ,200 ,300 ,400 ,500 ,600 ,700 ,800 ,900]]
        def itemMap12 = [itemList:[0 ,10 ,30 ,60 ,100 ,150 ,210 ,280 ,360]]

        testItemList.add(itemMap1)
        testItemList.add(itemMap2)
        testItemList.add(itemMap3)
        testItemList.add(itemMap4)
        testItemList.add(itemMap5)
        testItemList.add(itemMap6)
        testItemList.add(itemMap7)
        testItemList.add(itemMap8)
        testItemList.add(itemMap9)
        testItemList.add(itemMap10)
        testItemList.add(itemMap11)
        testItemList.add(itemMap12)

        testItemList
    }


    static ArrayList productRejectionRateLegendList(){
        def trendTestLegendList = []

        def trendTestLegendMap1 = ['scode':'破片']
        def trendTestLegendMap2 = ['scode':'不良品']
        def trendTestLegendMap3 = ['scode':'In line不良率']

        trendTestLegendList.add(trendTestLegendMap1)
        trendTestLegendList.add(trendTestLegendMap2)
        trendTestLegendList.add(trendTestLegendMap3)

        trendTestLegendList
    }

    static ArrayList productRejectionRateXaisList(){
        ['2016-03-12', '2016-03-13', '2016-03-14', '2016-03-15', '2016-03-16', '2016-03-17', '2016-03-18', '2016-03-19', '2016-03-20', '2016-03-21']
    }

    static ArrayList productRejectionRateSeriesDataList(){
        [[251, 113, 451, 237, 159, 222, 568, 582, 110, 318],[251, 113, 451, 237, 159, 222, 568, 582, 110, 318],[1.21, 6.46, 8.02, 4.91, 4.76, 6.12, 8.61, 9.11, 3.68, 5.89]];
    }

    static  ArrayList getProductRejectionRateList(){
        def now = new Date()
        def trendTestList = []

        def trendTestMap1 = ['id': 1, 'type': 's', 'operation': '11100', 'eqpId': '3CIP101', 'operInTime': now, 'operInLotId': '3CCS360001', 'input': 197, 's': '0%', 'dcs': '1.5%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap2 = ['id': 2, 'type': 'DCS', 'operation': '11100', 'eqpId': '3CIP102', 'operInTime': now, 'operInLotId': '3CCS360002', 'input': 81, 's': '0%', 'dcs': '1.2%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap3 = ['id': 3, 'type': 'X 亮线', 'operation': '11100', 'eqpId': '3CIP103', 'operInTime': now, 'operInLotId': '3CCS360003', 'input': 54, 's': '0%', 'dcs': '0.1%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap4 = ['id': 4, 'type': 'Particle其他', 'operation': '11100', 'eqpId': '3CIP104', 'operInTime': now, 'operInLotId': '3CCS360004', 'input': 55, 's': '0%', 'dcs': '3.6%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap5 = ['id': 5, 'type': 'Sell Scratch', 'operation': '11100', 'eqpId': '3CIP105', 'operInTime': now, 'operInLotId': '3CCS360005', 'input': 72, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap6 = ['id': 6, 'type': 'Y薄白线', 'operation': '11100', 'eqpId': '3CIP106', 'operInTime': now, 'operInLotId': '3CCS360006', 'input': 82, 's': '0%', 'dcs': '2.4%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap7 = ['id': 7, 'type': '点灯异常', 'operation': '11100', 'eqpId': '3CIP107', 'operInTime': now, 'operInLotId': '3CCS360007', 'input': 45, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap8 = ['id': 8, 'type': 'X薄暗线', 'operation': '11100', 'eqpId': '3CIP108', 'operInTime': now, 'operInLotId': '3CCS360008', 'input': 78, 's': '0%', 'dcs': '3.8%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']
        def trendTestMap9 = ['id': 9, 'type': 'Y暗线', 'operation': '11100', 'eqpId': '3CIP109', 'operInTime': now, 'operInLotId': '3CCS360009', 'input': 107, 's': '0%', 'dcs': '0.0%', 'xHighLight': '0.0%', 'particle': '0.0%', 'cellSearch': '0.0%', 'yWhiteLight': '0.0%', 'lightException': '0.5%', 'xBlachLine': '0.0%', 'yBlachLine': '0.0%']

        trendTestList.add(trendTestMap1)
        trendTestList.add(trendTestMap2)
        trendTestList.add(trendTestMap3)
        trendTestList.add(trendTestMap4)
        trendTestList.add(trendTestMap5)
        trendTestList.add(trendTestMap6)
        trendTestList.add(trendTestMap7)
        trendTestList.add(trendTestMap8)
        trendTestList.add(trendTestMap9)
        trendTestList
    }


    static ArrayList qTimeList(){
        def qTimeList = []

        def qTimeMap1 = ['qType':'>=']
        def qTimeMap2 = ['qType':'<=']

        qTimeList.add(qTimeMap1)
        qTimeList.add(qTimeMap2)

        qTimeList
    }

}
