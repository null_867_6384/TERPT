package kdx

import org.apache.tools.zip.ZipEntry
import org.apache.tools.zip.ZipFile
import org.apache.tools.zip.ZipOutputStream

class FileUtils {

    static File format(File file){
        String fileName= file.absolutePath
        File newFile= new File("${fileName}-tmp")
        newFile.withWriter { writer ->
            file.eachLine {
                def line = it.replaceAll(/\t/, '    ').replaceAll(/^\s+$/, '').replaceAll(/^(.+\S+)\s+$/, '$1')
                writer.writeLine(line)
            }
        }
         newFile
    }


    static String readLine(fileName) {
        def stringBuilder = new StringBuilder()
        new File(fileName).eachLine { line ->
            stringBuilder.append("${line}");
        }
        stringBuilder.toString()
    }


    static void zipDIR(String sourceDIR, String targetZipFile) {
        try {
                FileOutputStream target = new FileOutputStream(targetZipFile)
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(target))
                int BUFFER_SIZE = 1024
                byte[] buff = new byte[BUFFER_SIZE]
                File dir = new File(sourceDIR)
                if (!dir.isDirectory()) {
                        throw new IllegalArgumentException(sourceDIR+" is not a directory!")
                    }
                File[] files = dir.listFiles()
                for (int i = 0; i < files.length; i++) {
                        FileInputStream fi = new FileInputStream(files[i])
                        BufferedInputStream origin = new BufferedInputStream(fi)
                        ZipEntry entry = new ZipEntry(files[i].getName())
                        out.putNextEntry(entry)
                        int count
                        while ((count = origin.read(buff)) != -1) {
                                out.write(buff, 0, count)
                            }
                        origin.close()
                    }
                out.close()

            }catch (IOException e) {
                throw e
            }
    }

    static  unZip(String srcFile, String dest, boolean deleteFile) {
        try {
            File file = new File(srcFile)
            if (!file.exists()) {
                throw new RuntimeException("解压文件不存在!")
            }
            ZipFile zipFile = new ZipFile(file)
            Enumeration e = zipFile.getEntries()
            while (e.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) e.nextElement()
                if (zipEntry.isDirectory()) {
                    String name = zipEntry.getName()
                    name = name.substring(0, name.length() - 1)
                    File f = new File(dest + name)
                    f.mkdirs()
                } else {
                    File f = new File(dest + zipEntry.getName())
                    f.getParentFile().mkdirs()
                    f.createNewFile()
                    InputStream is = zipFile.getInputStream(zipEntry)
                    FileOutputStream fos = new FileOutputStream(f)
                    int length = 0
                    byte[] b = new byte[1024]
                    while ((length = is.read(b, 0, 1024)) != -1) {
                        fos.write(b, 0, length)
                    }
                    is.close()
                    fos.close()
                }
            }
            if (zipFile != null) {
                zipFile.close()
            }
            if (deleteFile) {
                file.delete()
            }
        } catch (IOException e) {
            throw e
        }
    }

}
