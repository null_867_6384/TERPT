package te.rpt

class CoaReportForController {
    def dynamicQueryService
    def exportFileService
    def coaReportFor() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def coaReportFor=[]
        def totalCount=[]
        def tableHeadList=[]
        def coaReportForEdc=[]
        params.SYS_QUERY_NAME = 'checkerOutputByDayChecker'
        def checkerOutputByDayChecker = dynamicQueryService.queryForList(params)

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'coaReportFor'
            coaReportFor = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'coaReportForEdc'
            coaReportForEdc = dynamicQueryService.queryForList(params)

            if (coaReportFor.size() > 0) {
                tableHeadList = coaReportFor[0].keySet()
            }

        }

        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,checkerOutputByDayChecker:checkerOutputByDayChecker,
         startno:params.startno?:'',endno:params.endno?:'',coaReportFor:coaReportFor,coaReportForEdc:coaReportForEdc,
         lotId:params.lotId?:'',startTime:params.startTime?:'',endTime:params.endTime?:'']
    }
}
