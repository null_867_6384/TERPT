package te.rpt

import java.text.SimpleDateFormat

class ReportByDayController {

    def dynamicQueryService
    def exportFileService
    def reportByDay() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent


        def pageSizeList = [10, 20, 50, 100]
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))
        def endTime = params.endTime ? sdf.parse(params.endTime) : sdf.parse(sdf.format(new Date()))
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)
        def tableHeadList = []
        def reportByDay=[]
        def totalCount=0
        def reportByDayPic=[]
        def reportByDayPicX=[]

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'reportByDay'
            reportByDay = dynamicQueryService.queryForList(params)


            if (reportByDay.size() > 0) {
                tableHeadList = reportByDay[0].keySet()
            }
            params.SYS_QUERY_NAME = 'reportByDayCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
            params.SYS_QUERY_NAME = 'reportByDayPic'
            reportByDayPic = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'reportByDayPicX'
            reportByDayPicX = dynamicQueryService.queryForList(params)
        }


        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [reportByDay: reportByDay, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,
             tableHeadList: tableHeadList, reportByDay: reportByDay,reportByDayPicX:reportByDayPicX,
             startno:params.startno?:'',endno:params.endno?:'',startTime:params.startTime?:'',endTime:params.endTime?:'',reportByDayPic:reportByDayPic]
        }
    }
}
