package te.rpt

class StageWipController {

    def commonDataService
    def dynamicQueryService
    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }
    //在制品工序汇总
    def stageWip(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10,20,50,100]
        params.SYS_QUERY_NAME = 'stageWip'
        def stageWip = dynamicQueryService.queryForList(params)
        def tableHeadList =  []
        if(stageWip.size()>0){
            tableHeadList =  stageWip[0].keySet()
        }
        params.SYS_QUERY_NAME = 'stageIdList'
        def stageIdList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'stageWipCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        [pageSize: pageSize,tableHeadList:tableHeadList,stageWip:stageWip,tableHeadList:tableHeadList,stageIdList:stageIdList,stageId:params.stageId?:'',totalCount:totalCount,
         startno:params.startno?:'',endno: params.endno?:'',pageSizeList:pageSizeList,pageSize:pageSize]
    }
    //生产阶段明细
    def stageWipStep(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        def pageSizeList = [10,20,50,100]
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        params.SYS_QUERY_NAME = 'stageWipStep'
        def stageWipStep = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'stageWipStepCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        def tableHeadList =  []
        if(stageWipStep.size()>0){
            tableHeadList =  stageWipStep[0].keySet()
        }
        println(params)
        //查询条件（lot）
        params.SYS_QUERY_NAME = 'stageWipSteplotList'
        def stageWipSteplotList = dynamicQueryService.queryForList(params)
        //查询条件（step）
        params.SYS_QUERY_NAME = 'stageWipStepstepList'
        def stageWipStepstepList = dynamicQueryService.queryForList(params)
        //查询条件（part）
        params.SYS_QUERY_NAME = 'stageWipSteppartList'
        def stageWipSteppartList = dynamicQueryService.queryForList(params)
        //model
        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',stageDesc:params.stageDesc?:'',
         stageWipStep:stageWipStep,stageWipSteplotList:stageWipSteplotList,stageWipStepstepList:stageWipStepstepList,stageWipSteppartList:stageWipSteppartList,
         lotid:params.lotid?:'',stepdesc:params.stepdesc?:'',partdesc:params.partdesc?:'',
         lotId:params.lotId?:'',partName:params.partName?:'']
    }
    //一阶导出
    def exportExcel() {
        def sysQueryName


        sysQueryName = params.SYS_QUERY_NAME

        params.startno = 1
        params.endno = 99999999999999999


        println params.SYS_QUERY_NAME
        params.SYS_QUERY_NAME = sysQueryName
        println params.stepdesc
        def dataList = dynamicQueryService.queryForList(params)
        println(dataList)
        def tableHeadList =  []
        if(dataList.size()>0){
            tableHeadList =  dataList[0].keySet()
        }
        String filename = new String("${sysQueryName}.xls".getBytes("UTF-8"), "UTF-8");
        response.setContentType('application/vnd.ms-excel;charset=UTF-8')
        response.setHeader('Content-disposition', "attachment;filename=${filename};charset=UTF-8")
        response.setCharacterEncoding("UTF-8");
        render(template: "export${params.EXPORT_CONTENT_NAME}", model: [dataList: dataList,tableHeadList:tableHeadList,startno:params.startno?:'',endno:params.endno?:''])
    }
}
