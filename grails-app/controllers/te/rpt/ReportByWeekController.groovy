package te.rpt

import java.text.SimpleDateFormat

class ReportByWeekController {
    def dynamicQueryService
    def exportFileService
    def reportByWeek() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent


        def pageSizeList = [10, 20, 50, 100]
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()-6))
        def endTime = params.endTime ? sdf.parse(params.endTime) : sdf.parse(sdf.format(new Date()))
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)
        def reportByWeek=[]
        def totalCount=0
        def tableHeadList = []
        def reportByWeekType=[]
        def reportByDayPicX=[]
        def reportByWeekPic=[]
        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'reportByWeek'
            reportByWeek = dynamicQueryService.queryForList(params)
            if (reportByWeek.size() > 0) {
                tableHeadList = reportByWeek[0].keySet()
            }
            params.SYS_QUERY_NAME = 'reportByWeekCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

            params.SYS_QUERY_NAME = 'reportByWeekType'
            reportByWeekType = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'reportByDayPicX'
            reportByDayPicX = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'reportByWeekPic'
            reportByWeekPic = dynamicQueryService.queryForList(params)
        }


        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [reportByWeek: reportByWeek, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,reportByWeekType:reportByWeekType,
             tableHeadList: tableHeadList, reportByWeek: reportByWeek,reportByDayPicX:reportByDayPicX,reportByWeekPic:reportByWeekPic,
             startno:params.startno?:'',endno:params.endno?:'',startTime:params.startTime?:'',endTime:params.endTime?:'',
            type1:params.type1?:'']
        }
    }
}
