package te.rpt

/**
 * 在制品历史表
 */
class WipHisController {

    def commonDataService
    def dynamicQueryService
    def exportFileService

    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }

    def Report_085() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]
        print params.lot_id
        params.SYS_QUERY_NAME = 'Report_085'
        def Report_085List = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (Report_085List.size() > 0) {
            tableHeadList = Report_085List[0].keySet()
        }
        params.SYS_QUERY_NAME = 'Report_085Count'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [Report_085List: Report_085List, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize  : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
             totalCount: totalCount, tableHeadList: tableHeadList, Report_085List: Report_085List, lot_id: params.lot_id ?: '',
             startTime : params.startTime ?: '', endTime: params.endTime ?: '', start: params.start ?: '', end: params.end ?: '']
        }
    }
}
