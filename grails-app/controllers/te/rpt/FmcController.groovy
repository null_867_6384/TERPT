package te.rpt
/**
 * 设备状态历史查看
 */
import java.text.SimpleDateFormat

class FmcController {

    def dynamicQueryService

    def eqpHistory() {

        //获取默认时间
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))
        def endTime = params.endTime ? sdf.parse(params.endTime) : sdf.parse(sdf.format(new Date()))
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)
        request.setCharacterEncoding("UTF-8");//设定编码格式
        response.setContentType("text/html;charset=UTF-8");
        //def dayList = DateUtils.getHourList(startTime, endTime)


        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'eqpStateList'
        def eqpStateList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'locationList'
        def locationList = dynamicQueryService.queryForList(params)

        params.SYS_QUERY_NAME = 'eqpHistoryList'
        def report_011 = dynamicQueryService.queryForList(params)

        params.SYS_QUERY_NAME = 'eqpHistoryCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        [lotid    : params.lotid, lota: params.lota, woid: params.woid, partname: params.partname, lineid: params.lineid, location: params.location,
         startTime: startTime, endTime: endTime, eqpid: params.eqpid, resoncode: params.resoncode, eqpstate: params.eqpstate,
         pageSize : pageSize, totalCount: totalCount, pageSizeList: pageSizeList, locationList: locationList, eqpStateList: eqpStateList, report_011: report_011]
    }
}
