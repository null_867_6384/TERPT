package te.rpt

class CoaReportController {

    def dynamicQueryService
    def exportFileService

    def coaReport() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def coaReport=[]
        def totalCount=[]
        def tableHeadList=[]
        def coaReportEdc=[]
        params.SYS_QUERY_NAME = 'checkerOutputByDayChecker'
        def checkerOutputByDayChecker = dynamicQueryService.queryForList(params)

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'coaReport'
            coaReport = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'coaReportEdc'
            coaReportEdc = dynamicQueryService.queryForList(params)

            if (coaReport.size() > 0) {
                tableHeadList = coaReport[0].keySet()
            }

        }

        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,checkerOutputByDayChecker:checkerOutputByDayChecker,
         startno:params.startno?:'',endno:params.endno?:'',coaReport:coaReport,coaReportEdc:coaReportEdc,
         lotId:params.lotId?:'',startTime:params.startTime?:'',endTime:params.endTime?:'']
    }
}
