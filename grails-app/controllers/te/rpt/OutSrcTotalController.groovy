package te.rpt

class OutSrcTotalController {

    def commonDataService
    def dynamicQueryService
    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }

    def outSrcTotal(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10,20,50,100]
        def outSrcTotal=[]
        def tableHeadList =  []
        def totalCount=0

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'outSrcTotal'
            outSrcTotal = dynamicQueryService.queryForList(params)
            if(outSrcTotal.size()>0){
                tableHeadList =  outSrcTotal[0].keySet()
            }
            params.SYS_QUERY_NAME = 'outSrcTotalCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        }

        params.SYS_QUERY_NAME = 'outsrcCustName'
        def outsrcCustName = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'outsrcUserList'
        def outsrcUserList = dynamicQueryService.queryForList(params)
        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,startTime:params.startTime?:'',endTime:params.endTime?:'',startT:params.startT?:'',endT:params.endT?:'',custName:params.custName?:'',
         startno:params.startno?:'',endno:params.endno?:'',outSrcTotal:outSrcTotal,outsrcUserList:outsrcUserList,outsrcCustName:outsrcCustName,
        outsrcUser:params.outsrcUser?:'']
    }
//һ�׵���
    def exportExcel() {
        def sysQueryName


        sysQueryName = params.SYS_QUERY_NAME

        params.startno = 1
        params.endno = 99999999999999999



        params.SYS_QUERY_NAME = sysQueryName

        def dataList = dynamicQueryService.queryForList(params)

        def tableHeadList =  []
        if(sysQueryName=='partFinList'){
            dataList=TranposUtils.colToRow('STEP_NAME','STEP_QTY',dataList)
            println(dataList)
        }
        if(dataList.size()>0){
            tableHeadList =  dataList[0].keySet()
        }
        String filename = new String("${sysQueryName}.xls".getBytes("UTF-8"), "UTF-8");
        response.setContentType('application/vnd.ms-excel;charset=UTF-8')
        response.setHeader('Content-disposition', "attachment;filename=${filename};charset=UTF-8")
        response.setCharacterEncoding("UTF-8");
        render(template: "export${params.EXPORT_CONTENT_NAME}", model: [dataList: dataList,tableHeadList:tableHeadList,startno:params.startno?:'',endno:params.endno?:''])
    }

}
