package te.rpt

/**
 * 在制品分布报表
 */
class WipDstributionController {

    def dynamicQueryService
    def exportFileService

    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }

    def Report_079() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]
        params.SYS_QUERY_NAME = 'Report_079'
        def Report_079 = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (Report_079.size() > 0) {
            tableHeadList = Report_079[0].keySet()
        }
        println(params.pageCurrent)
        params.SYS_QUERY_NAME = 'Report_079Count'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [Report_079: Report_079, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize  : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
             totalCount: totalCount, tableHeadList: tableHeadList, Report_079: Report_079,
             stageId   : params.stageId ?: '']
        }
    }

    def Report_079step() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]
        println(params.pageCurrent)

        params.SYS_QUERY_NAME = 'Report_079step'
        def Report_079step = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (Report_079step.size() > 0) {
            tableHeadList = Report_079step[0].keySet()
        }
        params.SYS_QUERY_NAME = 'Report_079stepCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        [pageSize  : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
         totalCount: totalCount, tableHeadList: tableHeadList, Report_079step: Report_079step,
         stageId   : params.stageId ?: '']

    }

    def Report_079stepTwo() {
        params.SYS_QUERY_NAME = 'Report_079stepTwo'
        def Report_079img = dynamicQueryService.queryForList(params)
        [Report_079img: Report_079img]
    }
}
