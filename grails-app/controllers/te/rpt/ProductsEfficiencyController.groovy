package te.rpt
import java.text.SimpleDateFormat
class ProductsEfficiencyController {

    def dynamicQueryService
    def exportFileService
    def productsEfficiency() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))
        def endTime = params.endTime ? sdf.parse(params.endTime) : sdf.parse(sdf.format(new Date()))
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)
        println(params.startTime)
        def pageSizeList = [10, 20, 50, 100]

        def totalCount=0
        def tableHeadList=[]
        def productsEfficiency=[]


        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'productsEfficiency'
            productsEfficiency = dynamicQueryService.queryForList(params)
            println(productsEfficiency)
            params.SYS_QUERY_NAME = 'productsEfficiencyCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

            if (productsEfficiency.size() > 0) {
                tableHeadList = productsEfficiency[0].keySet()
            }

        }

        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',productsEfficiency:productsEfficiency,
         partDesc:params.partDesc?:'',startTime:params.startTime?:'',endTime:params.endTime?:'',docId:params.docId?:'',
         eqpId:params.eqpId?:'',lotId:params.lotId?:'',startT:params.startT?:'',endT:params.endT?:'']
    }
}
