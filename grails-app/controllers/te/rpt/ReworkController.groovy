package te.rpt

/**
 * 返工报表
 */
class ReworkController {

    def dynamicQueryService

    def rework() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]
        params.SYS_QUERY_NAME = 'productNameList'
        def productList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'Report_084'
        def Report_084List = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (Report_084List.size() > 0) {
            tableHeadList = Report_084List[0].keySet()
        }
        params.SYS_QUERY_NAME = 'Report_084Count'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        params.SYS_QUERY_NAME = 'Report_084img'
        def Report_084img = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'Report_084sumQty'
        def Report_084sumQty = dynamicQueryService.queryForList(params)
        List<Integer> list = new ArrayList<Integer>();
        int n = 100;
        int sum = 0;
        double ration1 = 0;
        double ration2 = 0;
        //将rework的百分比存入list集合
        for (int i = 0; i <= Report_084img.size() - 1; i++) {
            if (i == 0) {
                ration1 = Report_084img[i].REWORK_QTY / Report_084sumQty[0].SUM_QTY * 100
                String str = ration1.toString();
                str = str.substring(0, 5)

                list.add(i, str)
            } else {
                //sum重置为0
                sum = 0;
                for (int j = 0; j <= i; j++) {
                    sum = sum + Report_084img[j].REWORK_QTY
                }
                ration2 = sum / Report_084sumQty[0].SUM_QTY * 100
                String str = ration2.toString();
                str = str.substring(0, 5)
                list.add(i, str)
            }
        }

        [Report_084img: Report_084img, list: list, pageSize: pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
         totalCount   : totalCount, tableHeadList: tableHeadList, Report_084List: Report_084List, productList: productList,
         productid    : params.productid ?: '', startTime: params.startTime ?: '', endTime: params.endTime ?: '', lotId: params.lotId ?: '', partDesc: params.partDesc ?: '']
    }

    def reworkDetail() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent

        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'reworkDetail'
        def Report_084stepTwo = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'reworkDetailCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        def tableHeadList = []
        if (Report_084stepTwo.size() > 0) {
            tableHeadList = Report_084stepTwo[0].keySet()
        }

        [pageSize  : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
         totalCount: totalCount, tableHeadList: tableHeadList, Report_084stepTwo: Report_084stepTwo,
         startTime : params.startTime ?: '', endTime: params.endTime ?: '', lotId: params.lotId ?: '', partDesc: params.partDesc ?: '', reworkCode: params.reworkCode ?: '']

    }
}
