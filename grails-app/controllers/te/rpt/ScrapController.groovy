package te.rpt
/**
 * ���ϱ���
 */
class ScrapController {

    def commonDataService
    def dynamicQueryService
    def exportFileService

    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }

    def Report_081() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'productNameList'
        def partList = dynamicQueryService.queryForList(params)

        params.SYS_QUERY_NAME = 'Report_081'
        def Report_081List = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (Report_081List.size() > 0) {
            tableHeadList = Report_081List[0].keySet()
        }
        params.SYS_QUERY_NAME = 'Report_081Count'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [Report_081List: Report_081List, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize  : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
             totalCount: totalCount, tableHeadList: tableHeadList, Report_081List: Report_081List, partList: partList,
             lotType   : params.lotType ?: '', lotId: params.lotId ?: '', name: params.name ?: '', startTime: params.startTime ?: '', endTime: params.endTime ?: '', location: params.location ?: '', stageId: params.stageId ?: '']
        }
    }
}
