package te.rpt

class MkwdxController {

    def commonDataService
    def dynamicQueryService
    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }

    def mkwdx(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10,20,50,100]
        def mokuaiwangongdan=[]
        def tableHeadList =  []
        def totalCount=0

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'mokuaiwangongdan'
            mokuaiwangongdan = dynamicQueryService.queryForList(params)

            if(mokuaiwangongdan.size()>0){
                tableHeadList =  mokuaiwangongdan[0].keySet()
            }
            params.SYS_QUERY_NAME = 'mokuaiwangongdanCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT


        }


        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,startTime:params.startTime?:'',endTime:params.endTime?:'',
         startno:params.startno?:'',endno:params.endno?:'',mokuaiwangongdan:mokuaiwangongdan,startT:params.startT?:'',endT:params.endT?:'',
                start:params.start?:'',end:params.end?:'',customCode:params.customCode?:'',lotId:params.lotId?:'',part:params.part?:''
         ]
    }
}
