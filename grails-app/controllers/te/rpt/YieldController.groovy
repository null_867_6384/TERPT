package te.rpt
import kdx.DateUtils
import org.beetl.ext.fn.Println

import java.text.SimpleDateFormat
class YieldController {

    def dynamicQueryService
    def yield(){
        params.SYS_QUERY_NAME = 'productTypeList'
        def productList = dynamicQueryService.queryForList(params)
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')
        params.year=params.year?:'2018'
        request.setCharacterEncoding("UTF-8");//设置字符编码
        response.setContentType("text/html;charset=UTF-8");
        params.productid=params.productid?:'基板'

        println params.productid
        def startTime = sdf.parse(params.year+'-01')
        def endTime =  sdf.parse(params.year+'-12')

        def dayList = DateUtils.getMonthList(startTime, endTime)

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def dataListNo=[]
        def dataList=[]
        def legendDataList=[]
        def percentItemList=[]
        def testDataList = []
        def seriesData = []
        def seryHourData = []
        if(params.EXPORT_CONTENT_NAME=='yield'){
            params.SYS_QUERY_NAME = 'yieldyearList'
             dataListNo = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'yieldMonthList'
             dataList = dynamicQueryService.queryForList(params)
            println dataList
             legendDataList = ['OUT_QTY','YIELD','TARGETYIELD']
             percentItemList = ['YIELD','TARGETYIELD']

            for(def k=0;k<legendDataList.size();k++){

                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<dataList.size();i++){
                        if(dayList[j]==dataList[i].INPUT_MONTH){
                            seryHourData.push(dataList[i][legendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        seryHourData.push(0)
                    }
                }
                seriesData.push(seryHourData)
            }
            println seriesData

        }

        [productList:productList,productid:params.productid?:'',year:params.year?:'',
         percentItemList:percentItemList,dayList:dayList,legendDataList:legendDataList,
         seriesData:seriesData,testDataList:testDataList,
         pageSize:pageSize,pageSizeList:pageSizeList,dataListNo:dataListNo]
    }

    def exportYield() {
        params.startno = 1
        params.endno = 5000
        def sysQueryName = params.SYS_QUERY_NAME
        params.SYS_QUERY_NAME = 'productTypeList'
        def productList = dynamicQueryService.queryForList(params)
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')
        params.year=params.year?:'2018'
        request.setCharacterEncoding("UTF-8");//设置字符编码
        response.setContentType("text/html;charset=UTF-8");
        params.productid=params.productid?:'基板'

        println params.productid
        def startTime = sdf.parse(params.year+'-01')
        def endTime =  sdf.parse(params.year+'-12')

        def dayList = DateUtils.getMonthList(startTime, endTime)

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def dataListNo=[]
        def dataList=[]
        def legendDataList=[]
        def percentItemList=[]
        def testDataList = []
        def seriesData = []
        def seryHourData = []
            params.SYS_QUERY_NAME = 'yieldyearList'
            dataListNo = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'yieldMonthList'
            dataList = dynamicQueryService.queryForList(params)
            println dataList
            legendDataList = ['OUT_QTY','YIELD','TARGETYIELD']
            percentItemList = ['YIELD','TARGETYIELD']

            for(def k=0;k<legendDataList.size();k++){

                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<dataList.size();i++){
                        if(dayList[j]==dataList[i].INPUT_MONTH){
                            seryHourData.push(dataList[i][legendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        seryHourData.push(0)
                    }
                }
                seriesData.push(seryHourData)
            }
            println seriesData


        String filename = new String("${sysQueryName}.xls".getBytes("UTF-8"), "UTF-8");
        response.setContentType('application/vnd.ms-excel;charset=UTF-8')
        response.setHeader('Content-disposition', "attachment;filename=${filename};charset=UTF-8")
        response.setCharacterEncoding("UTF-8");
        render(template: "export${params.EXPORT_CONTENT_NAME}",
                model:  [productList:productList,productid:params.productid?:'',year:params.year?:'',
                         percentItemList:percentItemList,dayList:dayList,legendDataList:legendDataList,
                         seriesData:seriesData,testDataList:testDataList,
                         pageSize:pageSize,pageSizeList:pageSizeList,dataListNo:dataListNo])
    }
}
