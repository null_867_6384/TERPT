package te.rpt

import com.xxd.Util.CellToList
import org.DataUtils

/**
 * ooc controller
 */
class DwsStatisticsController {

    def exportFileService
    def dynamicQueryService
    def commonDataService

    def statisticsReport() {
        //��ҳ
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        if (params.export == 'xls') {
            params.startno = 1
            params.endno = 56800
        } else {
            params.startno = (pageSize * (pageCurrent - 1)) + 1
            params.endno = pageSize * pageCurrent
        }

        def charidList = commonDataService.charidList(params)
        println(charidList)
        def departmentList = commonDataService.departmentList(params)

        params.SYS_QUERY_NAME = 'statisticsList'
        def statisticsList = dynamicQueryService.queryForList(params)

//        String[] clunms = ["ABNORMAL",'RAW_DATA_STRING']
//        List<LinkedHashMap<String, Object>> result = CellToList.cellToList(statisticsList, clunms, ";");

        def tableHeadList = []
        if (statisticsList.size() > 0) {
            tableHeadList = statisticsList[0].keySet();
        }

        params.SYS_QUERY_NAME = 'statisticsCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        print(params)
        //����
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [result: statisticsList, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else
            [pageSizeList  : DataUtils.getPageSizeList(), pageSize: pageSize, pageCurrent: pageCurrent, result: statisticsList,
             departmentList: departmentList, startTime: params.startTime ?: '',totalCount:totalCount,
             startno       : params.startno, tableHeadList: tableHeadList, endno: params.endno, endTime: params.endTime ?: '',
             charidList    : charidList, name: params.name ?: '', department: params.department ?: '', edc_from: params.edc_from ?: '']
    }
}
