package te.rpt
import java.text.SimpleDateFormat
class ProductEfficiencyController {
    def dynamicQueryService
    def exportFileService
    def productEfficiency() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))
        def endTime = params.endTime ? sdf.parse(params.endTime) : sdf.parse(sdf.format(new Date()))
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)

        def pageSizeList = [10, 20, 50, 100]

        def totalCount=0
        def tableHeadList=[]
        def productEfficiency=[]


        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'productEfficiency'
            productEfficiency = dynamicQueryService.queryForList(params)
            println(productEfficiency)
            params.SYS_QUERY_NAME = 'productEfficiencyCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

            if (productEfficiency.size() > 0) {
                tableHeadList = productEfficiency[0].keySet()
            }

        }

        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',productEfficiency:productEfficiency,
         lotId:params.lotId?:'',startTime:params.startTime?:'',endTime:params.endTime?:'',docId:params.docId?:'',
        eqpId:params.eqpId?:'',lotId:params.lotId?:'',startT:params.startT?:'',endT:params.endT?:'']
    }
}
