package te.rpt

class ProEfficiencyController {

    def dynamicQueryService
    def exportFileService

    def proEfficiency() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        params.SYS_QUERY_NAME = 'proEfficiency'
        def proEfficiency = dynamicQueryService.queryForList(params)



        def tableHeadList = []
        if (proEfficiency.size() > 0) {
            tableHeadList = proEfficiency[0].keySet()
        }
        params.SYS_QUERY_NAME = 'proEfficiencyCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        params.SYS_QUERY_NAME = 'proEfficiencyOrder'
        def proEfficiencyOrder = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'proEfficiencyEqp'
        def proEfficiencyEqp = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'proEfficiencyPart'
        def proEfficiencyPart = dynamicQueryService.queryForList(params)
        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',proEfficiency:proEfficiency
         ,startTime:params.startTime?:'',endTime:params.endTime?:'',order:params.order?:'',partDesc:params.partDesc?:'',
        eqp:params.eqp?:'',proEfficiencyOrder:proEfficiencyOrder,proEfficiencyEqp:proEfficiencyEqp,proEfficiencyPart:proEfficiencyPart,
        partDesc:params.partDesc?:'']
    }
}
