package te.rpt

import org.DataUtils

class DwtCpkController {

    def commonDataService
    def exportFileService
    def dynamicQueryService

    def CPKReport() {
        //��ҳ
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        if (params.export == 'xls') {
            params.startno = 1
            params.endno = 56800
        } else {
            params.startno = (pageSize * (pageCurrent - 1)) + 1
            params.endno = pageSize * pageCurrent
        }

        def partList = commonDataService.partList(params)
        def partversion = commonDataService.partversion(params)
        def charidList = commonDataService.charidList(params)

        params.SYS_QUERY_NAME = 'CPKList'
        def CPKList = dynamicQueryService.queryForList(params)
        println(CPKList)
        def tableHeadList = []
        if (CPKList.size() > 0) {
            tableHeadList = CPKList[0].keySet();
        }

        println("CPKList:" + CPKList)
        println("tableHeadList:" + tableHeadList)
        params.SYS_QUERY_NAME = 'CPKCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        //����
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [CPKList: CPKList, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSizeList : DataUtils.getPageSizeList(), pageSize: pageSize, pageCurrent: pageCurrent, totalCount: totalCount,
             version      : params.version ?: '', startTime: params.startTime ?: '', endTime: params.endTime ?: '',edc_from: params.edc_from ?: '',
             tableHeadList: tableHeadList, partName: params.partName ?: '', CPKList: CPKList, startno: params.startno,
             endno        : params.endno, partversion: partversion, charidList: charidList, partList: partList, name: params.name ?: '']
        }
    }
}
