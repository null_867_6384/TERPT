package te.rpt
/**
 * �豸״̬�鿴
 */
class EqpStateController {

    def dynamicQueryService
    def exportFileService
    def eqpState() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent

        params.SYS_QUERY_NAME = 'eqpStateList'
        def eqpStateList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'locationList'
        def locationList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'eqpIdList'
        def eqpIdList = dynamicQueryService.queryForList(params)

        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'eqpIdStateList'
        def eqpIdStateList = dynamicQueryService.queryForList(params)

        def tableHeadList = []
        if (eqpIdStateList.size() > 0) {
            tableHeadList = eqpIdStateList[0].keySet()
        }
        params.SYS_QUERY_NAME = 'eqpIdStateCount'

        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        print eqpIdStateList
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [eqpIdStateList: eqpIdStateList, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,
             eqpStateList : eqpStateList, locationList: locationList, eqpIdList: eqpIdList,
             tableHeadList: tableHeadList, eqpIdStateList: eqpIdStateList,
             eqpstate     : params.eqpstate ?: '', location: params.location ?: '', eqpid: params.eqpid ?: '']
        }
    }
}
