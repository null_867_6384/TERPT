package te.rpt

class WipSelectController {
    def dynamicQueryService
    def exportFileService
    def wipSelect() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent


        def pageSizeList = [10, 20, 50, 100]
        def tableHeadList = []
        def wipSelect=[]
        def totalCount=0


        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'wipSelect'
            wipSelect = dynamicQueryService.queryForList(params)


            if (wipSelect.size() > 0) {
                tableHeadList = wipSelect[0].keySet()
            }
            params.SYS_QUERY_NAME = 'wipSelectCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        }

        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [wipSelect: wipSelect, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,
             tableHeadList: tableHeadList, wipSelect: wipSelect,
            startno:params.startno?:'',endno:params.endno?:'',docId:params.docId?:'',custName:params.custName?:'',lotId:params.lotId?:'',
             partDesc:params.partDesc?:'',guige:params.guige?:'',startJhrq:params.startJhrq?:'',endJhrq:params.endJhrq?:'',startJhkgrq:params.startJhkgrq?:''
             ,endJhkgrq:params.endJhkgrq?:'',startLotsc:params.startLotsc?:'',endLotsc:params.endLotsc?:'',startLottc:params.startLottc?:''
             ,endLottc:params.endLottc?:'',startJhwg:params.startJhwg?:'',endJhwg:params.endJhwg?:'',stepName:params.stepName?:'',partType:params.partType?:'']
        }
    }
}
