package te.rpt

class UserCheckController {

    def dynamicQueryService
    def exportFileService

    def userCheck() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent

        /*params.SYS_QUERY_NAME = 'productNameList'
        def productList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'lotTypeList'
        def lotTypeList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'stageList'
        def stageList = dynamicQueryService.queryForList(params)*/


        def pageSizeList = [10, 20, 50, 100]
        def userCheck=[]
        def tableHeadList = []
        def totalCount=0


        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'userCheck'
            userCheck = dynamicQueryService.queryForList(params)

            if (userCheck.size() > 0) {
                tableHeadList = userCheck[0].keySet()
            }
            params.SYS_QUERY_NAME = 'userCheckCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        }


        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [userCheck: userCheck, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,
             tableHeadList: tableHeadList,userCheck: userCheck,
             startTime    : params.startTime ?: '', endTime: params.endTime ?: '', startT: params.startT ?: '', endT: params.endT ?: '',
             userDesc: params.userDesc ?: '',beizhu: params.beizhu ?: '',
            lotId:params.lotId?:'',part:params.part?:'',docId:params.docId?:'']
        }
    }
}
