package te.rpt
import org.TranposUtils
class StepProRecordController {

    def dynamicQueryService
    def exportFileService

    def stepProRecord() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def stepProRecord=[]
        def totalCount=[]
        def reportList=[]
        def tableHeadList=[]
        def stepProRecordAction=[]
        if(params.EXPORT_CONTENT_NAME!=null){
            params.SYS_QUERY_NAME = 'stepProRecord'
            stepProRecord = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'stepProRecordAction'
            stepProRecordAction = dynamicQueryService.queryForList(params)
            reportList = TranposUtils.colToRow('ACTION_CODE', 'MAIN_QTY', stepProRecord)


            if (reportList.size() > 0) {
                tableHeadList = reportList[0].keySet()
            }
        }

        /*params.SYS_QUERY_NAME = 'stepProRecordCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT*/

        [/*totalCount:totalCount,*/tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',stepProRecord:stepProRecord,stepProRecordAction:stepProRecordAction
         ,startTime:params.startTime?:'',endTime:params.endTime?:'',stepName:params.stepName,lotId:params.lotId?:'',
         reportList:reportList]
    }

    def stepProRecordStepTwo(){


        [woId:params.woId?:'',partDesc:params.partDesc?:'',lotCount:params.lotCount?:'',inQty:params.inQty?:'',outQty:params.outQty?:'']
    }
}
