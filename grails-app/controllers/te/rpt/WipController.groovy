package te.rpt

/**
 * 在制品报表
 */
class WipController {

    def dynamicQueryService
    def exportFileService

    def Wip() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent

        params.SYS_QUERY_NAME = 'productNameList'
        def productList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'lotTypeList'
        def lotTypeList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'stageList'
        def stageList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'stateList'
        def stateList = dynamicQueryService.queryForList(params)

        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'wipTrend'
        def wipTrendList = dynamicQueryService.queryForList(params)

        def tableHeadList = []
        if (wipTrendList.size() > 0) {
            tableHeadList = wipTrendList[0].keySet()
        }
        params.SYS_QUERY_NAME = 'wipTrendCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        print params
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [wipTrendList: wipTrendList, tableHeadList: tableHeadList])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, totalCount: totalCount, pageSizeList: pageSizeList,
             productList  : productList, lotTypeList: lotTypeList, stageList: stageList, stateList: stateList,
             tableHeadList: tableHeadList, wipTrendList: wipTrendList,
             productid    : params.productid ?: '', lotType: params.lotType ?: '', stage: params.stage ?: '', state: params.state ?: '']
        }
    }

    def exportExce() {
        params.startno = 1
        params.endno = 5000

        def sysQueryName = params.SYS_QUERY_NAME
        params.SYS_QUERY_NAME = 'wipTrend'
        def wipTrendList = dynamicQueryService.queryForList(params)

        def tableHeadList = []
        if (wipTrendList.size() > 0) {
            tableHeadList = wipTrendList[0].keySet()
        }
        params.SYS_QUERY_NAME = 'wipTrendCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT

        String filename = new String("${sysQueryName}.xls".getBytes("UTF-8"), "UTF-8");
        response.setContentType('application/vnd.ms-excel;charset=UTF-8')
        response.setHeader('Content-disposition', "attachment;filename=${filename};charset=UTF-8")
        response.setCharacterEncoding("UTF-8");
        render(template: "export${params.EXPORT_CONTENT_NAME}",
                model: [startno     : params.startno ?: '', endno: params.endno ?: '',
                        wipTrendList: wipTrendList, totalCount: totalCount, tableHeadList: tableHeadList])
    }

}
