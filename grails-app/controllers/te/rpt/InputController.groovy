package te.rpt
import kdx.DateUtils
import org.beetl.ext.fn.Println

import java.text.SimpleDateFormat

class InputController {
    def dynamicQueryService
    def input(){
        params.SYS_QUERY_NAME = 'productTypeList'
        def productList = dynamicQueryService.queryForList(params)
        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')
        params.year=params.year?:'2018'
        request.setCharacterEncoding("UTF-8");//�����ַ�����
        response.setContentType("text/html;charset=UTF-8");
        def startTime = sdf.parse(params.year+'-01')
        def endTime =  sdf.parse(params.year+'-12')

        def dayList = DateUtils.getMonthList(startTime, endTime)

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def dataListNo=[]
        def dataListNo1=[]
        def mkdataList=[]
        def mklegendDataList=[]
        def mkpercentItemList=[]
        def mkseriesData = []
        def dataList=[]

        def legendDataList=[]
        def percentItemList=[]
        def seriesData = []
        def testDataList = []
        def totalCount=0


        if(params.EXPORT_CONTENT_NAME=='input'){
            params.SYS_QUERY_NAME = 'inputyearList'
            dataListNo = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'inputyieldList'
            dataListNo1 = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'mkyieldList'
            mkdataList = dynamicQueryService.queryForList(params)
            mklegendDataList = ['YIELD','TARGETYIELD']
            mkpercentItemList = ['YIELD']
            println mkdataList;
            for(def k=0;k<mklegendDataList.size();k++){
                def mkseryHourData=[]
                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<mkdataList.size();i++){
                        if(dayList[j]==mkdataList[i].INPUT_MONTH){
                            println '1'
                            mkseryHourData.push(mkdataList[i][mklegendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        mkseryHourData.push(0)
                    }

                }
                mkseriesData.push(mkseryHourData)
            }
            println(mkseriesData)

            params.SYS_QUERY_NAME = 'jbyieldList'
            dataList = dynamicQueryService.queryForList(params)
            legendDataList = ['YIELD','TARGETYIELD']
            percentItemList = ['YIELD']

            for(def k=0;k<legendDataList.size();k++){
                def seryHourData=[]
                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<dataList.size();i++){
                        if(dayList[j]==dataList[i].INPUT_MONTH){
                            seryHourData.push(dataList[i][legendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        seryHourData.push(0)
                    }
                }
                seriesData.push(seryHourData)
            }
            println(seriesData)
        }


        [productList:productList,productid:params.productid?:'',year:params.year?:'',mkseriesData:mkseriesData,mklegendDataList:mklegendDataList,mkpercentItemList:mkpercentItemList,
         percentItemList:percentItemList,dayList:dayList,legendDataList:legendDataList,
         seriesData:seriesData,testDataList:testDataList,dataListNo1:dataListNo1,
         pageSize:pageSize,pageSizeList:pageSizeList,dataListNo:dataListNo]
    }


    def inputlist(){
        params.SYS_QUERY_NAME='inputlist'
        def inputList =dynamicQueryService.queryForList(params)
        [inputList:inputList]
    }
    //�޸�finaly��
    def updateinput(DwsInputY c){
        render (view:'updateinput',
                model:[input:c])
    }
    def addinput(DwsInputY cfu){
        cfu.save()
        render """{
                    "statusCode":"200",
                    "message":"${message(code: 'default.created.message', args: [message(code: 'DwsInputY.label', default: 'DwsFinalYield'), cfu.id])}",
                    "closeCurrent":true,
                    "forward":"",
                    "forwardConfirm":""
                    }"""
    }
    def deleteinput(DwsInputY c){

        c.delete()
        render """{
                    "statusCode":"200",
                    "message":"${message(code: 'default.created.message', args: [message(code: 'DwsInputY.label', default: 'DwsInputY'), c.id])}",
                    "closeCurrent":true,
                    "forward":"",
                    "forwardConfirm":""
                    }"""

    }

    def exportInput() {
        params.startno = 1
        params.endno = 5000
        def sysQueryName = params.SYS_QUERY_NAME
        def dataListNo=[]
        def dataListNo1=[]
        def mkdataList=[]
        def mklegendDataList=[]
        def mkpercentItemList=[]
        def mkseriesData = []
        def seryHourData = []
        def dataList=[]

        def legendDataList=[]
        def percentItemList=[]
        def seriesData = []
        def testDataList = []
        def totalCount=0

        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM')
        params.year=params.year?:'2018'
        request.setCharacterEncoding("UTF-8");//�����ַ�����
        response.setContentType("text/html;charset=UTF-8");
        def startTime = sdf.parse(params.year+'-01')
        def endTime =  sdf.parse(params.year+'-12')

        def dayList = DateUtils.getMonthList(startTime, endTime)

            params.SYS_QUERY_NAME = 'inputyearList'
            dataListNo = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'inputyieldList'
            dataListNo1 = dynamicQueryService.queryForList(params)
            params.SYS_QUERY_NAME = 'mkyieldList'
            mkdataList = dynamicQueryService.queryForList(params)
            mklegendDataList = ['YIELD','TARGETYIELD']
            mkpercentItemList = ['YIELD']

            for(def k=0;k<mklegendDataList.size();k++){

                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<mkdataList.size();i++){
                        if(dayList[j]==mkdataList[i].INPUT_MONTH){
                            seryHourData.push(mkdataList[i][mklegendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        seryHourData.push(0)
                    }
                }
                mkseriesData.push(seryHourData)
            }


            params.SYS_QUERY_NAME = 'jbyieldList'
            dataList = dynamicQueryService.queryForList(params)
            legendDataList = ['YIELD','TARGETYIELD']
            percentItemList = ['YIELD']

            for(def k=0;k<legendDataList.size();k++){

                for(def j=0;j<dayList.size();j++){
                    def flag = false;
                    for(def i=0;i<dataList.size();i++){
                        if(dayList[j]==dataList[i].INPUT_MONTH){
                            seryHourData.push(dataList[i][legendDataList[k]])
                            flag = true;
                        }
                    }
                    if(!flag){
                        seryHourData.push(0)
                    }
                }
                seriesData.push(seryHourData)
            }
            println mkseriesData
            println seriesData
        String filename = new String("${sysQueryName}.xls".getBytes("UTF-8"), "UTF-8");
        response.setContentType('application/vnd.ms-excel;charset=UTF-8')
        response.setHeader('Content-disposition', "attachment;filename=${filename};charset=UTF-8")
        response.setCharacterEncoding("UTF-8");
        render(template: "export${params.EXPORT_CONTENT_NAME}",
                model:  [productid:params.productid?:'',year:params.year?:'',mkseriesData:mkseriesData,mklegendDataList:mklegendDataList,mkpercentItemList:mkpercentItemList,
                         percentItemList:percentItemList,legendDataList:legendDataList,
                         seriesData:seriesData,testDataList:testDataList,dataListNo1:dataListNo1
                         ,dataListNo:dataListNo])
    }

}
