package te.rpt

class CheckerOutputByDayController {

    def dynamicQueryService
    def exportFileService

    def checkerOutputByDay() {

        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        def checkerOutputByDay=[]
        def totalCount=[]
        def tableHeadList=[]
        params.SYS_QUERY_NAME = 'checkerOutputByDayChecker'
        def checkerOutputByDayChecker = dynamicQueryService.queryForList(params)

        if(params.EXPORT_CONTENT_NAME=='Excel'){
            params.SYS_QUERY_NAME = 'checkerOutputByDay'
            checkerOutputByDay = dynamicQueryService.queryForList(params)

            if (checkerOutputByDay.size() > 0) {
                tableHeadList = checkerOutputByDay[0].keySet()
            }
            params.SYS_QUERY_NAME = 'checkerOutputByDayCount'
            totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        }

        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,checkerOutputByDayChecker:checkerOutputByDayChecker,
         startno:params.startno?:'',endno:params.endno?:'',checkerOutputByDay:checkerOutputByDay,
         checker:params.checker?:'',startTime:params.startTime?:'',endTime:params.endTime?:'']
    }

    def checkerOutputByDayStep(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10, 20, 50, 100]
        println(params.checker1+'aaa')
        def startTime=params.startTime
        def endTime=params.endTime
        params.SYS_QUERY_NAME = 'checkerOutputByDayStep'
        def checkerOutputByDayStep = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'checkerOutputByDayStepCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        def tableHeadList = []
        if (checkerOutputByDayStep.size() > 0) {
            tableHeadList = checkerOutputByDayStep[0].keySet()
        }
        //��ѯ
        params.SYS_QUERY_NAME = 'checkerOutputByDayCheckerStep'
        def checkerOutputByDayCheckerStep = dynamicQueryService.queryForList(params)
        [totalCount:totalCount,tableHeadList:tableHeadList,startTime:params.startTime?:'',endTime:params.endTime?:'',
         pageSizeList:pageSizeList,pageSize:pageSize,checkerOutputByDayStep:checkerOutputByDayStep,
         startno:params.startno?:'',endno:params.endno?:'',checkerOutputByDayCheckerStep:checkerOutputByDayCheckerStep,
         checker1:params.checker1?:'',checker2:params.checker2?:'']
    }
}
