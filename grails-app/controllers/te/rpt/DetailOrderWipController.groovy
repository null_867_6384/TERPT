package te.rpt

class DetailOrderWipController {

    def commonDataService
    def dynamicQueryService
    def index(Integer max) {
        def cellList = org.kdx.DataUtils.getCellList()
        def pageSizeList = org.kdx.DataUtils.getPageSizeList()

        [totalCount: 5, pageSize: 20, pageCurrent: 1, orderField: 'id', orderDirection: 'asc', pageSizeList: pageSizeList, cellList: cellList]
    }
    def detailOrderWip(){
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = (pageSize * (pageCurrent - 1)) + 1
        params.endno = pageSize * pageCurrent
        def pageSizeList = [10,20,50,100]
        params.SYS_QUERY_NAME = 'detailOrderWip'
        def detailOrderWip = dynamicQueryService.queryForList(params)
        def tableHeadList =  []
        if(detailOrderWip.size()>0){
            tableHeadList =  detailOrderWip[0].keySet()
        }
        params.SYS_QUERY_NAME = 'detailOrderWipCount'
        def totalCount = dynamicQueryService.queryForList(params)[0].TOTALCOUNT
        print(totalCount)
        params.SYS_QUERY_NAME = 'detailOrderLotList'
        def detailOrderLotList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'detailOrderPartList'
        def detailOrderPartList = dynamicQueryService.queryForList(params)
        params.SYS_QUERY_NAME = 'detailOrderLotTypeList'
        def detailOrderLotTypeList = dynamicQueryService.queryForList(params)
        [totalCount:totalCount,tableHeadList:tableHeadList,
         pageSizeList:pageSizeList,pageSize:pageSize,
         startno:params.startno?:'',endno:params.endno?:'',lotId:params.lotId?:'',
         detailOrderWip:detailOrderWip,detailOrderLotList:detailOrderLotList,detailOrderPartList:detailOrderPartList,detailOrderLotTypeList:detailOrderLotTypeList,
         partId:params.partId?:'',lotType:params.lotType?:'',startTime:params.startTime?:'',endTime:params.endTime?:'']

    }
}
