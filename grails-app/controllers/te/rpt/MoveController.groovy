package te.rpt

import java.text.SimpleDateFormat

/**
 * MOVE 报表
 */
class MoveController {

    def dynamicQueryService
    def exportFileService

    def move() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]

        SimpleDateFormat sdf = new SimpleDateFormat('yyyy-MM-dd')
        def startTime = params.startTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))
        def endTime = params.endTime ? sdf.parse(params.startTime) : sdf.parse(sdf.format(new Date().clearTime()))

        Calendar ca = Calendar.getInstance();
        ca.setTime(startTime);
        ca.add(Calendar.MONTH, 0);
        ca.set(Calendar.DAY_OF_MONTH, 1);
        startTime = ca.getTime();

        //结束时间月最后一日
        Calendar ca1 = Calendar.getInstance();
        ca1.setTime(endTime);
        ca1.add(Calendar.MONTH, 1);
        ca1.set(Calendar.DAY_OF_MONTH, 0);
        endTime = ca1.getTime();
        //将日期格式转化为字符串
        params.startTime = sdf.format(startTime)
        params.endTime = sdf.format(endTime)

        params.SYS_QUERY_NAME = 'moveLocationList'
        def moveLocationList = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (moveLocationList.size() > 0) {
            tableHeadList = moveLocationList[0].keySet()
        }

        params.SYS_QUERY_NAME = 'moveStageList'
        def moveStageList = dynamicQueryService.queryForList(params)
        def tableHeadList1 = []
        if (moveStageList.size() > 0) {
            tableHeadList1 = moveStageList[0].keySet()
        }
        if (params.export == 'xls') {
            def str = g.render(template: "${params.EXPORT_CONTENT_NAME}", model: [moveStageList: moveStageList, tableHeadList1: tableHeadList1])
            exportFileService.exportExcelbyWeb(response, "${params.SYS_QUERY_NAME}", str)
        } else {
            [pageSize     : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
             tableHeadList: tableHeadList, moveLocationList: moveLocationList, tableHeadList1: tableHeadList1, moveStageList: moveStageList,
             startTime    : params.startTime ?: '', endTime: params.endTime ?: '']
        }
    }

    def moveDetail() {
        def pageCurrent = params.pageCurrent ? Integer.parseInt(params.pageCurrent) : 1
        def pageSize = params.pageSize ? Integer.parseInt(params.pageSize) : 10
        params.startno = ((pageCurrent - 1) * pageSize) + 1
        params.endno = pageCurrent * pageSize
        def pageSizeList = [10, 20, 50, 100]

        params.SYS_QUERY_NAME = 'moveDetail'
        def moveDetail = dynamicQueryService.queryForList(params)
        def tableHeadList = []
        if (moveDetail.size() > 0) {
            tableHeadList = moveDetail[0].keySet()
        }

        [pageSize     : pageSize, pageSizeList: pageSizeList, startno: params.startno ?: '', endno: params.endno ?: '',
         tableHeadList: tableHeadList, moveDetail: moveDetail, stageId: params.stageId ?: '', startTime: params.startTime ?: '', endTime: params.endTime ?: '']
    }

}