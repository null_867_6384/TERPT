package te.rpt

class DwsInputY {

    String partType; //产品类别
    String inputYear;//年
    String inputMonth;//月
    int inQty;//投入数
    int outQty;//产出数
    double yield;//实际良率
    Date rptDateTime;//数据录入时间
    Date lastUpdated;//最后动作时间
    String lmUser;//最后动作人
    String reserveString1;
    String reserveString2;
    String reserveString3;
    String reserveString4;
    String reserveString5;
    int reserveInt1;
    int reserveInt2;
    Date reserveDate1;
    double reserveDouble1;
    static constraints = {
        partType nullable: true
        inputYear nullable: true
        inputMonth nullable: true
        inQty nullable: true
        outQty nullable: true
        yield nullable: true
        rptDateTime nullable: true
        lastUpdated nullable: true
        lmUser nullable: true
        reserveString1 nullable: true
        reserveString2 nullable: true
        reserveString3 nullable: true
        reserveString4 nullable: true
        reserveString5 nullable: true
        reserveInt1 nullable: true
        reserveInt2 nullable: true
        reserveDate1 nullable: true
        reserveDouble1 nullable: true
    }
}
