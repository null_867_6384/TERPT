

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/rework/rework" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="Report_084stepTwo" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <input type="hidden" name="reworkCode" value="${params.reworkCode}"/>
            <label>批号：</label>
            <input type="text" name="lotId" value="${lotId}"/>
            <label>产品名称：</label>
            <g:select name="productid" from="${productList}" data-toggle="selectpicker" data-live-search="true" optionKey="PRODUCTID" optionValue="PRODUCTID" value="${productid}" noSelection="['':'ALL']" />&nbsp;
            <label>开始时间：</label>
            <input type="text" name="startTime" readonly="readonly" value="${startTime}" data-pattern="yyyy-MM-dd HH:mm:ss"  data-toggle="datepicker" placeholder="FROM">&nbsp;&nbsp;&nbsp;&nbsp;
            <label>结束时间：</label>
            <input type="text" name="endTime"readonly="readonly" value="${endTime}" data-pattern="yyyy-MM-dd HH:mm:ss"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            %{--<button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/bb214074/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;--}%
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <table>
        <div id="Report084main" style="width:100%;height:50%;" ></div>
        <script  src="${request.getContextPath()}/js/echarts/echarts.js"></script>
        <script  src="${request.getContextPath()}/js/echarts/echarts-tool.js"></script>
        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var contextPath = "${request.getContextPath()}";
            eChartsTool.init(contextPath);
            var data=[];
            <g:each in="${Report_084img}" status="i" var="tableInstance">
            data.push('${tableInstance.REWORK_CODE}');
            </g:each>

            // 指定图表的配置项和数据
            var option = {
                /*backgroundColor: 'rgba(0,0,0,0)', // 工具箱背景颜色*/
                // 图表标题
                title: {

                },

                tooltip : {
                    trigger: 'rework123'
                },

                legend: {
                    data:['返工数量','返工比率']

                },
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:0,
                            margin:5
                        },
                        "data": data
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        name:'返工数量',
                        axisLabel : {
                            formatter: '{value}'
                        },
                        show:true
                    },
                    {
                        type : 'value',
                        name:'返工比率',

                        axisLabel : {
                            formatter: '{value}%'
                        },
                        show:true
                    }
                ],
                series : [
                    {
                        name: '返工数量',
                        type: 'bar',
                        data:${Report_084img.REWORK_QTY}
                    },
                    {
                        name: '返工比率',
                        type: 'line',
                        data:${list},
                        yAxisIndex: 1
                    }]
            };
            // 使用刚指定的配置项和数据显示图表。
            eChartsTool.setOption("roma", document.getElementById('Report084main'), option);
        </script>
    </table>
    <div>
<table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
    <thead>
    <g:if test="${tableHeadList.size()>0}">
    <tr>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
                <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
        </g:each>
    </tr>
        </thead>
        <tbody>
    <g:each in="${Report_084List}" status="i" var="dataInstance">
        <tr>
            <g:each in="${tableHeadList}" status="j" var="headInstance">
                %{--<g:if test="${headInstance=='REWORK_CODE'}">
                    <td><a href="${request.getContextPath()}/bb214084/Report_084step?lotId=<%=dataInstance.REWORK_CODE%>&lotId=<%=dataInstance.REWORK_DESC%>" data-toggle="navtab"  data-id="dialog-normal" data-options="{id:'table841', title:'返工折线图'}">${dataInstance["${headInstance}"] }</a></td>
                </g:if>--}%

                <g:if test="${headInstance=='REWORK_QTY'}">
                    <td><a href="${request.getContextPath()}/rework/reworkDetail?SYS_QUERY_NAME=reworkDetail&reworkCode=<%=dataInstance.REWORK_CODE%>" data-toggle="navtab"  data-id="dialog-normal" data-options="{id:'table84', title:'返工明细表'}">${dataInstance["${headInstance}"] }</a></td>
                </g:if>
                <g:else>
                    <td>${dataInstance["${headInstance}"]}</td>
                </g:else>
            </g:each>
        </tr>
    </g:each>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel" />
    </g:else>
</tbody>
</table>
</div>
</div>
<g:render template="../template/pagination" />
</html>