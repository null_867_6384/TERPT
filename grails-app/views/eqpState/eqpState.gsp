<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/eqpState/eqpState" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="eqpIdStateList" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="eqpStateExcel" />
            <label>location：</label>
            <g:select name="location" from="${locationList}" data-toggle="selectpicker" data-live-search="true" optionKey="location" optionValue="location" value="${location}" noSelection="['':'ALL']" />&nbsp;
            <label>EqpId：</label>
            <g:select name="eqpid" from="${eqpIdList}" data-toggle="selectpicker" data-live-search="true" optionKey="eqpid" optionValue="eqpid" value="${eqpid}" noSelection="['':'ALL']" />&nbsp;
            <label>Status：</label>
            <g:select name="eqpstate" from="${eqpStateList}" data-toggle="selectpicker" data-live-search="true" optionKey="eqpstate" optionValue="eqpstate" value="${eqpstate}" noSelection="['':'ALL']" />&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            %{--<button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/tv/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;--}%
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/eqpState/eqpState?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>
<div class="bjui-pageContent tableContent">
    <g:if test="${eqpIdStateList.size() > 0}">
        <g:render template="eqpStateExcel"></g:render>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel" />
    </g:else>
</div>
<g:render template="../template/pagination" />