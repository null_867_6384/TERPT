<table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
    <thead>
    <tr>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
            <g:if test="${headInstance != 'RN'}">
                <th width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
            </g:if>
        </g:each>
    </tr>
    </thead>
    <tbody>
    <g:each in="${eqpIdStateList}" status="i" var="dataInstance">
        <tr>
            <td>${dataInstance.EQP_TYPE}</td>
            <td>${dataInstance.EQPID}</td>
            <td>${dataInstance.DESCRIPTION}</td>
            <g:if test="${dataInstance.STATE == 'RUN'}">
                < style="background-color: chartreuse">${dataInstance.STATE}</td>
            </g:if>
            <g:else>
                <td>${dataInstance.STATE}</td>
            </g:else>
        </td>
            <td>${dataInstance.LOCATION}</td>
            <td>${dataInstance.UPDATED}</td>
            <td>${dataInstance.DURATION}</td>
        </tr>
    </g:each>
    </tbody>
</table>