<%@ page contentType="text/html;charset=UTF-8" %>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/move/move" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="moveStageList"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="excel"/>
            <input type="hidden" name="reworkCode" value="${params.reworkCode}"/>

            <label>开始时间：</label>
            <input type="text" name="startTime" value="${startTime}" data-toggle="datepicker">
            <label>结束时间：</label>
            <input type="text" name="endTime" value="${endTime}" data-toggle="datepicker">

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/move/move?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <div>
        <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
            <g:if test="${tableHeadList.size() > 0}">
                <tr>
                    <th>No.</th>
                    <g:each in="${tableHeadList}" status="j" var="headInstance">
                        <th><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:each>
                </tr>
                </thead>
                <tbody>
                <g:each in="${moveLocationList}" status="i" var="dataInstance">
                    <tr>
                        <td>${i + 1}</td>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">

                            <td>${dataInstance["${headInstance}"]}</td>

                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
            <g:render template="../template/emptyPanel" />
            </g:else>
        </tbody>
        </table>
    </div>
    &nbsp;
    <div>
       <g:render template="excel"></g:render>
    </div>
</div>
