<table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
<thead>
    <g:if test="${tableHeadList1.size() > 0}">
        <tr>
            <th>No.</th>
            <g:each in="${tableHeadList1}" status="j" var="headInstance1">
                <th><g:message code="${headInstance1}" default="${headInstance1}"/></th>
            </g:each>
        </tr>
        </thead>
        <tbody>
        <g:each in="${moveStageList}" status="i" var="dataInstance1">
            <tr>
                <td>${i + 1}</td>
                <g:each in="${tableHeadList1}" status="j" var="headInstance1">

                    <g:if test="${headInstance1 == 'STAGE_ID'}">
                        <td><a href="${request.getContextPath()}/move/moveDetail?SYS_QUERY_NAME=moveDetail&stageId=<%=dataInstance1.STAGE_ID%>&startTime=<%=params.startTime%>&endTime=<%=params.endTime%>"
                               data-toggle="dialog" data-width="900" data-height="400" data-id="dialog-normal"
                               data-options="{id:'moveDetail', title:'MOVE明细表'}">${dataInstance1["${headInstance1}"]}</a>
                        </td>
                    </g:if>
                    <g:else>
                        <td>${dataInstance1["${headInstance1}"]}</td>
                    </g:else>

                </g:each>
            </tr>
        </g:each>
    </g:if>
    <g:else>
    <g:render template="../template/emptyPanel" />
    </g:else>
</tbody>
</table>