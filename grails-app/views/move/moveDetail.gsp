
<%@ page contentType="text/html;charset=UTF-8" %>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/move/moveDetail?stageId=${params.stageId}" method="post">
        <div class="bjui-searchBar">
            %{--<button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/tv/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;--}%
        </div>
    </form>
</div>


<div class="bjui-pageContent tableContent">
    <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
        <tr>
            <th>No.</th>
            <g:each in="${tableHeadList}" status="j" var="headInstance">
                <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
            </g:each>
        </tr>
        </thead>
        <tbody>
        <g:each in="${moveDetail}" status="i" var="dataInstance">
            <tr>
                <td>${i+1}</td>
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <td>${dataInstance["${headInstance}"]}</td>
                </g:each>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>

