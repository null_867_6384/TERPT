<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/productEfficiency/productEfficiency" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="productEfficiency" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label >进站时间：</label>&nbsp;
            <input type="text" name="startTime" value="${startTime}" data-rule="required" data-pattern="yyyy-MM-dd HH:mm:ss"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime" value="${endTime}" data-rule="required" data-pattern="yyyy-MM-dd HH:mm:ss"  data-toggle="datepicker"  placeholder="TO">&nbsp;&nbsp;
            <label >出站时间：</label>&nbsp;
            <input type="text" name="startT" value="${startT}" data-pattern="yyyy-MM-dd HH:mm:ss"   data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endT" value="${endT}" data-pattern="yyyy-MM-dd HH:mm:ss"   data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >订单号：</label>&nbsp;
            <input type="text" name="docId" value="${docId}"/>
            <label >设备号：</label>&nbsp;
            <input type="text" name="eqpId" value="${eqpId}"/>
            <label >批次号：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${productEfficiency.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${productEfficiency.size()>0}">
                <g:each in="${productEfficiency}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>

                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />