<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/coaReportFor/coaReportFor" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="coaReportFor" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label >LOT NO：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${coaReportFor.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <tbody>
            <tr><td colspan="11" style="text-align: right">编号: S/QRBC8600100-05</td></tr>
            <tr>
                <td rowspan="2" colspan="4" style="text-align: center">Quality Inspection Report</td>
                <td colspan="7" style="text-align: center">SHANGHAI SHENHE THERMO-MAGNETICS ELECTRONICS CO., LTD.</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">Inspector</td>
                <td colspan="4" style="text-align: center">Quality Assurance</td>
            </tr>
            <tr>
                <td style="text-align: center">PO</td>
                <td colspan="3" style="text-align: center">${coaReportFor.PARENT_ID[0]}</td>
                <td colspan="3" style="text-align: center">${coaReportFor.OPERATOR[0]}</td>
                <td colspan="4" style="text-align: center">8995</td>
            </tr>
            <tr>
                <td style="text-align: center">Spec.</td>
                <td colspan="3" style="text-align: center">${coaReportFor.SPEC1[0]}</td>
                <td colspan="3" style="text-align: center">Customer</td>
                <td colspan="4" style="text-align: center">${coaReportFor.DESCRIPTION[0]}</td>
            </tr>
            <tr>
                <td style="text-align: center">DATE</td>
                <td colspan="2" style="text-align: center">${coaReportFor.MEASURE_TIME[0]}</td>
                <td colspan="1" style="text-align: center">Qty:${coaReportFor.TRACK_IN_MAIN_QTY[0]}</td>
                <td rowspan="${coaReportFor.size()}" style="text-align: center">材料</td>
                <td colspan="2" style="text-align: center">CopperLOT NO</td>
                <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[0]}</td>
            </tr>
            <tr>
                <g:if test="${coaReportFor.size()!=1}">
                    <td rowspan="${coaReportFor.size()-1}" style="text-align: center">LOT NO</td>
                    <td colspan="3" rowspan="${coaReportFor.size()-1}" style="text-align: center">${coaReportFor.LOT_ID[0]}</td>
                </g:if>
                <g:else>
                    <td rowspan="1" style="text-align: center">LOT NO</td>
                    <td colspan="3" rowspan="1" style="text-align: center">${coaReportFor.LOT_ID[0]}</td>
                </g:else>

                <g:if test="${coaReportFor.size()>=2}">
                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[1]}</td>
                </g:if>

            </tr>
            <g:if test="${coaReportFor.size()>=3}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[2]}</td>
                </tr>
            </g:if>
            <g:if test="${coaReportFor.size()>=4}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[3]}</td>
                </tr>
            </g:if>


            <g:if test="${coaReportFor.size()>=5}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[4]}</td>
                </tr>
            </g:if>


            <g:if test="${coaReportFor.size()>=6}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReportFor.MLOT_ID[5]}</td>
                </tr>
            </g:if>
            <tr>
                <td style="text-align: center">NO</td>
                <td style="text-align: center">Items</td>
                <td style="text-align: center">Instrument</td>
                <td style="text-align: center">Method</td>
                <td colspan="1" style="text-align: center">1</td>
                <td colspan="1" style="text-align: center">2</td>
                <td colspan="1" style="text-align: center">3</td>
                <td colspan="1" style="text-align: center">4</td>
                <td colspan="1" style="text-align: center">5</td>
                <td colspan="2" style="text-align: center">RESULT</td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: center">无</td>
                <td rowspan="2" style="text-align: center">外观</td>
                <td rowspan="2" style="text-align: center">台灯</td>
                <td rowspan="2" style="text-align: center">目检</td>
                <td colspan="5" style="text-align: center">无裂纹</td>
                <td colspan="2" rowspan="${coaReportForEdc.size+3}" style="text-align: center"></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center">瓷片无翘曲、残缺、铜片无脱落、破损、表面不良、气泡符合客户标准。</td>
            </tr>
            <tr>
                <td style="text-align: center">覆铜陶瓷边缘毛刺</td>
                <td style="text-align: center">台灯</td>
                <td style="text-align: center">目检</td>
                <td colspan="5" style="text-align: center">PASS</td>
            </tr>
            <g:each in="${coaReportForEdc}" var="edc" status="j">
                <tr>
                    <td style="text-align: center">${j+1}</td>
                    <td style="text-align: center">${edc.DESCRIPTION}</td>
                    <td style="text-align: center">${edc.EQP_DESC}</td>
                    <td style="text-align: center">${edc.LL}</td>
                    <td style="text-align: center" COLSPAN="5">${edc.DC_DATA}</td>

                </tr>
            </g:each>



            </tbody>
        </table>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel"></g:render>
    </g:else>
</div>
