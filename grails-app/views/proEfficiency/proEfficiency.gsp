<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/ProEfficiency/proEfficiency" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="proEfficiency" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
             <label >设备名称*：</label>&nbsp;
        <g:select name="eqp" from="${proEfficiencyEqp}" data-toggle="selectpicker" data-live-search="true" optionKey="eqp_desc" optionValue="eqp_desc" value="${eqp}" noSelection="['':'ALL']" />&nbsp;
            <label >产品名称：</label>&nbsp;
        <g:select name="partDesc" from="${proEfficiencyPart}" data-toggle="selectpicker" data-live-search="true" optionKey="spec1" optionValue="spec1" value="${partDesc}" noSelection="['':'ALL']" />&nbsp;
            <label >订单号：</label>&nbsp;
        <g:select name="order" from="${proEfficiencyOrder}" data-toggle="selectpicker" data-live-search="true" optionKey="parent_id" optionValue="parent_id" value="${order}" noSelection="['':'ALL']" />&nbsp;
            <label >日期*：</label>&nbsp;
            <input type="text" name="startTime" data-pattern="yyyy-MM-dd" value="${startTime}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime" data-pattern="yyyy-MM-dd" value="${endTime}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${proEfficiency.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
%{--
            <div><h2 style="color: black;text-align: center">DCB 制造日报表</h2></div>
--}%
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${proEfficiency.size()>0}">
                <g:each in="${proEfficiency}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <td>${dataInstance["${headInstance}"]}</td>
                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />
