<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/DetailOrderWip/detailOrderWip" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="detailOrderWip" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label >批号：</label>&nbsp;
        <g:select name="lotId" from="${detailOrderLotList}" data-toggle="selectpicker" data-live-search="true" optionKey="lot_id" optionValue="lot_id" value="${lotId}" noSelection="['':'ALL']" />&nbsp;
            <label >产品id：</label>&nbsp;
        <g:select name="partId" from="${detailOrderPartList}" data-toggle="selectpicker" data-live-search="true" optionKey="part_name" optionValue="part_name" value="${partId}" noSelection="['':'ALL']" />&nbsp;
            <label >批次类型：</label>&nbsp;
        <g:select name="lotType" from="${detailOrderLotTypeList}" data-toggle="selectpicker" data-live-search="true" optionKey="lot_type" optionValue="lot_type" value="${lotType}" noSelection="['':'ALL']" />&nbsp;
            <label >时间：</label>&nbsp;
            <input type="text" name="startTime" value="${startTime}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime" value="${endTime}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>

<div class="panel-body">
    <div style="mini-width:400px;height:450px;" id="cellGlassOutputDiv"></div>
</div>

<div class="bjui-pageContent">
    <g:if test="${detailOrderWip.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${detailOrderWip.size()>0}">
                <g:each in="${detailOrderWip}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>
                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />