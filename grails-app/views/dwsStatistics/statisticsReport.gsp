<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch"
          action="${request.getContextPath()}/dwsStatistics/statisticsReport" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="statisticsList"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel"/>
            <input name="edc_from" type="radio" value="LOT" id="j_form_radio"
                   <g:if test="${edc_from == 'LOT'}">checked="checked"</g:if>/>
            <label>InLine</label>
            <input name="edc_from" type="radio" value="GENERAL" id="j_form_radio"
                   <g:if test="${edc_from == 'GENERAL'}">checked="checked"</g:if>/>
            <label>Offine</label>&nbsp;&nbsp;&nbsp;&nbsp;
            <label class="row-label">开始时间：<strong><span class="text-danger">*</span></strong></label>
            <input name="startTime" value="${startTime}" data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   type="text" data-rule="required">
            <label class="row-label">结束时间：<strong><span class="text-danger">*</span></strong></label>
            <input name="endTime" value="${endTime}" data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   type="text" data-rule="required"><br>
            <label>部门：</label>
            <g:select name="department" from="${departmentList}" class="selectpicker" data-live-search="true"
                      data-live-search-placeholder="查找" noSelection="['': '']" data-width="200"
                      data-toggle="selectpicker" data-size="12" optionKey="department"
                      optionValue="department" value="${department}"/>
            <label>Chart ID：</label>
            <g:select name="name" from="${charidList}" class="selectpicker" data-live-search="true"
                      data-live-search-placeholder="查找" noSelection="['': '']" data-width="200"
                      data-toggle="selectpicker" data-size="12" optionKey="name"
                      optionValue="name" value="${name}"/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;

        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <g:if test="${result.size() > 0}">
        <g:render template="statisticsExcel"></g:render>
    </g:if>
</div>
<g:render template="../template/pagination"/>