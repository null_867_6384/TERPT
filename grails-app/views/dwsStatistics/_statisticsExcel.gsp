<table data-toggle="tablefixed" data-width="100%" data-nowrap="true" id="ooc">
    <thead>
    <tr id="t1">
        <th width="50">
            NO.
        </th>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
            <g:if test="${headInstance != 'RN'}">
                <th width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
            </g:if>
        </g:each>
    </tr>
    </thead>
    <tbody>
    <g:if test="${result.size() > 0}">
        <g:each in="${result}" status="i" var="dataInstance">
            <tr>
                <td>
                    ${i + 1}
                </td>
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance != 'RN'}">
                        <td>${dataInstance["${headInstance}"]}</td>
                    </g:if>
                </g:each>
            </tr>
        </g:each>
    </g:if>
    </tbody>
</table>