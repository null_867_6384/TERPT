<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/coaReport/coaReport" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="coaReport" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label >LOT NO：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${coaReport.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <tbody>
                <tr><td colspan="11" style="text-align: right">编号:S/QRBC8600100-02</td></tr>
                <tr>
                    <td rowspan="2" colspan="4" style="text-align: center">品质检查表</td>
                    <td colspan="7" style="text-align: center">上海申和热磁有限公司</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center">检查担当者</td>
                    <td colspan="4" style="text-align: center">品质担当者(印)</td>
                </tr>
                <tr>
                    <td style="text-align: center">制造订单号</td>
                    <td colspan="3" style="text-align: center">${coaReport.PARENT_ID[0]}</td>
                    <td colspan="3" style="text-align: center">${coaReport.OPERATOR[0]}</td>
                    <td colspan="4" style="text-align: center">8995</td>
                </tr>
                <tr>
                    <td style="text-align: center">规格</td>
                    <td colspan="3" style="text-align: center">${coaReport.SPEC1[0]}</td>
                    <td colspan="3" style="text-align: center">客户名称</td>
                    <td colspan="4" style="text-align: center">${coaReport.DESCRIPTION[0]}</td>
                </tr>
                <tr>
                    <td style="text-align: center">检查日期</td>
                    <td colspan="2" style="text-align: center">${coaReport.MEASURE_TIME[0]}</td>
                    <td colspan="1" style="text-align: center">数量:${coaReport.TRACK_IN_MAIN_QTY[0]}</td>
                    <td rowspan="${coaReport.size()}" style="text-align: center">材料</td>
                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[0]}</td>
                </tr>
                <tr>
                    <g:if test="${coaReport.size()!=1}">
                    <td rowspan="${coaReport.size()-1}" style="text-align: center">LOT NO</td>
                    <td colspan="3" rowspan="${coaReport.size()-1}" style="text-align: center">${coaReport.LOT_ID[0]}</td>
                    </g:if>
                    <g:else>
                        <td rowspan="1" style="text-align: center">LOT NO</td>
                        <td colspan="3" rowspan="1" style="text-align: center">${coaReport.LOT_ID[0]}</td>
                    </g:else>

                    <g:if test="${coaReport.size()>=2}">
                        <td colspan="2" style="text-align: center">CopperLOT NO</td>
                        <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[1]}</td>
                    </g:if>

                </tr>
                <g:if test="${coaReport.size()>=3}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[2]}</td>
                </tr>
                </g:if>
            <g:if test="${coaReport.size()>=4}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[3]}</td>
                </tr>
            </g:if>


            <g:if test="${coaReport.size()>=5}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[4]}</td>
                </tr>
            </g:if>


            <g:if test="${coaReport.size()>=6}">
                <tr>

                    <td colspan="2" style="text-align: center">CopperLOT NO</td>
                    <td colspan="4" style="text-align: center">${coaReport.MLOT_ID[5]}</td>
                </tr>
            </g:if>
                <tr>
                    <td style="text-align: center">序号</td>
                    <td style="text-align: center">检查项目</td>
                    <td style="text-align: center">检查仪器</td>
                    <td style="text-align: center">检查标准</td>
                    <td colspan="5" style="text-align: center">合格判定</td>
                    <td colspan="2" style="text-align: center">判定依据</td>
                </tr>
            <tr>
                <td rowspan="3" style="text-align: center">无</td>
                <td rowspan="2" style="text-align: center">外观</td>
                <td rowspan="2" style="text-align: center">台灯</td>
                <td rowspan="2" style="text-align: center">目检</td>
                <td colspan="5" style="text-align: center">无裂纹</td>
                <td colspan="2" rowspan="${coaReportEdc.size+3}" style="text-align: center"></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center">瓷片无翘曲、残缺、铜片无脱落、破损、表面不良、气泡符合客户标准。</td>
            </tr>
            <tr>
                <td style="text-align: center">覆铜陶瓷边缘毛刺</td>
                <td style="text-align: center">台灯</td>
                <td style="text-align: center">目检</td>
                <td colspan="5" style="text-align: center">PASS</td>
            </tr>
            <g:each in="${coaReportEdc}" var="edc" status="j">
                <tr>
                    <td style="text-align: center">${j+1}</td>
                    <td style="text-align: center">${edc.DESCRIPTION}</td>
                    <td style="text-align: center">${edc.EQP_DESC}</td>
                    <td style="text-align: center">${edc.LL}</td>
                    <td style="text-align: center" COLSPAN="5">${edc.DC_DATA}</td>

                </tr>
            </g:each>



            </tbody>
        </table>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel"></g:render>
    </g:else>
</div>
