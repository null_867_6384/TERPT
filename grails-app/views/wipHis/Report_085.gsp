<%--
  Created by IntelliJ IDEA.
  User: LX
  Date: 2017/5/27
  Time: 11:38
--%>
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/wipHis/Report_085"
          method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="Report_085"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="excel"/>

            <label>lotID：</label>
            <input type="text" name="lot_id" value="${lot_id}"/>

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/wipHis/Report_085?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <g:render template="excel"></g:render>
</div>
<g:render template="../template/pagination"/>
