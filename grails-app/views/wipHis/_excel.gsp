<table data-toggle="tablefixed" data-width="100%" data-nowrap="true"
       class="table table-bordered table-hover table-striped table-top">
    <thead>
    <tr>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
            <g:if test="${headInstance != 'RN'}">
                <th width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
            </g:if>
        </g:each>
    </tr>
    </thead>
    <tbody>
    <g:each in="${Report_085List}" status="i" var="dataInstance">
        <tr>
            <g:each in="${tableHeadList}" status="j" var="headInstance">
                <g:if test="${headInstance != 'RN'}">
                    <g:if test="${headInstance == 'ZUOYESHIJIAN' && dataInstance["${headInstance}"] != null}">
                        <td>${dataInstance["${headInstance}"]}小时</td>
                    </g:if>
                    <g:else>
                        <td>${dataInstance["${headInstance}"]}</td>
                    </g:else>
                </g:if>
            </g:each>
        </tr>
    </g:each>
    </tbody>
</table>