<style type="text/css">
th {
    width: 1em;
}

tr, td {
    border: 1px #ADBFCD solid;
}

.odd {
    background-color: #EBEBEB;
}
</style><table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
    <thead>
    <tr>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
            <g:if test="${headInstance!='RN'}">
                <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
            </g:if>
        </g:each>
    </tr>
    </thead>
    <tbody>
    <g:each in="${wipTrendList}" status="i" var="dataInstance">
        <tr>
            <g:each in="${tableHeadList}" status="j" var="headInstance">
                <g:if test="${headInstance!='RN'}">


                    <td>${dataInstance["${headInstance}"]}</td>

                </g:if>
            </g:each>
        </tr>
    </g:each>
    </tbody>
</table>