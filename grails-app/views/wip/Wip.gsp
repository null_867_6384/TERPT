<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/wip/Wip" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="wipTrend" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="wipExcel" />
            <label>产品名称：</label>
            <g:select name="productid" from="${productList}" data-toggle="selectpicker" data-live-search="true" optionKey="PRODUCTID" optionValue="PRODUCTID" value="${productid}" noSelection="['':'ALL']" />&nbsp;
            <label>批次类型：</label>
            <g:select name="lotType" from="${lotTypeList}" data-toggle="selectpicker" data-live-search="true" optionKey="LOTTYPE" optionValue="LOTTYPE" value="${lotType}" noSelection="['':'ALL']" />&nbsp;
            <label>生产阶段：</label>
            <g:select name="stage" from="${stageList}" data-toggle="selectpicker" data-live-search="true" optionKey="STAGE" optionValue="STAGE" value="${stage}" noSelection="['':'ALL']" />&nbsp;
            <label>批次状态：</label>
            <g:select name="state" from="${stateList}" data-toggle="selectpicker" data-live-search="true" optionKey="STATE" optionValue="STATE" value="${state}" noSelection="['':'ALL']" />&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue"
                    onclick="javascript:bjuiExportExl('#pagerForm', '${request.getContextPath()}/wip/exportExce')"
                    data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>
<div class="bjui-pageContent tableContent">
    <g:if test="${wipTrendList.size() > 0}">
        <g:render template="exportwipExcel"></g:render>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel" />
    </g:else>
</div>
<g:render template="../template/pagination" />