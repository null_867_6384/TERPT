<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/mkwdx/mkwdx" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="mokuaiwangongdan" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label>客户名称：</label>&nbsp;
            <input type="text" name="customCode" value="${customCode}"/>
            <label>规格型号：</label>&nbsp;
            <input type="text" name="part" value="${part}"/>
            <label>批次号：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>
            <label >计划完工日期：</label>&nbsp;
            <input type="text" name="startT" value="${startT}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endT" value="${endT}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >交货日期：</label>&nbsp;
            <input type="text" name="start" value="${start}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="end" value="${end}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >实际完工日期：</label>&nbsp;
            <input type="text" name="startTime" value="${startTime}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime" value="${endTime}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${mokuaiwangongdan.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${mokuaiwangongdan.size()>0}">
                <g:each in="${mokuaiwangongdan}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>

                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />