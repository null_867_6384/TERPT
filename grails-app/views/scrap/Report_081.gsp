<%--
  Created by IntelliJ IDEA.
  User: LX
  Date: 2017/5/27
  Time: 11:38
--%>

<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/scrap/Report_081"
          method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="Report_081"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel"/>

            <label>产品名称：</label>
            <input type="text" name="name" value="${name}"/>
            <label>批次类型：</label>
            <input type="text" name="lotType" value="${lotType}"/>
            <label>报废区域：</label>
            <input type="text" name="location" value="${location}"/><br/>
            <label>组别：</label>
            <input type="text" name="stageId" value="${stageId}"/>
            <label>报废时间：</label>
            <input type="text" name="startTime" value="${startTime}" data-toggle="datepicker"
                   placeholder="FROM">&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="endTime" value="${endTime}" data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/scrap/Report_081?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <g:render template="excel"></g:render>
</div>
<g:render template="../template/pagination"/>
