<g:if test="${it.Use_Flag}">
    <span class="text-success"><i class="fa fa-check-circle-o fa-lg"></i></span>
</g:if>
<g:else>
    <span class="text-danger"><i class="fa fa-times-circle-o fa-lg"></i></span>
</g:else>
