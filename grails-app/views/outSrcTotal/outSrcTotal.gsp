﻿<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/OutSrcTotal/outSrcTotal" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="outSrcTotal" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label>客户名称：</label>
            <g:select name="custName" from="${outsrcCustName}" data-toggle="selectpicker" data-live-search="true" optionKey="cust_name" optionValue="cust_name" value="${custName}" noSelection="['':'--ALL--']" />&nbsp;
            <label>委外商：</label>
            <g:select name="outsrcUser" from="${outsrcUserList}" data-toggle="selectpicker" data-live-search="true" optionKey="COMPERATION_NAME" optionValue="COMPERATION_NAME" value="${outsrcUser}" noSelection="['':'--ALL--']" />&nbsp;
            <label >委外日期：</label>&nbsp;
            <input type="text" name="startT" data-pattern="yyyy-MM-dd" value="${startT}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endT" data-pattern="yyyy-MM-dd" value="${endT}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/outSrcTotal/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>
<div class="panel-body">
    <div style="mini-width:400px;height:450px;" id="cellGlassOutputDiv"></div>
</div>
<div class="bjui-pageContent">
    <g:if test="${outSrcTotal.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'&&headInstance!='露铜'&&headInstance!='露铜良率'&&headInstance!='表面镍污'&&headInstance!='表面镍污良率'&&headInstance!='间距镍污'&&headInstance!='间距镍污良率'&&headInstance!='划痕'&&headInstance!='划痕良率'&&headInstance!='镀镍其他不良'&&headInstance!='镀镍其他不良良率'}">
                        <th rowspan="2" width="125" align="center" valign="middle"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                    <g:elseif test="${headInstance=='露铜'||headInstance=='表面镍污'||headInstance=='间距镍污'||headInstance=='划痕'||headInstance=='镀镍其他不良'}">
                        <th colspan="2" width="125"  align="center" valign="middle">${headInstance}</th>
                    </g:elseif>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${outSrcTotal.size()>0}">
                <g:each in="${outSrcTotal}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td width="125" align="center" valign="middle">${dataInstance["${headInstance}"]}</td>
                            </g:if>
                        </g:each>
                    </tr>
                </g:each>
            %{--<tr>
                <td colspan="6">小计(当前页)</td>
                <td name="sum">0</td>
            </tr>
            <tr>
                <td colspan="6">合计(总)</td>
                <td >${orderStatusAnalysisStepSum[0].SUM_QTY}</td>
            </tr>--}%
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />

