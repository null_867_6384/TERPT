<div class="bjui-pageFooter">
    <div class="pages">
        <span>每页&nbsp;</span>

        <div class="selectPagesize">
            <g:select name="pageSize" from="${pageSizeList}" value="${pageSize}" data-toggle="selectpicker" data-toggle-change="changepagesize"/>&nbsp;
        </div>
        <span>&nbsp;条，共 ${totalCount} 条</span>
    </div>
    <div id="box" class="pagination-box" data-toggle="pagination" data-total="${totalCount}" data-page-size="${pageSize}"
         data-page-current="${pageCurrent}">
    </div>
</div>
