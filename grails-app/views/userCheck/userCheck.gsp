<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/UserCheck/userCheck" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="userCheck" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />

            <label >检查日期：</label>&nbsp;
            <input type="text" name="startTime" data-pattern="yyyy-MM-dd HH:mm:ss" value="${startTime}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime"data-pattern="yyyy-MM-dd HH:mm:ss" value="${endTime}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label>订单号：</label>&nbsp;
            <input type="text" name="docId" value="${docId}"/>
            <label>批次号：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>
            <label>规格型号：</label>&nbsp;
            <input type="text" name="part" value="${part}"/><br/>
            <label >计划交货日期：</label>&nbsp;
            <input type="text" name="startT" data-pattern="yyyy-MM-dd HH:mm:ss" value="${startT}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endT" data-pattern="yyyy-MM-dd HH:mm:ss" value="${endT}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >客户名称：</label>&nbsp;
            <input type="text" name="userDesc" value="${userDesc}"/>
            <label >备注：</label>&nbsp;
            <input type="text" name="beizhu" value="${beizhu}"/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${userCheck.size()>0}">
                <g:each in="${userCheck}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <td>${dataInstance["${headInstance}"]}</td>
                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>

</div>
<g:render template="../template/pagination" />
