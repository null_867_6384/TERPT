﻿
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/input/inputlist" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="inputlist" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <a  class="btn btn-green"  data-toggle="dialog"
                data-url="${request.getContextPath()}/input/updateinput"
                data-title="新增">新增</a>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/tv/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;

        </div>
    </form>
</div>
<div >

        <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr>
                <th width="80">产品类型</th>
                <th width="80">年份</th>
                <th width="80">月份</th>
                <th width="80">投入数量</th>
                <th width="80">良品数量</th>
                <th width="80">良率</th>
                <th width="80">目标良率</th>
                <th width="110">最后操作人</th>
                <th width="110">最后操作时间</th>
                <th width="150">操作</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${inputList}" status="i" var="wipTrendInstance">
                <tr>
                    <td>${wipTrendInstance.PART_TYPE}</td>
                    <td>${wipTrendInstance.INPUT_YEAR}</td>
                    <td>${wipTrendInstance.INPUT_MONTH}</td>
                    <td>${wipTrendInstance.IN_QTY}</td>
                    <td>${wipTrendInstance.OUT_QTY}</td>
                    <td>${wipTrendInstance.YIELD}</td>
                    <td>${wipTrendInstance.RESERVE_DOUBLE1}</td>
                    <td>${wipTrendInstance.LM_USER}</td>
                    <td>${wipTrendInstance.LAST_UPDATED}</td>
                    <td>
                        <a  class="btn btn-green"  data-toggle="dialog"
                            data-url="${request.getContextPath()}/input/updateinput?id=${wipTrendInstance.ID}"
                            data-title="编辑">编辑</a>
                        <a  class="btn btn-red"   data-confirm-msg="确定要删除该行信息吗？"
                            data-toggle="doajax"  data-type="DELETE"
                            data-callback=function(){$.CurrentNavtab.navtab('refresh')}
                            href="${request.getContextPath()}/input/deleteinput?id=${wipTrendInstance.ID}"
                        >删除</a>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>

</div>


<script type="text/javascript">
    $(function () {
        $.CurrentNavtab.find('#start-date-input,#end-date-input').datetimepicker({
            format:'yyyy-mm-dd hh',
            autoclose: true,
            todayBtn: true,
            startView:1,
            minView:1,
            pickerPosition: "bottom-left",
            language:'zh-CN',
            startDate:'2016-01-01'
        });

        $.CurrentNavtab.find('.selectpicker').selectpicker();
    })
</script>