﻿<div class="bjui-pageHeader">
</div>
<div class="bjui-pageContent tableContent">

    <form id="addinputfrom" data-reload-navtab="true" data-toggle="validate"
          action="${request.getContextPath()}/input/addinput?uid=${input?.id}" method="post"
          onsubmit="return check11();">
        <div class="bjui-searchBar">
            <table>
                <tr>
                    <td><label>产品类型<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                    <input name="partType" readonly value="${input.partType}"/>
                    </td>
                </tr>
                <input type="hidden" name="id" value="${input?.id}"/>
                <tr>
                    <td><label>年份<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="inputYear" readonly value="${input.inputYear}"/>
                    </td>
                </tr>
                <tr>
                    <td><label>月份<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="inputMonth" readonly value="${input.inputMonth}"/>
                    </td>
                </tr>
                <tr>
                    <td><label>投入数量<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="inQty" readonly value="${input.inQty}"/>
                    </td>
                </tr>
                <tr>
                    <td><label>良品数量<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="outQty" readonly value="${input.outQty}"/>
                    </td>
                </tr>
                <tr>
                    <td><label>良率<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="yield" readonly value="${input.yield}"/>
                    </td>
                </tr>
                <tr>
                    <td><label>目标良率<strong><span class="text-danger">*</span></strong>：</label></td>
                    <td>
                        <input name="reserveDouble1" value="${input.reserveDouble1}"/>
                    </td>
                </tr>
                <tr>
                    <td><button type="submit" class="btn btn-green" >保存</button>&nbsp;</td>
                    <td><button type="button" class="btn btn-green" onclick='javascript:clear11()' >清空</button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </form>





</div>
<script type="text/javascript">
  function clear11() {

      $("input").val("");
  }
    function check11(){

        if(document.getElementById("filesn").value==""){
            return false
        }else{
            return true
        }
    }
</script>
