<table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
    <thead>
    <tr>
        <th width="110" align="center">产品类型</th>
        <th width="50" align="center">名称</th>
        <th width="50" align="center">1月</th>
        <th width="50" align="center">2月</th>
        <th width="50" align="center">3月</th>
        <th width="50" align="center">4月</th>
        <th width="50" align="center">5月</th>
        <th width="50" align="center">6月</th>
        <th width="50" align="center">7月</th>
        <th width="50" align="center">8月</th>
        <th width="50" align="center">9月</th>
        <th width="50" align="center">10月</th>
        <th width="50" align="center">11月</th>
        <th width="50" align="center">12月</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${dataListNo}" status="i" var="dataInstance">
        <tr>
            <td>${dataInstance.PART_TYPE}</td>
            <td>
                <g:if test="${dataInstance.PARAM_NAME=='in_qty'}">
                    检查总数
                </g:if>
                <g:elseif test="${dataInstance.PARAM_NAME=='out_qty'}">
                    良品数
                </g:elseif>
                <g:else>
                    良品率
                </g:else>
            </td>
            <td>${dataInstance.JAN}</td>
            <td>${dataInstance.FEB}</td>
            <td>${dataInstance.MAR}</td>
            <td>${dataInstance.APR}</td>
            <td>${dataInstance.MAY}</td>
            <td>${dataInstance.JUN}</td>
            <td>${dataInstance.JUL}</td>
            <td>${dataInstance.AUG}</td>
            <td>${dataInstance.SEP}</td>
            <td>${dataInstance.OCT}</td>
            <td>${dataInstance.NOV}</td>
            <td>${dataInstance.DEC}</td>
        </tr>
    </g:each>
    </tbody>
</table>

<table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
    <thead>
    <tr>
        <th width="110" align="center">产品类型</th>
        <th width="50" align="center">名称</th>
        <th width="50" align="center">1月</th>
        <th width="50" align="center">2月</th>
        <th width="50" align="center">3月</th>
        <th width="50" align="center">4月</th>
        <th width="50" align="center">5月</th>
        <th width="50" align="center">6月</th>
        <th width="50" align="center">7月</th>
        <th width="50" align="center">8月</th>
        <th width="50" align="center">9月</th>
        <th width="50" align="center">10月</th>
        <th width="50" align="center">11月</th>
        <th width="50" align="center">12月</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${dataListNo1}" status="i" var="dataInstance">
        <tr>
            <td>${dataInstance.PART_TYPE}</td>
            <td>
                <g:if test="${dataInstance.PARAM_NAME=='yield'}">
                    良品率
                </g:if>
                <g:else>
                    良品率目标
                </g:else>
            </td>
            <td>${dataInstance.JAN}</td>
            <td>${dataInstance.FEB}</td>
            <td>${dataInstance.MAR}</td>
            <td>${dataInstance.APR}</td>
            <td>${dataInstance.MAY}</td>
            <td>${dataInstance.JUN}</td>
            <td>${dataInstance.JUL}</td>
            <td>${dataInstance.AUG}</td>
            <td>${dataInstance.SEP}</td>
            <td>${dataInstance.OCT}</td>
            <td>${dataInstance.NOV}</td>
            <td>${dataInstance.DEC}</td>
        </tr>
    </g:each>
    </tbody>
</table>