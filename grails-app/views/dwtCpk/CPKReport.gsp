<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch"
          action="${request.getContextPath()}/dwtCpk/CPKReport" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="CPKList"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel"/>
            <label>起始日期：<strong><span class="text-danger">*</span></strong></label>
            <input name="startTime" value="${startTime}" data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   type="text" data-rule="required">
            <label class="row-label">结束时间：<strong><span class="text-danger">*</span></strong></label>
            <input name="endTime" value="${endTime}" data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm"
                   type="text" data-rule="required"><br>
            <label>产品名称：</label>
            <g:select name="partName" from="${partList}" data-toggle="selectpicker" data-live-search="true"
                      optionKey="name" optionValue="name" value="${partName}" data-size="12" data-width="200"
                      noSelection="['': '']" id="prod"/>&nbsp;
            <label>产品版本：</label>
            <g:select name="version" from="${partversion}" class="selectpicker" data-live-search="true"
                      data-live-search-placeholder="查找" noSelection="['': '===请选择===']" data-width="200"
                      data-toggle="selectpicker" data-size="12" optionKey="version"
                      optionValue="version" value="${version}"/>
            <label>Chart ID：</label>
            <g:select name="name" from="${charidList}" class="selectpicker" data-live-search="true"
                      data-live-search-placeholder="查找" noSelection="['': '===请选择===']" data-width="200"
                      data-toggle="selectpicker" data-size="12" optionKey="name" id="chartid"
                      optionValue="name" value="${name}"/>

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <g:if test="${CPKList.size() > 0}">
        <g:render template="CPKExcel"></g:render>
    </g:if>
</div>
<g:render template="../template/pagination"/>