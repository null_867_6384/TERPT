<table data-toggle="tablefixed" data-width="100%" data-nowrap="true" class="table table-bordered">
    <thead>

    <tr>
        <g:each in="${tableHeadList}" status="j" var="headInstance">
            <th width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
        </g:each>
    </tr>
    </thead>
    <tbody>
    <g:if test="${CPKList.size() > 0}">
        <g:each in="${CPKList}" status="i" var="dataInstance">
            <tr>
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <td>${dataInstance["${headInstance}"]}</td>
                </g:each>
            </tr>
        </g:each>
    </g:if>
    </tbody>
</table>