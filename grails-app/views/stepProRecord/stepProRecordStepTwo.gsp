

<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/StepProRecord/stepProRecordStepTwo"
          method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="stepProRecordStepTwo"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="stepProRecordStepTwo"/>

            %{--<label>产品名称：</label>
            <input type="text" name="name" value="${name}"/>
            <label>批次类型：</label>
            <input type="text" name="lotType" value="${lotType}"/>
            <label>报废区域：</label>
            <input type="text" name="location" value="${location}"/><br/>
            <label>组别：</label>
            <input type="text" name="stageId" value="${stageId}"/>
            <label>报废时间：</label>
            <input type="text" name="startTime" value="${startTime}" data-toggle="datepicker"
                   placeholder="FROM">&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="endTime" value="${endTime}" data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;--}%

            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/scrap/Report_081?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <div><h2 style="color: black;text-align: center">贴膜曝光显影记录表</h2></div>
        <thead>
            <tr>
                <td rowspan="7">订单号</td>
                <td rowspan="7">产品型号</td>
                <td rowspan="7">LOT.NO</td>
                <td rowspan="7">投入数</td>
                <td rowspan="7">产出数</td>
                <td>贴膜预热</td>
                <td colspan="6">贴膜</td>
                <td>曝光</td>
                <td colspan="7">显影</td>
            </tr>
            <tr>
                <td>烘箱实际</td>
                <td>滚轮实际压力</td>
                <td>上轮实际</td>
                <td>下轮实际</td>
                <td>贴膜实际速度</td>
                <td rowspan="6">操作人员</td>
                <td rowspan="6">作业时间</td>
                <td>光强度值</td>
                <td>传送实际速度</td>
                <td>显影液实际</td>
                <td>热水洗实际</td>
                <td>热风实际</td>
                <td rowspan="6">操作人员</td>
                <td rowspan="6">作业时间</td>
                <td rowspan="6">备注</td>
            </tr>
            <tr>
                <td>温度(℃)</td>
                <td>（kg/cm2）</td>
                <td>温度(℃)</td>
                <td>温度(℃)</td>
                <td>(m/min)</td>
                <td>(mj)</td>
                <td>（m/min)</td>
                <td>温度(℃)</td>
                <td>温度(℃)</td>
                <td>温度(℃)</td>
            </tr>
            <tr>
                <td rowspan="4">85±5</td>
                <td rowspan="4">3.5～4.5</td>
                <td rowspan="4">115±5</td>
                <td rowspan="4">115±5</td>
                <td>2.5±0.5</td>
                <td rowspan="4">40±20</td>
                <td>1.3±0.2</td>
                <td rowspan="4">30±5</td>
                <td rowspan="4">30±5</td>
                <td rowspan="4">75±7</td>
            </tr>
            <tr>
                <td>(138*190)</td>
                <td>（双面板）</td>
            </tr>
            <tr>
                <td>2.0±0.5</td>
                <td>1.6±0.2</td>
            </tr>
            <tr>
                <td>（小于138*190）</td>
                <td>（单面板）</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${params.woId}</td>
                <td>${params.partDesc}</td>
                <td>${params.lotCount}</td>
                <td>${params.inQty}</td>
                <td>${params.outQty}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>

            </tr>
        </tbody>
    </table>
</div>
<g:render template="../template/pagination"/>
