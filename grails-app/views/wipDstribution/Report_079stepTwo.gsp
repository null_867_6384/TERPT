<%--
  Created by IntelliJ IDEA.
  User: LX
  Date: 2017/6/8
  Time: 15:50
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="width:100%;height:100%;" ></div>
<script  src="${request.getContextPath()}/js/echarts/echarts.js"></script>
<script  src="${request.getContextPath()}/js/echarts/echarts-tool.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var contextPath = "${request.getContextPath()}";
    eChartsTool.init(contextPath);
    var data=[];
    <g:each in="${Report_079img}" status="i" var="tableInstance">
    data.push('${tableInstance.STAGE_DESC}');
    </g:each>

    // 指定图表的配置项和数据
    var option = {
        title: {
            text: '各组在制品分布统计图'
        },

        tooltip: {},
        legend: {
            data:['在制数量','等待数量','运行数量','暂停数量']
        },
        smooth:false,

        xAxis: {
            data:data
        },
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name: '在制数量',
                type: 'bar',
                data:${Report_079img.WIP_QTY}
            },
            {
                name:'等待数量',
                type:'line',
                data:${Report_079img.WAIT_QTY}
            },{
                name:'运行数量',
                type:'line',
                data:${Report_079img.RUN_QTY}
            },{
                name:'暂停数量',
                type:'line',
                data:${Report_079img.HOLD_QTY}

            }]
    };
    // 使用刚指定的配置项和数据显示图表。
    eChartsTool.setOption("dark", document.getElementById('main'), option);
</script>
</body>
</html>