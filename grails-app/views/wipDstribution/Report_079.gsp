
<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/WipDstribution/Report_079"
          method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="Report_079"/>
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="excel"/>
            <label>组别名称：</label>
            <input type="text" name="stageId" value="${stageId}"/>
            <a href="${request.getContextPath()}/WipDstribution/Report_079stepTwo" type="button" data-toggle="navtab"
               data-id="dialog-normal" data-options="{id:'table79img', title:'分组统计'}">分组统计</a>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <a type="button" class="btn btn-green"
               href="${request.getContextPath()}/WipDstribution/Report_079?export=xls"
               data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="sign-out">导出</a>
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">
    <g:render template="excel"></g:render>
</div>
<g:render template="../template/pagination"/>
