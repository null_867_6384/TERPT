
<g:if test="${dataList.size() > 0}">

    <meta charset="utf-8">
    <style type="text/css">
    th{
        width: 9em;
    }
    tr,td{
        border: 1px #ADBFCD solid;
    }
    .odd{
        background-color: #EBEBEB;
    }

</style>

    <table class="table table-bordered table-hover table-striped table-top" data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <g:each in="${dataList}" status="i" var="tableData">
            <g:if test="${i==0}">
                <thead>
                <tr>
                    <g:each in="${tableData.keySet()}" status="j" var="data">
                    <g:if test="${data!='RN'&&data!='FORM_RRN'&&data!='SYSTIME'&&data!='MARK'&&data!='UUID'}">
                        <th><g:message code="${data}" default="${data}"/></th>
                    </g:if>
                    </g:each>
                </tr>
                </thead>
                <tbody>
            </g:if>
            <tr>
                <g:each in="${tableData.keySet()}" status="j" var="data">
                    <td>
                        <g:if test="${tableData[data]==''}">
                            0.00
                        </g:if>
                        <g:else>
                            <g:if test="${data!='RN'&&data!='FORM_RRN'&&data!='SYSTIME'&&data!='MARK'&&data!='UUID'}">
                            ${tableData[data]}
                            </g:if>
                        </g:else>
                    </td>
                </g:each>
            </tr>
        </g:each>
    </tbody>
    </table>
</g:if>
