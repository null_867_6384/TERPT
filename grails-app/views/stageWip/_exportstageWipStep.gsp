<style type="text/css">
th{
    width: 9em;
}

/*.xl69

{mso-style-parent:style0;
    mso-number-format:"yyyy\\-mm";
    white-space:normal;
    font-size:10.0pt;
    font-family:Arial Unicode MS;
    mso-font-charset:134;
    border:1.0pt solid #ADBFCD;}
*/

td{
    border: 1px #ADBFCD solid;
}
.odd{
    background-color: #EBEBEB;
}
</style>

<div class="bjui-pageContent tableContent">

    <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
        <thead>
        <tr id="t1">
            <g:each in="${tableHeadList}" status="j" var="headInstance">
                <g:if test="${headInstance!='RN'}">
                    <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                </g:if>
            </g:each>
        </tr>
        </thead>
        <tbody>
        <g:if test="${stageWipStep.size()>0}">
            <g:each in="${stageWipStep}" status="i" var="dataInstance">
                <tr>
                    <g:each in="${tableHeadList}" status="j" var="headInstance">
                        <g:if test="${headInstance!='RN'}">
                            <td>${dataInstance["${headInstance}"]}</td>
                        </g:if>
                    </g:each>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <g:render template="../template/emptyPanel" />
        </g:else>
        </tbody>
    </table>
</div>
