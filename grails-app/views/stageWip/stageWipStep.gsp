<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/StageWip/stageWipStep" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="stageWipStep" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <input type="hidden" name="stageDesc" value="${params.stageDesc}"/>
            <input type="hidden" name="lotId" value="${params.lotId}"/>
            <input type="hidden" name="stepDesc" value="${params.stepDesc}"/>
            <input type="hidden" name="partDesc" value="${params.partDesc}"/>
            <label >批号：</label>&nbsp;
        <g:select name="lotid" from="${stageWipSteplotList}" data-toggle="selectpicker" data-live-search="true" optionKey="lot_id" optionValue="lot_id" value="${lotid}" noSelection="['':'ALL']" />&nbsp;
            <label >工序描述：</label>&nbsp;
        <g:select name="stepdesc" from="${stageWipStepstepList}" data-toggle="selectpicker" data-live-search="true" optionKey="step_desc" optionValue="step_desc" value="${stepdesc}" noSelection="['':'ALL']" />&nbsp;
            <label >产品名称：</label>&nbsp;
        <g:select name="partdesc" from="${stageWipSteppartList}" data-toggle="selectpicker" data-live-search="true" optionKey="part_desc" optionValue="part_desc" value="${partdesc}" noSelection="['':'ALL']" />&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>

<div class="panel-body">
    <div style="mini-width:400px;height:450px;" id="cellGlassOutputDiv"></div>
</div>

<div class="bjui-pageContent">
    <g:if test="${stageWipStep.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${stageWipStep.size()>0}">
                <g:each in="${stageWipStep}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                            <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>
                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />