

<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/StageWip/stageWip" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="stageWip" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label>工序：</label>
            <g:select name="step" from="${stageIdList}" data-toggle="selectpicker" data-live-search="true" optionKey="step_name" optionValue="step_name" value="${step}" noSelection="['':'ALL']" />&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>

<div class="bjui-pageContent tableContent">


        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">

            <thead>
            <tr>
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:each in="${stageWip}" status="i" var="dataInstance">
                <tr>
                    <g:each in="${tableHeadList}" status="j" var="headInstance">
                      <g:if test="${headInstance=='STEP'}">
                          <td><a href="${request.getContextPath()}/StageWip/stageWipStep?SYS_QUERY_NAME=stageWipStep&stageDesc=<%=dataInstance.STEP%>&lotId=<%=dataInstance.LOT_ID%>&partName=<%=dataInstance.SAVEPARTCODE%>" data-toggle="navtab"  data-id="dialog-normal" data-options="{id:'stageWipStep', title:'工序明细'}">${dataInstance["${headInstance}"] }</a></td>
                      </g:if>
                    <g:elseif test="${headInstance!='RN'}">
                        <td>${dataInstance["${headInstance}"] }</td>
                    </g:elseif>
                    </g:each>
                </tr>
            </g:each>
            </tbody>
        </table>

</div>
<g:render template="../template/pagination" />