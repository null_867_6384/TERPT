<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/reportByDay/reportByDay" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="reportByDay" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label>时间段：</label>
            <input type="text" name="startTime" readonly="readonly" value="${startTime}"
                   data-pattern="yyyy-MM-dd" data-toggle="datepicker" data-rule="required"
                   placeholder="FROM">&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>

<div class="bjui-pageContent">
    <g:if test="${reportByDay.size() > 0}">


    %{----}%
        <div>
        <div style="margin: 20px;">
            <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
                <thead>
                <tr id="t1">
                    <g:each in="${tableHeadList}" status="j" var="headInstance">
                        <g:if test="${headInstance!='RN'}">
                            <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                        </g:if>

                    </g:each>
                %{-- <th width="125">设备异常比率累积</th>--}%
                </tr>
                </thead>
                <tbody>
                <g:if test="${reportByDay.size()>0}">
                    <g:each in="${reportByDay}" status="i" var="dataInstance">
                        <tr>
                            <g:each in="${tableHeadList}" status="j" var="headInstance">
                                <g:if test="${headInstance!='RN'}">
                                    <td>
                                        <g:if test="${dataInstance["${headInstance}"]=='0'||dataInstance["${headInstance}"]=='0.0%'||dataInstance["${headInstance}"]=='%'||dataInstance["${headInstance}"]=='0.0%'||dataInstance["${headInstance}"]=='0%'}">

                                        </g:if>
                                        <g:elseif test="${headInstance=='良品率'&&dataInstance["${headInstance}"]!=null}">
                                            ${dataInstance["${headInstance}"]}%
                                        </g:elseif>
                                        <g:else>
                                            ${dataInstance["${headInstance}"]}
                                        </g:else>
                                    </td>
                                </g:if>
                            </g:each>
                        %{-- <td>${list[i]}%</td>--}%
                        </tr>
                    </g:each>

                </g:if>
                <g:else>
                    <g:render template="../template/emptyPanel" />
                </g:else>
                </tbody>
            </table>

        </div>
        </div>

        <div class="row" style="padding: 0 8px;">
            <div class="col-md-12" style="padding: 0px;margin: 0px">
                <div class="panel panel-default" >
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-line-chart fa-fw"></i>

                            DCB品质日报


                        </h3>
                    </div>
                    <div class="panel-body">
                        <div id="reportByDay" style="height:420px;" ></div>
                    </div>
                </div>
            </div>


        </div>















        <script  src="${request.getContextPath()}/js/echarts/echarts.js"></script>
        <script  src="${request.getContextPath()}/js/echarts/echarts-tool.js"></script>
        <script type="text/javascript">
            // 基于准备好的dom，初始化echarts实例
            var contextPath = "${request.getContextPath()}";
            eChartsTool.init(contextPath);
            var data=[]
            <g:each in="${reportByDayPicX}" var="x">
            data.push('${x.DEFECT_CODE}')
            </g:each>
            // 指定图表的配置项和数据
            var option = {
                /*backgroundColor: 'rgba(0,0,0,0)', // 工具箱背景颜色*/
                // 图表标题
                title: {

                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                tooltip : {
                    trigger: ''
                },

                legend: {
                    data:['模块','基板PM']

                },
                xAxis: {
                    data:data
                },
                yAxis : [

                    {
                        type : 'value',
                        name:'不良占比',

                        axisLabel : {
                            formatter: '{value}%'
                        },
                        show:true
                    }
                ],

                series : [
                        <g:each in="${reportByDayPic}" var="pic">
                    {
                        name: '${pic.DESCRIPTION}',
                        type: 'bar',
                        data:[${pic.DEFECT_YIELD}]
                    },
                    </g:each>
                    ]
            };
            // 使用刚指定的配置项和数据显示图表。
            eChartsTool.setOption("macarons", document.getElementById('reportByDay'), option);
        </script>




    </g:if>
</div>

