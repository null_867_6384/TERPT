<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/fmc/eqpHistory" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="eqpHistoryList" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />

            <label>location：</label>
            <g:select name="location" from="${locationList}" data-toggle="selectpicker" data-live-search="true" optionKey="LOCATION" optionValue="LOCATION" value="${location}"  noSelection="['':'--ALL--']"/>&nbsp;
            <label>状态：</label>
            <g:select name="eqpstate" from="${eqpStateList}" data-toggle="selectpicker" data-live-search="true" optionKey="EQPSTATE" optionValue="EQPSTATE" value="${eqpstate}" noSelection="['':'--ALL--']" />&nbsp;

            <label>起始时间<strong><span class="text-danger">*</span></strong>：</label>
            <input type="text" name="startTime" class="form-control"
                   data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   size="18" id="start-date-input" readonly value="${startTime.format('yyyy-MM-dd HH:mm:ss')}">&nbsp;
            <label>结束时间<strong><span class="text-danger">*</span></strong>：</label>
            <input type="text" name="endTime" class="form-control"
                   data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   size="18" id="end-date-input" readonly value="${endTime.format('yyyy-MM-dd HH:mm:ss')}" >&nbsp;
            <br>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/tv/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>
<div class="bjui-pageContent tableContent">
    <g:if test="${report_011.size() > 0}">
        <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr>
                <th width="80">线体</th>
                <th width="80">段别</th>
                <th width="90">设备ID</th>
                <th width="160">开始时间</th>
                <th width="160">结束时间</th>
                <th width="100">状态</th>
                <th width="60">ResonCode</th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${report_011}" status="i" var="wipTrendInstance">
                <tr>
                    <td>${wipTrendInstance.LINE}</td>
                    <td>${wipTrendInstance.LOCATION}</td>
                    <td>${wipTrendInstance.EQPID}</td>
                    <td>${wipTrendInstance.FROMTIME}</td>
                    <td>${wipTrendInstance.TOTIME}</td>
                    <td>${wipTrendInstance.EQPSTATE}</td>
                    <td>${wipTrendInstance.RESONCODE}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel" />
    </g:else>
</div>
<g:render template="../template/pagination" />