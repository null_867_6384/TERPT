﻿<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/yield/yield" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="yieldyearList" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="yield" />
            <label>产品类型：</label>
            <g:select name="productid" from="${productList}" data-toggle="selectpicker" data-live-search="true" optionKey="part_type" optionValue="part_type" value="${productid}"  />&nbsp;

            <label>年份：</label>

            <input type="text" name="year" value="${year}">&nbsp;


            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/yield/exportYield')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>
<g:if test="${dataListNo.size()>0}">


<div class="bjui-pageContent">


    <div style="margin:15px auto 0; width:96%;">
        <div class="row" style="padding: 0 8px;">
            <div class="col-md-12" id="glassOutReportTable">
                <g:if test="${dataListNo.size()>0}">
                    <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
                        <thead>
                        <tr>
                            <th width="110" align="center">产品类型</th>
                            <th width="50" align="center">名称</th>
                            <th width="50" align="center">1月</th>
                            <th width="50" align="center">2月</th>
                            <th width="50" align="center">3月</th>
                            <th width="50" align="center">4月</th>
                            <th width="50" align="center">5月</th>
                            <th width="50" align="center">6月</th>
                            <th width="50" align="center">7月</th>
                            <th width="50" align="center">8月</th>
                            <th width="50" align="center">9月</th>
                            <th width="50" align="center">10月</th>
                            <th width="50" align="center">11月</th>
                            <th width="50" align="center">12月</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${dataListNo}" status="i" var="dataInstance">
                            <tr>
                                <td>${dataInstance.PART_TYPE}</td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='in_qty'}">
                                        检查总数
                                    </g:if>
                                    <g:elseif test="${dataInstance.PARAM_NAME=='out_qty'}">
                                        良品数
                                    </g:elseif>
                                    <g:elseif test="${dataInstance.PARAM_NAME=='yield'}">
                                        良品率
                                    </g:elseif>
                                    <g:else>
                                        良品率目标
                                    </g:else>
                                   </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.JAN!=null}">
                                        ${dataInstance.JAN}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.JAN}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.FEB!=null}">
                                        ${dataInstance.FEB}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.FEB}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.MAR!=null}">
                                        ${dataInstance.MAR}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.MAR}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.APR!=null}">
                                        ${dataInstance.APR}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.APR}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.MAY!=null}">
                                        ${dataInstance.MAY}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.MAY}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.JUN!=null}">
                                        ${dataInstance.JUN}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.JUN}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.JUL!=null}">
                                        ${dataInstance.JUL}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.JUL}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.AUG!=null}">
                                        ${dataInstance.AUG}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.AUG}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.SEP!=null}">
                                        ${dataInstance.SEP}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.SEP}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.OCT!=null}">
                                        ${dataInstance.OCT}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.OCT}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.NOV!=null}">
                                        ${dataInstance.NOV}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.NOV}
                                    </g:else>
                                </td>
                                <td>
                                    <g:if test="${dataInstance.PARAM_NAME=='yield'&&dataInstance.DEC!=null}">
                                        ${dataInstance.DEC}%
                                    </g:if>
                                    <g:else>
                                        ${dataInstance.DEC}
                                    </g:else>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
            </div>
        </div>
    </div>
    <div style="margin:15px auto 0; width:96%;">
        <div class="row" style="padding: 0 8px;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-line-chart fa-fw"></i>良率图表</h3>
                    </div>
                    <div class="panel-body">
                        <div style="mini-width:400px;height:350px;" id="cellGlassOutputDiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</g:if>
<g:render template="../template/pagination" />

<script src="${resource(dir: 'js/echarts', file: 'echarts.js')}" type="text/javascript"/>
<script src="${resource(dir: 'js/echarts', file: 'echarts-tool.js')}" type="text/javascript"/>

<g:if test="${dataListNo.size()>0}">
<script type="text/javascript">
    $(function () {
        var contextPath = "${request.getContextPath()}";
        eChartsTool.init(contextPath);
        var theme = 'shine';
        var stackText = 'Group';
        var rotateValue = 60;
        var typeData = ['bar'];
        var theme = 'shine';
        var stackText = '个数';

        var legendData = ['良品数','良率','目标']

        var unitData = [''];
        <g:each status="i" in="${legendDataList}" var="it">

        <g:if test="${i >0 }">
        typeData.push('line');
        unitData.push('%')
        </g:if>
        </g:each>

        var xAxisData = [];
        <g:each in="${dayList}">
        xAxisData.push('${it}');
        </g:each>

        var seriesData = ${seriesData?:"[]"};

        eChartsTool.init(contextPath);
        var monthOption = eChartsTool.initTwoyAxisOption(legendData, xAxisData, seriesData, typeData, unitData);
        var montChart = document.getElementById("cellGlassOutputDiv");
        eChartsTool.setOption(theme, montChart, monthOption);
/*
        $.CurrentNavtab.find('#start-date-input,#end-date-input').datetimepicker({
            format:'yyyy-mm-dd hh',
            autoclose: true,
            todayBtn: true,
            startView:1,
            minView:1,
            pickerPosition: "bottom-left",
            language:'zh-CN',
            startDate:'2016-01-01'
        });

        $.CurrentNavtab.find('.selectpicker').selectpicker();
		*/
    })
</script>
</g:if>