﻿<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/tv/report_011" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="report_011" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label>模组SN：</label>
            <input name="lotid" value="${lotid}"/>
            <label>三联单号：</label>
            <input name="lota" value="${lota}"/>
            <label>工单号：</label>
            <input name="woid" value="${woid}"/>
            <label>机种：</label>
            <input name="partname" value="${partname}"/>
            <label>线别：</label>
            <g:select name="lineid" from="${lineList}" data-toggle="selectpicker" data-live-search="true" optionKey="LINEID" optionValue="LINEDESC" value="${lineid}" noSelection="['':'--ALL--']" />&nbsp;
            <label>站点：</label>
            <g:select name="stepid" from="${stepList}" data-toggle="selectpicker" data-live-search="true" optionKey="Step_Id" optionValue="Step_DESC" value="${stepid}"  noSelection="['':'--ALL--']"/>&nbsp;
            <label>EQPID：</label>
            <input name="eqpid" value="${eqpid}"/>

            <label>起始时间<strong><span class="text-danger">*</span></strong>：</label>
            <input type="text" name="startTime" class="form-control"
                   data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   size="18" id="start-date-input" readonly value="${startTime.format('yyyy-MM-dd HH:mm:ss')}">&nbsp;
            <label>结束时间<strong><span class="text-danger">*</span></strong>：</label>
            <input type="text" name="endTime" class="form-control"
                   data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss"
                   size="18" id="end-date-input" readonly value="${endTime.format('yyyy-MM-dd HH:mm:ss')}" >&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/tv/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>
<div class="bjui-pageContent tableContent">
    <g:if test="${report_011.size() > 0}">
        <table data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr>
                <th width="110">模组SN</th>
                <th width="110">三联单号</th>
                <th width="90">工单号</th>
                <th width="60">线体</th>
                <th width="110">机种料号</th>
                <th width="100">机种名称</th>
                <th width="60">判定结果</th>
                <th width="60">站点ID</th>
                <th width="100">站点名称</th>
                <th width="60">EQPID</th>
                <th width="150">时间</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${report_011}" status="i" var="wipTrendInstance">
                <tr>
                    <td>${wipTrendInstance.LOT_ID}</td>
                    <td>${wipTrendInstance.LOT_ALIAS}</td>
                    <td>${wipTrendInstance.WO_ID}</td>
                    <td>${wipTrendInstance.LINE_ID}</td>
                    <td>${wipTrendInstance.PART_NAME}</td>
                    <td>${wipTrendInstance.PART_DESC}</td>
                    <td>${wipTrendInstance.JUDGE1}</td>
                    <td>${wipTrendInstance.STEP_NAME}</td>
                    <td>${wipTrendInstance.STEP_DESC}</td>
                    <td>${wipTrendInstance.EQUIPMENT_ID}</td>
                    <td>${wipTrendInstance.TRANS_TIME}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </g:if>
    <g:else>
        <g:render template="../template/emptyPanel" />
    </g:else>
</div>
<g:render template="../template/pagination" />