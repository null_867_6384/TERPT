<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/wipSelect/wipSelect" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="wipSelect" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <label >生产订单号：</label>&nbsp;
            <input type="text" name="docId" value="${docId}"/>
            <label >客户：</label>&nbsp;
            <input type="text" name="custName" value="${custName}"/>
            <label >批次号：</label>&nbsp;
            <input type="text" name="lotId" value="${lotId}"/>

            <label >产品名：</label>&nbsp;
            <input type="text" name="partDesc" value="${partDesc}"/>
            <label >产品规格：</label>&nbsp;
            <input type="text" name="guige" value="${guige}"/><br/>

            <label >交货日期：</label>&nbsp;
            <input type="text" name="startJhrq" data-pattern="yyyy-MM-dd" value="${startJhrq}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endJhrq" data-pattern="yyyy-MM-dd" value="${endJhrq}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >计划开工日期：</label>&nbsp;
            <input type="text" name="startJhkgrq" data-pattern="yyyy-MM-dd" value="${startJhkgrq}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endJhkgrq" data-pattern="yyyy-MM-dd" value="${endJhkgrq}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;<br/>

            <label >批次产生日期：</label>&nbsp;
            <input type="text" name="startLotsc" data-pattern="yyyy-MM-dd" value="${startLotsc}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endLotsc" data-pattern="yyyy-MM-dd" value="${endLotsc}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >批次投产日期：</label>&nbsp;
            <input type="text" name="startLottc" data-pattern="yyyy-MM-dd" value="${startLottc}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endLottc" data-pattern="yyyy-MM-dd" value="${endLottc}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;<br/>
            <label >计划完工日期：</label>&nbsp;
            <input type="text" name="startJhwg" data-pattern="yyyy-MM-dd" value="${startJhwg}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endJhwg" data-pattern="yyyy-MM-dd" value="${endJhwg}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >工步：</label>&nbsp;
            <input type="text" name="stepName" value="${stepName}"/>
            <label >产品类别：</label>&nbsp;
            <input type="text" name="partType" value="${partType}"/>
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${wipSelect.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${wipSelect.size()>0}">
                <g:each in="${wipSelect}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>

                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />