<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/CheckerOutputByDay/checkerOutputByDay" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="checkerOutputByDay" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />

            %{--<label >批号：</label>&nbsp;
        <g:select name="lotId" from="${detailOrderLotList}" data-toggle="selectpicker" data-live-search="true" optionKey="lot_id" optionValue="lot_id" value="${lotId}" noSelection="['':'ALL']" />&nbsp;--}%

            <label >检查日期：</label>&nbsp;
            <input type="text" name="startTime" value="${startTime}"  data-toggle="datepicker" placeholder="FROM">
            <input type="text" name="endTime" value="${endTime}"  data-toggle="datepicker" placeholder="TO">&nbsp;&nbsp;
            <label >检查员：</label>&nbsp;
            <g:select name="checker" from="${checkerOutputByDayChecker}" data-atoggle="selectpicker" data-live-search="true" optionKey="description" optionValue="description" value="${checker}" noSelection="['':'ALL']" />&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${checkerOutputByDay.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'&&headInstance!='OPERATOR2'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${checkerOutputByDay.size()>0}">
                <g:each in="${checkerOutputByDay}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'&&headInstance!='产出数'&&headInstance!='OPERATOR2'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>
                            <g:elseif test="${headInstance=='产出数'}">
                                <td><a href="${request.getContextPath()}/CheckerOutputByDay/CheckerOutputByDayStep?checker1=<%=dataInstance.OPERATOR2%>&startTime=<%=params.startTime%>&endTime=<%=params.endTime%>" data-toggle="navtab"  data-id="dialog-normal" data-options="{id:'checkerOutputByDayStep', title:'检验员每日产量表二阶'}">${dataInstance["${headInstance}"] }</a></td>
                            </g:elseif>
                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />