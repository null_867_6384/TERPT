<div class="bjui-pageHeader">
    <form id="pagerForm" data-toggle="ajaxsearch" action="${request.getContextPath()}/CheckerOutputByDay/checkerOutputByDayStep" method="post">
        <div class="bjui-searchBar">
            <g:hiddenField name="SYS_QUERY_NAME" value="checkerOutputByDayStep" />
            <g:hiddenField name="EXPORT_CONTENT_NAME" value="Excel" />
            <g:hiddenField name="startTime" value="${params.startTime}" />
            <g:hiddenField name="endTime" value="${params.endTime}" />
            <g:hiddenField name="checker1" value="${params.checker1}" />
            <g:hiddenField name="checker2" value="${params.checker2}" />
            <label >检查员：</label>&nbsp;
        <g:select name="checker2" from="${checkerOutputByDayCheckerStep}" data-atoggle="selectpicker" data-live-search="true" optionKey="operator2" optionValue="operator2" value="${checker2}" noSelection="['':'ALL']" />&nbsp;
            <button type="submit" class="btn-default" data-icon="search">查询</button>&nbsp;
            <button type="button" class="btn-blue" onclick="javascript:bjuiExportExl('#pagerForm','${request.getContextPath()}/StageWip/exportExcel')" data-icon="file-excel-o" title="导出Excel">导出</button>&nbsp;
        </div>
    </form>
</div>


<div class="bjui-pageContent">
    <g:if test="${checkerOutputByDayStep.size() > 0}">
        <table  data-toggle="tablefixed" data-width="100%" data-nowrap="true">
            <thead>
            <tr id="t1">
                <g:each in="${tableHeadList}" status="j" var="headInstance">
                    <g:if test="${headInstance!='RN'}">
                        <th  width="125"><g:message code="${headInstance}" default="${headInstance}"/></th>
                    </g:if>
                </g:each>
            </tr>
            </thead>
            <tbody>
            <g:if test="${checkerOutputByDayStep.size()>0}">
                <g:each in="${checkerOutputByDayStep}" status="i" var="dataInstance">
                    <tr>
                        <g:each in="${tableHeadList}" status="j" var="headInstance">
                            <g:if test="${headInstance!='RN'}">
                                <td>${dataInstance["${headInstance}"]}</td>
                            </g:if>

                        </g:each>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <g:render template="../template/emptyPanel" />
            </g:else>
            </tbody>
        </table>
    </g:if>
</div>
<g:render template="../template/pagination" />