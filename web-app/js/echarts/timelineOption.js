var DataMap = function () {
    var dataMap = {};
    function dataFormatter(obj) {
        var pList = [" "," "," "," "," "," "," "," "," "," "];
        var temp;
        var max = 0;
        for (var year = 2002; year <= 2011; year++) {
            temp = obj[year];
            for (var i = 0, l = temp.length; i < l; i++) {
                max = Math.max(max, temp[i]);
                obj[year][i] = {
                    name : pList[i],
                    value : temp[i]
                }
            }
            obj[year+'max'] = Math.floor(max/100) * 100;
        }
        return obj;
    }

    function dataMix(list) {
        var mixData = {};
        for (var i = 0, l = list.length; i < l; i++) {
            for (var key in list[i]) {
                if (list[i][key] instanceof Array) {
                    mixData[key] = mixData[key] || [];
                    for (var j = 0, k = list[i][key].length; j < k; j++) {
                        mixData[key][j] = mixData[key][j]
                        || {name : list[i][key][j].name, value : []};
                        mixData[key][j].value.push(list[i][key][j].value);
                    }
                }
            }
        }
        return mixData;
    }

    dataMap.dataGDP = dataFormatter({
        //max : 60000,
        2011:[251,113,451,237,159,222,568,582,110,318],
        2010:[113,224,294,200,172,187,667,368,165,414],
        2009:[153,521,235,358,340,212,278,587,146,344],
        2008:[115,619,161,315,496,366,426,314,406,309],
        2007:[246,252,137,224,423,164,284,104,244,208],
        2006:[117,462,117,478,444,304,425,211,105,215],
        2005:[169,305,101,420,305,347,320,513,324,198],
        2004:[333,110,847,371,341,272,322,450,873,103],
        2003:[307,578,621,255,238,102,262,457,694,442],
        2002:[415,210,618,234,140,548,234,363,541,106]
    });

    dataMap.dataEstate = dataFormatter({
        2011:[7.21,6.46,8.02,4.91,4.76,6.12,8.61,2.11,4.68,1.89],
        2010:[6.52,7.59,7.79,2.25,4.25,3.37,2.32,1.89,2.50,0.02],
        2009:[2.47,8.73,2.41,3.31,6.65,5.27,0.14,1.18,7.56,5.39],
        2008:[4.59,7.88,3.81,6.04,3.31,0.81,2.71,4.47,3.34,6.13],
        2007:[1.15,3.44,7.97,4.12,1.01,0.43,3.03,5.81,5.06,5.71],
        2006:[8.23,6.64,7.14,1.01,6.51,8.54,1.01,4.79,3.61,7.91],
        2005:[3.73,2.67,0.87,6.11,8.75,6.77,2.29,3.34,5.97,3.73],
        2004:[6.11,6.14,1.08,5.01,3.81,7.13,1.74,1.06,3.11,4.17],
        2003:[1.88,2.31,5.19,7.73,1.05,1.49,1.99,1.12,7.82,4.47],
        2002:[8.02,3.04,0.89,5.83,1.48,0.94,7.11,1.07,4.86,1.09]
    });

    dataMap.dataGDP_Estate = dataMix([dataMap.dataEstate, dataMap.dataGDP]);

    return {
        init: function () {
            return dataMap;
        }
    };
}();

