var eChartsTool = function () {
    var markPoint = {
        "data": [
            {
                "type": "max",
                "name": "最大值"
            },
            {
                "type": "min",
                "name": "最小值"
            }
        ]
    };

    var markLine = {
        "data": [
            {
                "type": "average",
                "name": "平均值"
            }
        ]
    };

    return {
        init: function (contextPath) {
            require.config({
                paths: {
                    'echarts': contextPath + "/js/echarts/"
                }
            });
        },

        initPartsReportOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':70,'y2':75},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value"
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData[i]
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },

        initAdverseTrendAnalysisOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':40,'y2':75},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value",
                        axisLabel : {
                            formatter: '{value}%'
                        }
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData[i]
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },

        initDeliveReportOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':40,'y2':75},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value"
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },
        initDuiJiBarOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':40,'y2':100},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value"
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData[i],
                            //"markPoint": markPoint,
                            "markLine": markLine,
                            "itemStyle":{
                                "normal":{label:{show:true,position:'top'}}
                        }
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },
        initVerticalBarOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':40,'y2':75},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value"
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData[i]
                            //"markPoint": markPoint,
                            //"markLine": markLine,
                        //"itemStyle":{
                        //    "normal":{label:{show:true,position:'inside'}}
                        //}
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },

        initPFQCyieldReportOption: function (legendData, xAxisDayData, seriesDayData, type) {
            return this.barLineChart(legendData, xAxisDayData, seriesDayData, type,true,'Yield','{value}个','{value}%');            
        },

        barLineChart: function (legendData, xAxisDayData, seriesDayData, type,isStack,rightY,y1Format,y2Format) {
            if(y1Format == null || y1Format == '')
                y1Format = "{value}"
            if(y2Format == null || y2Format == '')
                y2Format = "{value}"
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                calculable: true,
                grid : {'y':50,'y2':75},
                xAxis: [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisDayData
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel : {
                            formatter: y1Format
                        }
                    },
                    {
                        type: 'value',
                        axisLabel : {
                            formatter: y2Format
                        }
                    }
                ],
                series: function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": type[i],
                            "data": seriesDayData[i]
                        };

                        if(legendData[i] == rightY){
                            item['yAxisIndex'] = 1
                        }else{
                            if(isStack){
                                item['stack'] = '缺陷';
                            }
                            item['itemStyle']={normal:{label:{show:true,position:'top'}}};
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },

        barLineChart1: function (legendData, xAxisDayData, seriesDayData, type,isStack,rightY,rightY1,y1Format,y2Format) {
            if(y1Format == null || y1Format == '')
                y1Format = "{value}"
            if(y2Format == null || y2Format == '')
                y2Format = "{value}"
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                calculable: true,
                grid : {'y':50,'y2':75},
                xAxis: [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisDayData
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel : {
                            formatter: y1Format
                        }
                    },
                    {
                        type: 'value',
                        axisLabel : {
                            formatter: y2Format
                        }
                    }
                ],
                series: function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": type[i],
                            "data": seriesDayData[i]
                        };
                        if(legendData[i] == rightY || legendData[i] == rightY1){
                            item['yAxisIndex'] = 1
                        }else{
                            if(isStack)
                                item['stack'] = '缺陷'
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },



        initMoveTrend3dOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                calculable: true,
                grid : {'y':40,'y2':75},
                xAxis: [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                yAxis: [
                    {
                        "type": 'value'
                    }
                ],
                series: function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype,
                            "stack": stackText,
                            "data": seriesData[i],
                            "itemStyle":{
                                "normal":{label:{show:true,position:'top'}}
                            }
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },

        initPanelReportOption: function (legendData, xAxisData, seriesData, stype, stackText) {
            return {
                "tooltip": {
                    "trigger": "axis",
                    "axisPointer": {
                        "type": "shadow"
                    }
                },
                "legend": {
                    "data": legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                "calculable": true,
                "grid" : {'y':40,'y2':75},
                "xAxis": [
                    {
                        "type": "category",
                        "axisLabel":{
                            interval:0,
                            rotate:60,
                            margin:5
                        },
                        "data": xAxisData
                    }
                ],
                "yAxis": [
                    {
                        "type": "value"
                    },
                    {
                        "type": "value"
                    }
                ],
                "series": function () {
                    var serie = [];
                    for (var i = 0; i < legendData.length; i++) {
                        var item = {
                            name: legendData[i],
                            "type": stype[i],
                            "stack": stackText,
                            "data": seriesData[i]
                            //"markPoint": markPoint,
                            //"markLine": markLine,
                            //"itemStyle":{
                            //    "normal":{label:{show:true,position:'inside'}}
                            //}
                        }

                        if(legendData[i]=="投入累计" || legendData[i] == "产出累计"){
                            item['yAxisIndex'] = 1;
                        }

                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },


        initTimeLIneBarOption: function (timeLineData, legendData, xAxisData, seriesData, typeData, unitData) {
            return equipmentOnlineOption = {
                timeline: {
                    data: timeLineData,
                    label: {
                        formatter: function (s) {
                            return s.slice(5, 10);
                        }
                    },
                    autoPlay: false,
                    playInterval: 1000
                },
                options:function () {
                    var serie = [];
                    for (var k = 0; k < xAxisData.length; k++) {
                        var item;
                        if(k==0){
                            item = {
                                title: {
                                    show: false
                                },
                                tooltip: {
                                    trigger: 'axis',
                                    formatter: function (params) {
                                        var temStr ="";
                                        for(var l = 0; l < params.length; l++){
                                            if(l==0){
                                                temStr = temStr+ params[l].name + '<br/>'
                                            }
                                            temStr = temStr + params[l].seriesName + ' : ' + params[l].value + ' ('+unitData[l]+')';
                                            if(l< params.length -1){
                                                temStr += '<br/>';
                                            }
                                        }
                                        return temStr;
                                    }
                                },
                                legend: {
                                    x: 'center',
                                    'data': legendData,
                                    'selected': function () {
                                        var item = {};
                                        for (var i = 0; i < legendData.length; i++) {
                                            item[legendData[i]] = true;
                                        }
                                        return item;
                                    }()
                                },
                                toolbox: {
                                    'show': true,
                                    orient: 'vertical',
                                    x: 'right',
                                    y: 'center',
                                    'feature': {
                                        'mark': {'show': true},
                                        'dataView': {'show': true, 'readOnly': false},
                                        'magicType': {'show': true, 'type': ['line', 'bar', 'stack', 'tiled']},
                                        'restore': {'show': true},
                                        'saveAsImage': {'show': true}
                                    }
                                },
                                calculable: true,
                                grid: {'y': 40, 'y2': 80},
                                xAxis: [{
                                    'type': 'category',
                                    'axisLabel': {'interval': 0},
                                    'data': xAxisData
                                }],
                                yAxis: [
                                    {
                                        'type': 'value',
                                        'name': '次数'
                                    },
                                    {
                                        'type': 'value',
                                        'name': '百分比',
                                        'min': 0
                                    }
                                ],
                                series:
                                    function () {
                                        var tempSerie = [];
                                        for (var i = 0; i < seriesData.length; i++) {
                                            var tempItem = {
                                                name: legendData[i],
                                                "type": typeData[i],
                                                //"itemStyle":itemStyle,
                                                "itemStyle":function(){
                                                    var styleItem = {
                                                        normal: {
                                                            label: {
                                                                show: true,
                                                                position: 'top',
                                                                formatter: '{c}'+unitData[i]
                                                            }
                                                        }
                                                    };
                                                    return styleItem;
                                                }(),
                                                "data": seriesData[i][0]
                                            }
                                            if(i>0){
                                                tempItem['yAxisIndex']=i;
                                            }
                                            tempSerie.push(tempItem);
                                        }
                                        return tempSerie;
                                    }()
                            }
                        }else{
                            item = {
                                title: {show: false},
                                "series": function () {
                                    var temSerie = [];
                                    for (var i = 0; i < seriesData.length; i++) {
                                        var tmpItem = {
                                            "data": seriesData[i][k]
                                        }
                                        temSerie.push(tmpItem);
                                    }
                                    return temSerie;
                                }()
                            }
                        }
                        serie.push(item);
                    }
                    return serie;
                }()
            }
        },
        initTwoyAxisOption: function (legendData, xAxisData, seriesData, typeData, unitData) {
            return {
                tooltip : {'trigger':'axis'},
                legend : {
                    x:'center',
                    'data':legendData,
                    'selected':function () {
                        var item = {};
                        for (var i = 0; i < legendData.length; i++) {
                            item[legendData[i]] = true;
                        }
                        return item;
                    }()
                },
                toolbox : {
                    'show':true,
                    orient : 'vertical',
                    x: 'right',
                    y: 'center',
                    'feature':{
                        'mark':{'show':true},
                        'dataView':{'show':true,'readOnly':false},
                        'magicType':{'show':true,'type':['line','bar','stack','tiled']},
                        'restore':{'show':true},
                        'saveAsImage':{'show':true}
                    }
                },
                calculable : true,
                grid : {'y':40,'y2':60},
                xAxis : [{
                    'type':'category',
                    'axisLabel':{'interval':0,rotate:30},
                    'data':xAxisData
                }],
                yAxis :
                    function () {
                        var tmpSerie = [];
                        for (var i = 0; i < legendData.length; i++) {
                            var tempItem = {
                                "type": 'value',
                                "name": legendData[i]+'（'+unitData[i]+'）',
                                'min':0
                            }
                            tmpSerie.push(tempItem);
                        }
                        return tmpSerie;
                    }(),
                series :
                    function () {
                        var serie = [];
                        for (var i = 0; i < seriesData.length; i++) {
                            var tempItem = {
                                name: legendData[i],
                                "type": typeData[i],
                                "itemStyle":function(){
                                    var styleItem = {
                                        normal: {
                                            label: {
                                                show: true,
                                                position: 'top',
                                                formatter: '{c}'+unitData[i]
                                            }
                                        }
                                    };
                                    return styleItem;
                                }(),
                                "data": seriesData[i]
                            }
                            if(i>0){
                                tempItem['yAxisIndex']=1;
                            }
                            serie.push(tempItem);
                        }
                        return serie;
                    }()
            }
        },
        initTwoyYaxisAndNormalLegendOption: function (legendData, xAxisData, seriesData, typeData, yAxisData,rotateValue) {
            return {
                tooltip : {'trigger':'axis'},
                legend : {
                    'data':legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                calculable : true,
                grid: {'y':40,'y2':80},
                xAxis : [{
                    'type':'category',
                    "axisLabel":{
                        "interval":0,
                        "rotate":rotateValue,
                        "margin":5,
                        "textStyle":{
                            "color":"#222"
                        }
                    },
                    'data':xAxisData
                }],
                yAxis :
                    function () {
                        var tmpSerie = [];
                        for (var i = 0; i < yAxisData.length; i++) {
                            var tempItem = {
                                "type": 'value',
                                "name": yAxisData[i],
                                'min':0
                            }
                            tmpSerie.push(tempItem);
                        }
                        return tmpSerie;
                    }(),
                series :
                    function () {
                        var serie = [];
                        for (var i = 0; i < seriesData.length; i++) {
                            var tempItem = {
                                name: legendData[i],
                                "type": typeData[i],
                                "itemStyle":function(){
                                    var styleItem = {
                                        normal: {
                                            label: {
                                                show: true,
                                                position: 'top',
                                                formatter: '{c}'
                                            }
                                        }
                                    };
                                    return styleItem;
                                }(),
                                "data": seriesData[i]
                            }
                            if(i<2){
                                tempItem['stack']='数量';
                            }
                            if(i>1){
                                tempItem['yAxisIndex']=1;
                            }else{
                                tempItem['stackText']="Group";
                            }
                            serie.push(tempItem);
                        }
                        return serie;
                    }()
            }
        },
        initInlineYieldOption: function (legendData, xAxisData, seriesData, typeData, yAxisData,rotateValue) {

            return {
                tooltip : {'trigger':'axis'},
                legend : {
                    'data':legendData
                },
                "toolbox": {
                    "show": true,
                    "orient": "vertical",
                    "x": "right",
                    "y": "center",
                    "feature": {
                        "mark": {
                            "show": true
                        },
                        "dataView": {
                            "show": true,
                            "readOnly": false
                        },
                        "magicType": {
                            "show": true,
                            "type": [
                                "line",
                                "bar"
                            ]
                        },
                        "restore": {
                            "show": true
                        },
                        "saveAsImage": {
                            "show": true
                        }
                    }
                },
                calculable : false,
                grid: {'y':40,'y2':80},
                xAxis : [{
                    'type':'category',
                    "axisLabel":{
                        "interval":0,
                        "rotate":rotateValue,                       
                        "margin":5,
                        "textStyle":{
                            "color":"#222"
                        }
                    },
                    'data':xAxisData
                }],
                yAxis :
                    function () {
                        var tmpSerie = [];
                        for (var i = 0; i < yAxisData.length; i++) {
                            var tempItem = {
                                "type": 'value',
                                "name": yAxisData[i],
                                'min':0,
                                axisLabel : {
                                    formatter: '{value}'+yAxisData[i]
                                }
                            }
                            tmpSerie.push(tempItem);
                        }
                        return tmpSerie;
                    }(),
                series :
                    function () {
                        var serie = [];
                        for (var i = 0; i < seriesData.length; i++) {
                            var tempItem = {
                                name: legendData[i],
                                "type": typeData[i],
                                "itemStyle":function(){
                                    var styleItem = {
                                        normal: {
                                            label: {
                                                show: true,
                                                position: 'top',
                                                formatter: '{c}'
                                            }
                                        }
                                    };
                                    return styleItem;
                                }(),
                                "data": seriesData[i]
                            }
                            if(i<(legendData.length - 1)){
                                tempItem['stack']='数量';
                            }
                            if(i>(legendData.length - 2)){
                                tempItem['yAxisIndex']=1;
                            }else{
                                tempItem['stackText']="Group";
                            }
                            serie.push(tempItem);
                        }
                        return serie;
                    }()
            }
        },
        setOption: function (stheme, chart, option, fun) {
            require(
                [
                    'echarts',
                    'echarts/theme/' + stheme,
                    'echarts/chart/line',
                    'echarts/chart/pie',
                    'echarts/chart/bar',
                    'echarts/chart/map',
                    "echarts/config"
                ],
                function (ec) {
                    var theme = require('echarts/theme/' + stheme);
                    var myChart = ec.init(chart, theme);
                    myChart.setOption(option);
                    var ecConfig = require('echarts/config');
                    if (fun != null) {
                        myChart.on(ecConfig.EVENT.CLICK, fun);
                        myChart.on(ecConfig.EVENT.HOVER, function (param) {
                            $("canvas[data-zr-dom-id='_zrender_hover_']").css("cursor", "pointer");
                        });
                    }
                }
            );
        }
    };
}();