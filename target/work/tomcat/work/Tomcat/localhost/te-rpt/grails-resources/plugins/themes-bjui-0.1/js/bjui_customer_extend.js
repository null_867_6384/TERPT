    function changeSelectTarget(data,url,$next)
    {
         var nextVal = $next.val();
         $.ajax({
                    type     : 'POST',
                    dataType : 'json', 
                    url      : url,
                    cache    : false,
                    data     : data,
                    success  : function(json) {
                        if (!json) return
                        
                        var html = '', selected = ''
                        
                        $.each(json, function(i) {
                            var value, label
                            
                            if (json[i] && json[i].length) {
                                value = json[i][0]
                                label = json[i][1]
                            } else {
                                value = json[i].value
                                label = json[i].label
                            }
                            if (typeof nextVal != 'undefined') selected = value == nextVal ? ' selected' : ''
                            html += '<option value="'+ value +'"'+ selected +'>' + label + '</option>'
                        })
                        
                        $next.removeAttr('data-val').removeData('val')
                        
                        if (!html) {
                            html = $next.data('emptytxt') || '&nbsp;'
                            html = '<option>'+ html +'</option>'
                        }
                        
                        $next.html(html).selectpicker('refresh')
                    },
                    error   : BJUI.ajaxError
                })
    }