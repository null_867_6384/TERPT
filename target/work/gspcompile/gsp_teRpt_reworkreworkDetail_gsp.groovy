import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_reworkreworkDetail_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/rework/reworkDetail.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
expressionOut.print(request.getContextPath())
printHtmlPart(2)
expressionOut.print(params.reworkCode)
printHtmlPart(3)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(4)
invokeTag('message','g',14,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(5)
j++
}
}
printHtmlPart(6)
loop:{
int i = 0
for( dataInstance in (Report_084stepTwo) ) {
printHtmlPart(7)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(8)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(9)
j++
}
}
printHtmlPart(10)
i++
}
}
printHtmlPart(11)
invokeTag('render','g',26,['template':("../template/pagination")],-1)
printHtmlPart(0)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1510885136104L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
