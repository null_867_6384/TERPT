import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_coaReportcoaReport_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/coaReport/coaReport.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',4,['name':("SYS_QUERY_NAME"),'value':("coaReport")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',5,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
expressionOut.print(lotId)
printHtmlPart(4)
expressionOut.print(request.getContextPath())
printHtmlPart(5)
if(true && (coaReport.size() > 0)) {
printHtmlPart(6)
expressionOut.print(coaReport.PARENT_ID[0])
printHtmlPart(7)
expressionOut.print(coaReport.OPERATOR[0])
printHtmlPart(8)
expressionOut.print(coaReport.SPEC1[0])
printHtmlPart(9)
expressionOut.print(coaReport.DESCRIPTION[0])
printHtmlPart(10)
expressionOut.print(coaReport.MEASURE_TIME[0])
printHtmlPart(11)
expressionOut.print(coaReport.TRACK_IN_MAIN_QTY[0])
printHtmlPart(12)
expressionOut.print(coaReport.size())
printHtmlPart(13)
expressionOut.print(coaReport.MLOT_ID[0])
printHtmlPart(14)
if(true && (coaReport.size()!=1)) {
printHtmlPart(15)
expressionOut.print(coaReport.size()-1)
printHtmlPart(16)
expressionOut.print(coaReport.size()-1)
printHtmlPart(17)
expressionOut.print(coaReport.LOT_ID[0])
printHtmlPart(18)
}
else {
printHtmlPart(19)
expressionOut.print(coaReport.LOT_ID[0])
printHtmlPart(18)
}
printHtmlPart(20)
if(true && (coaReport.size()>=2)) {
printHtmlPart(21)
expressionOut.print(coaReport.MLOT_ID[1])
printHtmlPart(18)
}
printHtmlPart(22)
if(true && (coaReport.size()>=3)) {
printHtmlPart(23)
expressionOut.print(coaReport.MLOT_ID[2])
printHtmlPart(24)
}
printHtmlPart(2)
if(true && (coaReport.size()>=4)) {
printHtmlPart(23)
expressionOut.print(coaReport.MLOT_ID[3])
printHtmlPart(25)
}
printHtmlPart(26)
if(true && (coaReport.size()>=5)) {
printHtmlPart(23)
expressionOut.print(coaReport.MLOT_ID[4])
printHtmlPart(25)
}
printHtmlPart(26)
if(true && (coaReport.size()>=6)) {
printHtmlPart(23)
expressionOut.print(coaReport.MLOT_ID[5])
printHtmlPart(25)
}
printHtmlPart(27)
expressionOut.print(coaReportEdc.size+3)
printHtmlPart(28)
loop:{
int j = 0
for( edc in (coaReportEdc) ) {
printHtmlPart(29)
expressionOut.print(j+1)
printHtmlPart(30)
expressionOut.print(edc.DESCRIPTION)
printHtmlPart(30)
expressionOut.print(edc.EQP_DESC)
printHtmlPart(30)
expressionOut.print(edc.LL)
printHtmlPart(31)
expressionOut.print(edc.DC_DATA)
printHtmlPart(32)
j++
}
}
printHtmlPart(33)
}
else {
printHtmlPart(34)
invokeTag('render','g',139,['template':("../template/emptyPanel")],-1)
printHtmlPart(35)
}
printHtmlPart(36)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1528426982050L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
