import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_inputupdateinput_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/input/updateinput.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
expressionOut.print(input?.id)
printHtmlPart(2)
expressionOut.print(input.partType)
printHtmlPart(3)
expressionOut.print(input?.id)
printHtmlPart(4)
expressionOut.print(input.inputYear)
printHtmlPart(5)
expressionOut.print(input.inputMonth)
printHtmlPart(6)
expressionOut.print(input.inQty)
printHtmlPart(7)
expressionOut.print(input.outQty)
printHtmlPart(8)
expressionOut.print(input.yield)
printHtmlPart(9)
expressionOut.print(input.reserveDouble1)
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511231746629L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
