import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_dwsStatisticsstatisticsReport_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/dwsStatistics/statisticsReport.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',5,['name':("SYS_QUERY_NAME"),'value':("statisticsList")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',6,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
if(true && (edc_from == 'LOT')) {
printHtmlPart(4)
}
printHtmlPart(5)
if(true && (edc_from == 'GENERAL')) {
printHtmlPart(4)
}
printHtmlPart(6)
expressionOut.print(startTime)
printHtmlPart(7)
expressionOut.print(endTime)
printHtmlPart(8)
invokeTag('select','g',23,['name':("department"),'from':(departmentList),'class':("selectpicker"),'data-live-search':("true"),'data-live-search-placeholder':("查找"),'noSelection':(['': '']),'data-width':("200"),'data-toggle':("selectpicker"),'data-size':("12"),'optionKey':("department"),'optionValue':("department"),'value':(department)],-1)
printHtmlPart(9)
invokeTag('select','g',28,['name':("name"),'from':(charidList),'class':("selectpicker"),'data-live-search':("true"),'data-live-search-placeholder':("查找"),'noSelection':(['': '']),'data-width':("200"),'data-toggle':("selectpicker"),'data-size':("12"),'optionKey':("name"),'optionValue':("name"),'value':(name)],-1)
printHtmlPart(10)
expressionOut.print(request.getContextPath())
printHtmlPart(11)
if(true && (result.size() > 0)) {
printHtmlPart(12)
invokeTag('render','g',38,['template':("statisticsExcel")],-1)
printHtmlPart(13)
}
printHtmlPart(14)
invokeTag('render','g',1,['template':("../template/pagination")],-1)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1526024262121L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
