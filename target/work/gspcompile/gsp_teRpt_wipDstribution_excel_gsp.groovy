import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_wipDstribution_excel_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/wipDstribution/_excel.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(1)
if(true && (headInstance != 'RN')) {
printHtmlPart(2)
invokeTag('message','g',7,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(3)
}
printHtmlPart(4)
j++
}
}
printHtmlPart(5)
loop:{
int i = 0
for( dataInstance in (Report_079) ) {
printHtmlPart(6)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(7)
if(true && (headInstance != 'RN')) {
printHtmlPart(8)
if(true && (headInstance == 'WIP_QTY')) {
printHtmlPart(9)
expressionOut.print(request.getContextPath())
printHtmlPart(10)
out.print(dataInstance.STAGE_DESC)
printHtmlPart(11)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(12)
}
else {
printHtmlPart(13)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(14)
}
printHtmlPart(7)
}
printHtmlPart(1)
j++
}
}
printHtmlPart(15)
i++
}
}
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511423828947L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
