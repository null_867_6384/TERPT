import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_coaReportForcoaReportFor_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/coaReportFor/coaReportFor.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',4,['name':("SYS_QUERY_NAME"),'value':("coaReportFor")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',5,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
expressionOut.print(lotId)
printHtmlPart(4)
expressionOut.print(request.getContextPath())
printHtmlPart(5)
if(true && (coaReportFor.size() > 0)) {
printHtmlPart(6)
expressionOut.print(coaReportFor.PARENT_ID[0])
printHtmlPart(7)
expressionOut.print(coaReportFor.OPERATOR[0])
printHtmlPart(8)
expressionOut.print(coaReportFor.SPEC1[0])
printHtmlPart(9)
expressionOut.print(coaReportFor.DESCRIPTION[0])
printHtmlPart(10)
expressionOut.print(coaReportFor.MEASURE_TIME[0])
printHtmlPart(11)
expressionOut.print(coaReportFor.TRACK_IN_MAIN_QTY[0])
printHtmlPart(12)
expressionOut.print(coaReportFor.size())
printHtmlPart(13)
expressionOut.print(coaReportFor.MLOT_ID[0])
printHtmlPart(14)
if(true && (coaReportFor.size()!=1)) {
printHtmlPart(15)
expressionOut.print(coaReportFor.size()-1)
printHtmlPart(16)
expressionOut.print(coaReportFor.size()-1)
printHtmlPart(17)
expressionOut.print(coaReportFor.LOT_ID[0])
printHtmlPart(18)
}
else {
printHtmlPart(19)
expressionOut.print(coaReportFor.LOT_ID[0])
printHtmlPart(18)
}
printHtmlPart(20)
if(true && (coaReportFor.size()>=2)) {
printHtmlPart(21)
expressionOut.print(coaReportFor.MLOT_ID[1])
printHtmlPart(18)
}
printHtmlPart(22)
if(true && (coaReportFor.size()>=3)) {
printHtmlPart(23)
expressionOut.print(coaReportFor.MLOT_ID[2])
printHtmlPart(24)
}
printHtmlPart(2)
if(true && (coaReportFor.size()>=4)) {
printHtmlPart(23)
expressionOut.print(coaReportFor.MLOT_ID[3])
printHtmlPart(24)
}
printHtmlPart(25)
if(true && (coaReportFor.size()>=5)) {
printHtmlPart(23)
expressionOut.print(coaReportFor.MLOT_ID[4])
printHtmlPart(24)
}
printHtmlPart(25)
if(true && (coaReportFor.size()>=6)) {
printHtmlPart(23)
expressionOut.print(coaReportFor.MLOT_ID[5])
printHtmlPart(24)
}
printHtmlPart(26)
expressionOut.print(coaReportForEdc.size+3)
printHtmlPart(27)
loop:{
int j = 0
for( edc in (coaReportForEdc) ) {
printHtmlPart(28)
expressionOut.print(j+1)
printHtmlPart(29)
expressionOut.print(edc.DESCRIPTION)
printHtmlPart(29)
expressionOut.print(edc.EQP_DESC)
printHtmlPart(29)
expressionOut.print(edc.LL)
printHtmlPart(30)
expressionOut.print(edc.DC_DATA)
printHtmlPart(31)
j++
}
}
printHtmlPart(32)
}
else {
printHtmlPart(33)
invokeTag('render','g',143,['template':("../template/emptyPanel")],-1)
printHtmlPart(34)
}
printHtmlPart(35)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1528426618530L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
