import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_dwsStatistics_statisticsExcel_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/dwsStatistics/_statisticsExcel.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(1)
if(true && (headInstance != 'RN')) {
printHtmlPart(2)
invokeTag('message','g',9,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(3)
}
printHtmlPart(4)
j++
}
}
printHtmlPart(5)
if(true && (result.size() > 0)) {
printHtmlPart(4)
loop:{
int i = 0
for( dataInstance in (result) ) {
printHtmlPart(6)
expressionOut.print(i + 1)
printHtmlPart(7)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(8)
if(true && (headInstance != 'RN')) {
printHtmlPart(9)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(10)
}
printHtmlPart(11)
j++
}
}
printHtmlPart(12)
i++
}
}
printHtmlPart(13)
}
printHtmlPart(14)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508464675931L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
