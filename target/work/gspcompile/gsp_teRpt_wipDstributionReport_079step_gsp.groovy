import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_wipDstributionReport_079step_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/wipDstribution/Report_079step.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(2)
if(true && (headInstance != 'RN')) {
printHtmlPart(3)
invokeTag('message','g',17,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(4)
}
printHtmlPart(5)
j++
}
}
printHtmlPart(6)
loop:{
int i = 0
for( dataInstance in (Report_079step) ) {
printHtmlPart(7)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(8)
if(true && (headInstance != 'RN')) {
printHtmlPart(9)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(10)
}
printHtmlPart(2)
j++
}
}
printHtmlPart(11)
i++
}
}
printHtmlPart(12)
invokeTag('render','g',36,['template':("../template/pagination")],-1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508464675923L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
