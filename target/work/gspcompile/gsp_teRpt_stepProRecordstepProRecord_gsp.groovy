import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_stepProRecordstepProRecord_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/stepProRecord/stepProRecord.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',4,['name':("SYS_QUERY_NAME"),'value':("stepProRecord")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',5,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
expressionOut.print(stepName)
printHtmlPart(4)
expressionOut.print(startTime)
printHtmlPart(5)
expressionOut.print(endTime)
printHtmlPart(6)
expressionOut.print(lotId)
printHtmlPart(7)
expressionOut.print(request.getContextPath())
printHtmlPart(8)
if(true && (reportList.size() > 0)) {
printHtmlPart(9)
for( action in (stepProRecordAction) ) {
printHtmlPart(10)
expressionOut.print(action.ACTION_CODE)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (reportList.size()>0)) {
printHtmlPart(13)
loop:{
int i = 0
for( dataInstance in (reportList) ) {
printHtmlPart(14)
expressionOut.print(dataInstance.PARENT_ID)
printHtmlPart(15)
expressionOut.print(dataInstance.规格型号)
printHtmlPart(15)
expressionOut.print(dataInstance.LOT_NO)
printHtmlPart(15)
expressionOut.print(dataInstance.投入数)
printHtmlPart(15)
expressionOut.print(dataInstance.产出数)
printHtmlPart(15)
expressionOut.print(dataInstance.设备名称)
printHtmlPart(15)
expressionOut.print(dataInstance.设备耗时)
printHtmlPart(15)
expressionOut.print(dataInstance.返工数量)
printHtmlPart(15)
expressionOut.print(dataInstance.报废数量)
printHtmlPart(16)
for( action in (stepProRecordAction) ) {
printHtmlPart(17)
expressionOut.print(dataInstance[action.ACTION_CODE])
printHtmlPart(18)
}
printHtmlPart(19)
expressionOut.print(dataInstance.作业者)
printHtmlPart(15)
expressionOut.print(dataInstance.作业时间)
printHtmlPart(20)
i++
}
}
printHtmlPart(2)
}
else {
printHtmlPart(13)
invokeTag('render','g',65,['template':("../template/emptyPanel")],-1)
printHtmlPart(2)
}
printHtmlPart(21)
}
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1528427576899L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
