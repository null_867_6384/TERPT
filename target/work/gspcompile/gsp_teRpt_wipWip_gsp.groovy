import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_wipWip_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/wip/Wip.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',4,['name':("SYS_QUERY_NAME"),'value':("wipTrend")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',5,['name':("EXPORT_CONTENT_NAME"),'value':("wipExcel")],-1)
printHtmlPart(3)
invokeTag('select','g',7,['name':("productid"),'from':(productList),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("PRODUCTID"),'optionValue':("PRODUCTID"),'value':(productid),'noSelection':(['':'ALL'])],-1)
printHtmlPart(4)
invokeTag('select','g',9,['name':("lotType"),'from':(lotTypeList),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("LOTTYPE"),'optionValue':("LOTTYPE"),'value':(lotType),'noSelection':(['':'ALL'])],-1)
printHtmlPart(5)
invokeTag('select','g',11,['name':("stage"),'from':(stageList),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("STAGE"),'optionValue':("STAGE"),'value':(stage),'noSelection':(['':'ALL'])],-1)
printHtmlPart(6)
invokeTag('select','g',13,['name':("state"),'from':(stateList),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("STATE"),'optionValue':("STATE"),'value':(state),'noSelection':(['':'ALL'])],-1)
printHtmlPart(7)
expressionOut.print(request.getContextPath())
printHtmlPart(8)
if(true && (wipTrendList.size() > 0)) {
printHtmlPart(9)
invokeTag('render','g',24,['template':("exportwipExcel")],-1)
printHtmlPart(10)
}
else {
printHtmlPart(9)
invokeTag('render','g',27,['template':("../template/emptyPanel")],-1)
printHtmlPart(10)
}
printHtmlPart(11)
invokeTag('render','g',1,['template':("../template/pagination")],-1)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1510111210519L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
