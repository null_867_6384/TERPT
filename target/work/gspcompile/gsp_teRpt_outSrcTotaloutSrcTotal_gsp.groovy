import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_outSrcTotaloutSrcTotal_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/outSrcTotal/outSrcTotal.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',4,['name':("SYS_QUERY_NAME"),'value':("outSrcTotal")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',5,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
invokeTag('select','g',7,['name':("custName"),'from':(outsrcCustName),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("cust_name"),'optionValue':("cust_name"),'value':(custName),'noSelection':(['':'--ALL--'])],-1)
printHtmlPart(4)
invokeTag('select','g',9,['name':("outsrcUser"),'from':(outsrcUserList),'data-toggle':("selectpicker"),'data-live-search':("true"),'optionKey':("COMPERATION_NAME"),'optionValue':("COMPERATION_NAME"),'value':(outsrcUser),'noSelection':(['':'--ALL--'])],-1)
printHtmlPart(5)
expressionOut.print(startT)
printHtmlPart(6)
expressionOut.print(endT)
printHtmlPart(7)
expressionOut.print(request.getContextPath())
printHtmlPart(8)
if(true && (outSrcTotal.size() > 0)) {
printHtmlPart(9)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(10)
if(true && (headInstance!='RN'&&headInstance!='露铜'&&headInstance!='露铜良率'&&headInstance!='表面镍污'&&headInstance!='表面镍污良率'&&headInstance!='间距镍污'&&headInstance!='间距镍污良率'&&headInstance!='划痕'&&headInstance!='划痕良率'&&headInstance!='镀镍其他不良'&&headInstance!='镀镍其他不良良率')) {
printHtmlPart(11)
invokeTag('message','g',29,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(12)
}
else if(true && (headInstance=='露铜'||headInstance=='表面镍污'||headInstance=='间距镍污'||headInstance=='划痕'||headInstance=='镀镍其他不良')) {
printHtmlPart(13)
expressionOut.print(headInstance)
printHtmlPart(12)
}
printHtmlPart(14)
j++
}
}
printHtmlPart(15)
if(true && (outSrcTotal.size()>0)) {
printHtmlPart(14)
loop:{
int i = 0
for( dataInstance in (outSrcTotal) ) {
printHtmlPart(16)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(17)
if(true && (headInstance!='RN')) {
printHtmlPart(18)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(19)
}
printHtmlPart(20)
j++
}
}
printHtmlPart(21)
i++
}
}
printHtmlPart(22)
}
else {
printHtmlPart(14)
invokeTag('render','g',51,['template':("../template/emptyPanel")],-1)
printHtmlPart(2)
}
printHtmlPart(23)
}
printHtmlPart(24)
invokeTag('render','g',54,['template':("../template/pagination")],-1)
printHtmlPart(25)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1528340410643L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
