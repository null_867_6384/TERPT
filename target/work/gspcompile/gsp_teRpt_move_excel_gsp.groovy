import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_move_excel_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/move/_excel.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (tableHeadList1.size() > 0)) {
printHtmlPart(1)
loop:{
int j = 0
for( headInstance1 in (tableHeadList1) ) {
printHtmlPart(2)
invokeTag('message','g',7,['code':(headInstance1),'default':(headInstance1)],-1)
printHtmlPart(3)
j++
}
}
printHtmlPart(4)
loop:{
int i = 0
for( dataInstance1 in (moveStageList) ) {
printHtmlPart(5)
expressionOut.print(i + 1)
printHtmlPart(6)
loop:{
int j = 0
for( headInstance1 in (tableHeadList1) ) {
printHtmlPart(7)
if(true && (headInstance1 == 'STAGE_ID')) {
printHtmlPart(8)
expressionOut.print(request.getContextPath())
printHtmlPart(9)
out.print(dataInstance1.STAGE_ID)
printHtmlPart(10)
out.print(params.startTime)
printHtmlPart(11)
out.print(params.endTime)
printHtmlPart(12)
expressionOut.print(dataInstance1["${headInstance1}"])
printHtmlPart(13)
}
else {
printHtmlPart(14)
expressionOut.print(dataInstance1["${headInstance1}"])
printHtmlPart(15)
}
printHtmlPart(16)
j++
}
}
printHtmlPart(17)
i++
}
}
printHtmlPart(18)
}
else {
printHtmlPart(18)
invokeTag('render','g',32,['template':("../template/emptyPanel")],-1)
printHtmlPart(18)
}
printHtmlPart(19)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511424090069L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
