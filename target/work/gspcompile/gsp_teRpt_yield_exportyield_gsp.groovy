import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_yield_exportyield_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/yield/_exportyield.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
loop:{
int i = 0
for( dataInstance in (dataListNo) ) {
printHtmlPart(1)
expressionOut.print(dataInstance.PART_TYPE)
printHtmlPart(2)
if(true && (dataInstance.PARAM_NAME=='in_qty')) {
printHtmlPart(3)
}
else if(true && (dataInstance.PARAM_NAME=='out_qty')) {
printHtmlPart(4)
}
else if(true && (dataInstance.PARAM_NAME=='yield')) {
printHtmlPart(5)
}
else {
printHtmlPart(6)
}
printHtmlPart(7)
expressionOut.print(dataInstance.JAN)
printHtmlPart(8)
expressionOut.print(dataInstance.FEB)
printHtmlPart(8)
expressionOut.print(dataInstance.MAR)
printHtmlPart(8)
expressionOut.print(dataInstance.APR)
printHtmlPart(8)
expressionOut.print(dataInstance.MAY)
printHtmlPart(8)
expressionOut.print(dataInstance.JUN)
printHtmlPart(8)
expressionOut.print(dataInstance.JUL)
printHtmlPart(8)
expressionOut.print(dataInstance.AUG)
printHtmlPart(8)
expressionOut.print(dataInstance.SEP)
printHtmlPart(8)
expressionOut.print(dataInstance.OCT)
printHtmlPart(8)
expressionOut.print(dataInstance.NOV)
printHtmlPart(8)
expressionOut.print(dataInstance.DEC)
printHtmlPart(9)
i++
}
}
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1527159895039L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
