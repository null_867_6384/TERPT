import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_dwtCpk_CPKExcel_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/dwtCpk/_CPKExcel.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(1)
invokeTag('message','g',6,['code':(headInstance),'default':(headInstance)],-1)
printHtmlPart(2)
j++
}
}
printHtmlPart(3)
if(true && (CPKList.size() > 0)) {
printHtmlPart(4)
loop:{
int i = 0
for( dataInstance in (CPKList) ) {
printHtmlPart(5)
loop:{
int j = 0
for( headInstance in (tableHeadList) ) {
printHtmlPart(6)
expressionOut.print(dataInstance["${headInstance}"])
printHtmlPart(7)
j++
}
}
printHtmlPart(8)
i++
}
}
printHtmlPart(9)
}
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508464675932L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
