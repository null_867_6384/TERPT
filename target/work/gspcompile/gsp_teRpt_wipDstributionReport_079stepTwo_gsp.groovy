import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_wipDstributionReport_079stepTwo_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/wipDstribution/Report_079stepTwo.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createTagBody(2, {->
invokeTag('captureTitle','sitemesh',10,[:],-1)
})
invokeTag('wrapTitleTag','sitemesh',11,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',11,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(request.getContextPath())
printHtmlPart(5)
expressionOut.print(request.getContextPath())
printHtmlPart(6)
expressionOut.print(request.getContextPath())
printHtmlPart(7)
loop:{
int i = 0
for( tableInstance in (Report_079img) ) {
printHtmlPart(8)
expressionOut.print(tableInstance.STAGE_DESC)
printHtmlPart(9)
i++
}
}
printHtmlPart(10)
expressionOut.print(Report_079img.WIP_QTY)
printHtmlPart(11)
expressionOut.print(Report_079img.WAIT_QTY)
printHtmlPart(12)
expressionOut.print(Report_079img.RUN_QTY)
printHtmlPart(13)
expressionOut.print(Report_079img.HOLD_QTY)
printHtmlPart(14)
})
invokeTag('captureBody','sitemesh',70,[:],1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1508464675923L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
