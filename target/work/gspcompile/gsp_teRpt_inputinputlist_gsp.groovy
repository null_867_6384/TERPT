import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_teRpt_inputinputlist_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/input/inputlist.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.getContextPath())
printHtmlPart(1)
invokeTag('hiddenField','g',5,['name':("SYS_QUERY_NAME"),'value':("inputlist")],-1)
printHtmlPart(2)
invokeTag('hiddenField','g',6,['name':("EXPORT_CONTENT_NAME"),'value':("Excel")],-1)
printHtmlPart(3)
expressionOut.print(request.getContextPath())
printHtmlPart(4)
expressionOut.print(request.getContextPath())
printHtmlPart(5)
loop:{
int i = 0
for( wipTrendInstance in (inputList) ) {
printHtmlPart(6)
expressionOut.print(wipTrendInstance.PART_TYPE)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.INPUT_YEAR)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.INPUT_MONTH)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.IN_QTY)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.OUT_QTY)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.YIELD)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.RESERVE_DOUBLE1)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.LM_USER)
printHtmlPart(7)
expressionOut.print(wipTrendInstance.LAST_UPDATED)
printHtmlPart(8)
expressionOut.print(request.getContextPath())
printHtmlPart(9)
expressionOut.print(wipTrendInstance.ID)
printHtmlPart(10)
expressionOut.print(request.getContextPath())
printHtmlPart(11)
expressionOut.print(wipTrendInstance.ID)
printHtmlPart(12)
i++
}
}
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1511231746629L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
